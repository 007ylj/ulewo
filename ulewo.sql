/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : ulewo4

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2017-03-24 16:02:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for news_article
-- ----------------------------
DROP TABLE IF EXISTS `news_article`;
CREATE TABLE `news_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `thumbnail` varchar(500) DEFAULT NULL COMMENT '缩略图以竖线分割',
  `content` longtext,
  `category` int(1) DEFAULT NULL COMMENT '分类',
  `create_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0:未发布1:已发布',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='爬新闻';

-- ----------------------------
-- Records of news_article
-- ----------------------------

-- ----------------------------
-- Table structure for news_web_site
-- ----------------------------
DROP TABLE IF EXISTS `news_web_site`;
CREATE TABLE `news_web_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `web_site` varchar(200) NOT NULL DEFAULT '',
  `content_class` varchar(30) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='爬新闻的网址';

-- ----------------------------
-- Records of news_web_site
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_ask
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_ask`;
CREATE TABLE `ulewo_ask` (
  `ask_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '问答ID',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `p_category_id` int(11) DEFAULT NULL COMMENT '父级分类ID',
  `category_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `content` longtext COMMENT '内容',
  `summary` text COMMENT '摘要',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `read_count` int(11) DEFAULT '0' COMMENT '阅读数',
  `like_count` int(11) DEFAULT '0' COMMENT '喜欢数',
  `comment_count` int(11) DEFAULT '0' COMMENT '评论数',
  `collection_count` int(11) DEFAULT '0' COMMENT '收藏数',
  `ask_image` varchar(1000) DEFAULT NULL COMMENT '图片',
  `ask_image_thum` varchar(1000) DEFAULT NULL COMMENT '缩略图',
  `mark` int(11) DEFAULT '0' COMMENT '赏分',
  `best_answer_id` int(11) DEFAULT NULL COMMENT '最佳回复id',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `slove_type` int(1) DEFAULT '0' COMMENT '是否已经解决 0未解决 1已经解决',
  `best_answer_user_id` int(11) DEFAULT NULL COMMENT '最佳答案作者',
  `best_answer_user_name` varchar(50) DEFAULT NULL COMMENT '最佳答案作者名称',
  PRIMARY KEY (`ask_id`),
  KEY `idx_topic_id` (`ask_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`p_category_id`,`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='问答';

-- ----------------------------
-- Records of ulewo_ask
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_attachment
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_attachment`;
CREATE TABLE `ulewo_attachment` (
  `attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL COMMENT '文章ID',
  `file_name` varchar(200) DEFAULT NULL COMMENT '文件名',
  `file_url` varchar(255) DEFAULT NULL COMMENT '文件地址',
  `download_mark` int(11) DEFAULT '0' COMMENT '下载所需积分',
  `download_count` int(11) DEFAULT '0' COMMENT '下载次数',
  `topic_type` char(1) DEFAULT 'T' COMMENT 'T:论坛 B:博客',
  PRIMARY KEY (`attachment_id`),
  KEY `attachment_id` (`attachment_id`),
  KEY `attachment_topicid` (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='附件';

-- ----------------------------
-- Records of ulewo_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_attachment_download
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_attachment_download`;
CREATE TABLE `ulewo_attachment_download` (
  `attachment_id` int(11) NOT NULL DEFAULT '0' COMMENT '附件ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `create_time` datetime DEFAULT NULL COMMENT '下载时间',
  PRIMARY KEY (`attachment_id`,`user_id`),
  KEY `attachement_download_id` (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件下载';

-- ----------------------------
-- Records of ulewo_attachment_download
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_blog
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_blog`;
CREATE TABLE `ulewo_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `content` longtext,
  `read_count` int(11) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `key_word` varchar(255) DEFAULT NULL,
  `allow_comment` int(2) DEFAULT NULL,
  `blog_image_thum` varchar(3000) DEFAULT NULL COMMENT '缩略图',
  `blog_image` varchar(3000) DEFAULT NULL COMMENT '图片',
  `comment_count` int(11) DEFAULT '0' COMMENT '评论数',
  `like_count` int(11) DEFAULT '0' COMMENT '喜欢数',
  `collection_count` int(11) DEFAULT '0' COMMENT '收藏数',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `status` int(1) DEFAULT '0' COMMENT '状态 0：草稿 1：已经发布',
  `markdown_content` longtext,
  `editor_type` int(1) DEFAULT '1' COMMENT '1:百度编辑器 2：markdown编辑器',
  PRIMARY KEY (`blog_id`),
  KEY `blog_index_id` (`blog_id`),
  KEY `blog_index_userid` (`user_id`),
  KEY `blog_index_categoryid` (`category_id`),
  KEY `blog_index_userid_categoryid` (`user_id`,`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='博客';

-- ----------------------------
-- Records of ulewo_blog
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_blog_category
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_blog_category`;
CREATE TABLE `ulewo_blog_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `name` varchar(50) DEFAULT NULL,
  `rang` int(2) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `blog_categroy_id` (`category_id`),
  KEY `blog_categroy_userid` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='博客分类';

-- ----------------------------
-- Records of ulewo_blog_category
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_book
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_book`;
CREATE TABLE `ulewo_book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'bookId',
  `cover_image` varchar(255) DEFAULT NULL COMMENT '封面图片路径',
  `cover_background` varchar(30) DEFAULT NULL COMMENT '封面背景颜色',
  `book_name` varchar(255) DEFAULT NULL COMMENT '书名',
  `chapter_count` int(11) DEFAULT NULL COMMENT '章节数',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='书、专题';

-- ----------------------------
-- Records of ulewo_book
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_book_chapter
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_book_chapter`;
CREATE TABLE `ulewo_book_chapter` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `book_id` int(11) DEFAULT NULL COMMENT '书ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `summary` text COMMENT '摘要',
  `content` longtext COMMENT '内容',
  `read_count` int(11) DEFAULT '0' COMMENT '阅读数',
  `key_word` varchar(255) DEFAULT NULL COMMENT '关键词',
  `allow_comment` int(1) DEFAULT NULL COMMENT '是否允许评论 0：允许 1不允许',
  `article_image_thum` varchar(3000) DEFAULT NULL COMMENT '缩略图',
  `article_image` varchar(3000) DEFAULT NULL COMMENT '图片',
  `comment_count` int(11) DEFAULT '0' COMMENT '评论数',
  `like_count` int(11) DEFAULT '0' COMMENT '喜欢数',
  `collection_count` int(11) DEFAULT '0' COMMENT '收藏数',
  `status` int(1) DEFAULT '0' COMMENT '状态 0：草稿 1：已经发布',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`article_id`),
  KEY `blog_index_userid` (`user_id`),
  KEY `blog_index_bookid` (`book_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='书、专题 章节';

-- ----------------------------
-- Records of ulewo_book_chapter
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_category
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_category`;
CREATE TABLE `ulewo_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0' COMMENT '父Id',
  `name` varchar(30) DEFAULT NULL COMMENT '分类名称',
  `description` varchar(500) DEFAULT '分类描述',
  `rang` int(11) DEFAULT NULL COMMENT '分类编号',
  `allow_post` varchar(1) DEFAULT 'Y' COMMENT '允许发帖',
  `show_in_bbs` char(1) DEFAULT 'Y' COMMENT '是否是论坛分类：Y 是 N 不是',
  `show_in_question` char(1) DEFAULT 'Y' COMMENT '是否是问答分类：Y 是 N 不是',
  `show_in_knowledge` char(1) DEFAULT 'Y' COMMENT '是否是知识库分类：Y 是 N 不是',
  `show_in_exam` char(1) DEFAULT 'Y',
  PRIMARY KEY (`category_id`),
  KEY `topic_category_index_id` (`category_id`),
  KEY `idx_show_in_bbs` (`show_in_bbs`),
  KEY `idx_show_in_question` (`show_in_question`),
  KEY `idx_show_in_knowledge` (`show_in_knowledge`)
) ENGINE=InnoDB AUTO_INCREMENT=10060 DEFAULT CHARSET=utf8 COMMENT='系统分类';

-- ----------------------------
-- Records of ulewo_category
-- ----------------------------
INSERT INTO `ulewo_category` VALUES ('10030', '0', '招聘求职', '招聘求职，招聘人才，面试技巧，面试经验', '5', 'Y', 'Y', null, 'Y', null);
INSERT INTO `ulewo_category` VALUES ('10031', '10030', '招聘', null, '1', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10032', '10030', '面试分享', null, '2', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10033', '0', '程序猿那些事儿', '海阔天空，程序猿的那些事儿', '3', 'Y', 'Y', null, null, null);
INSERT INTO `ulewo_category` VALUES ('10034', '10033', '八卦', null, '1', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10035', '10033', '实力吹牛', null, '2', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10036', '10033', '闲聊', null, '3', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10037', '10033', '锻炼身体', null, '5', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10038', '0', '有乐窝官方', '有乐窝官方', '4', 'Y', 'Y', null, null, null);
INSERT INTO `ulewo_category` VALUES ('10039', '10038', '活动', null, '1', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10040', '10038', '意见\\建议', null, '2', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10041', '10038', '网站bug反馈', null, '3', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10042', '10038', '网站源码', null, '5', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10043', '0', '实力自学', '自学能力很重要，一起来自学吧', '1', 'Y', 'Y', null, 'Y', null);
INSERT INTO `ulewo_category` VALUES ('10044', '10043', '自学心得', null, '1', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10045', '10043', '学习资料', null, '2', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10048', '10038', '新人介绍', null, '7', null, 'Y', null, null, null);
INSERT INTO `ulewo_category` VALUES ('10049', '0', '编程技术', '用程序改变世界', '2', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10050', '10049', 'Java相关', null, '1', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10051', '10049', 'Web前端', null, '2', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10052', '10049', '其他编程语言', null, '3', 'Y', 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10054', '10049', '面试题', null, '4', null, 'Y', null, 'Y', 'Y');
INSERT INTO `ulewo_category` VALUES ('10055', '10049', 'IT资讯', null, '5', null, 'Y', null, null, null);
INSERT INTO `ulewo_category` VALUES ('10056', '10038', '有乐窝视频教程', null, '6', null, 'Y', null, null, null);
INSERT INTO `ulewo_category` VALUES ('10057', '10038', '网站更新', null, '8', null, 'Y', null, null, null);
INSERT INTO `ulewo_category` VALUES ('10058', '10038', '开源项目', null, '4', null, 'Y', null, null, null);
INSERT INTO `ulewo_category` VALUES ('10059', '10033', '虐心的code', null, '4', null, 'Y', null, null, null);

-- ----------------------------
-- Table structure for ulewo_collection
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_collection`;
CREATE TABLE `ulewo_collection` (
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `article_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `article_type` char(1) NOT NULL DEFAULT '',
  `create_time` datetime DEFAULT NULL,
  `article_user_id` int(11) DEFAULT NULL COMMENT '文章作者ID',
  PRIMARY KEY (`user_id`,`article_id`,`article_type`),
  KEY `collection_index_userid` (`user_id`),
  KEY `collection_index_articleid` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏';

-- ----------------------------
-- Records of ulewo_collection
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_comment
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_comment`;
CREATE TABLE `ulewo_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL COMMENT '文章ID',
  `content` longtext COMMENT '内容',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL,
  `source_from` char(1) DEFAULT 'P' COMMENT 'P:pc端发布  A:安卓客户端发出',
  `article_type` char(1) DEFAULT NULL COMMENT '文章类型 T:讨论区 B:博客 S:吐槽 K:知识库 A:问答',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`id`),
  KEY `topic_comment_index_id` (`id`),
  KEY `topic_comment_index_pid` (`pid`),
  KEY `topic_comment_index_topicid` (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='评论';

-- ----------------------------
-- Records of ulewo_comment
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_exam
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_exam`;
CREATE TABLE `ulewo_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_title` text,
  `p_category_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `choose_type` int(2) DEFAULT NULL COMMENT '1:单选  2:多选',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '状态 0 未审核  1已审核',
  `analyse` text,
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='在线考试';

-- ----------------------------
-- Records of ulewo_exam
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_exam_detail
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_exam_detail`;
CREATE TABLE `ulewo_exam_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL,
  `answer` varchar(500) DEFAULT NULL COMMENT '答案',
  `is_right_answer` int(1) DEFAULT '0' COMMENT '0:非正确答案 1:正确答案',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='考题详情';

-- ----------------------------
-- Records of ulewo_exam_detail
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_knowledge
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_knowledge`;
CREATE TABLE `ulewo_knowledge` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `p_category_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `content` longtext,
  `summary` text,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL,
  `read_count` int(11) DEFAULT '0' COMMENT '0',
  `topic_image` text,
  `topic_image_thum` text,
  `status` int(1) DEFAULT '0' COMMENT '状态 0：未审核 1：已经审核',
  `comment_count` int(11) DEFAULT '0' COMMENT '评论数',
  `like_count` int(11) DEFAULT '0' COMMENT '喜欢数',
  `collection_count` int(11) DEFAULT '0' COMMENT '收藏数',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`topic_id`),
  KEY `idx_topic_id` (`topic_id`),
  KEY `idx_category_id` (`p_category_id`,`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='知识库';

-- ----------------------------
-- Records of ulewo_knowledge
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_like
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_like`;
CREATE TABLE `ulewo_like` (
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '文章ID',
  `article_type` char(1) NOT NULL DEFAULT '' COMMENT '文章类型 T:讨论区 B:博客 S:吐槽 K:知识库 A:问答',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`article_id`,`user_id`,`article_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='点赞';

-- ----------------------------
-- Records of ulewo_like
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_message
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_message`;
CREATE TABLE `ulewo_message` (
  `id` varchar(50) NOT NULL,
  `received_user_id` int(11) DEFAULT NULL COMMENT '接受人ID',
  `create_time` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `description` varchar(500) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_id` (`id`),
  KEY `idx_receive_userid` (`received_user_id`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息';

-- ----------------------------
-- Records of ulewo_message
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_message_settings
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_message_settings`;
CREATE TABLE `ulewo_message_settings` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `msg_type` int(1) DEFAULT NULL COMMENT '0：开启：1关闭',
  `create_time` datetime DEFAULT NULL,
  `notice_sign_in_type` int(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接受邮件消息设置';

-- ----------------------------
-- Records of ulewo_message_settings
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_sign_in
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_sign_in`;
CREATE TABLE `ulewo_sign_in` (
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `sign_date` date NOT NULL DEFAULT '0000-00-00',
  `sign_time` datetime DEFAULT NULL,
  `source_from` varchar(1) DEFAULT 'P' COMMENT '来源 A:安卓 P:PC',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`user_id`,`sign_date`),
  KEY `signin_index_userid` (`user_id`),
  KEY `signin_index_signdate` (`sign_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签到';

-- ----------------------------
-- Records of ulewo_sign_in
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_spit_slot
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_spit_slot`;
CREATE TABLE `ulewo_spit_slot` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `image_url` varchar(500) DEFAULT NULL COMMENT '图片路径',
  `image_url_small` varchar(600) DEFAULT NULL,
  `music_url` varchar(500) DEFAULT NULL,
  `content` text COMMENT '内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `source_from` varchar(1) DEFAULT 'P' COMMENT '来源 A:安卓 P:PC',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `comment_count` int(11) DEFAULT '0' COMMENT '评论数',
  `like_count` int(11) DEFAULT '0' COMMENT '喜欢数',
  PRIMARY KEY (`id`),
  KEY `blast_index_id` (`id`),
  KEY `blast_index_userid` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='吐槽';

-- ----------------------------
-- Records of ulewo_spit_slot
-- ----------------------------
INSERT INTO `ulewo_spit_slot` VALUES ('10000', '10000', '', '', null, '第一个吐槽', '2017-03-24 14:51:27', 'P', 'ulewo', 'avatars/10000.jpg', '0', '0');

-- ----------------------------
-- Table structure for ulewo_spit_slot_comment
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_spit_slot_comment`;
CREATE TABLE `ulewo_spit_slot_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '吐槽评论ID',
  `spit_slot_id` int(11) DEFAULT NULL COMMENT '吐槽id',
  `content` varchar(1000) DEFAULT NULL COMMENT '评论内容',
  `create_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `source_from` char(1) DEFAULT 'P',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`id`),
  KEY `blastcomment_index_blastid` (`spit_slot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='吐槽评论';

-- ----------------------------
-- Records of ulewo_spit_slot_comment
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_statistics
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_statistics`;
CREATE TABLE `ulewo_statistics` (
  `statistics_date` date NOT NULL COMMENT '统计日期',
  `signin_count` int(11) DEFAULT '0' COMMENT '签到数',
  `spit_slot_count` int(11) DEFAULT '0' COMMENT '吐槽数',
  `spit_slot_comment_count` int(11) DEFAULT '0' COMMENT '吐槽评论数',
  `topic_count` int(11) DEFAULT '0' COMMENT '帖子数',
  `topic_comment_count` int(11) DEFAULT '0' COMMENT '帖子评论数',
  `knowledge_count` int(11) DEFAULT '0' COMMENT '知识库数',
  `knowledge_comment_count` int(11) DEFAULT '0' COMMENT '知识库评论数',
  `ask_count` int(11) DEFAULT '0' COMMENT '问答数',
  `ask_comment_count` int(11) DEFAULT '0' COMMENT '问答评论数',
  `blog_count` int(11) DEFAULT '0' COMMENT '博客数',
  `blog_comment_count` int(11) DEFAULT '0' COMMENT '博客评论数',
  `exam_count` int(11) DEFAULT '0' COMMENT '考题数',
  `user_count` int(11) DEFAULT '0' COMMENT '新增用户数',
  `active_user_count` int(11) DEFAULT '0' COMMENT '活跃用户数',
  PRIMARY KEY (`statistics_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='统计信息';

-- ----------------------------
-- Records of ulewo_statistics
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_task
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_task`;
CREATE TABLE `ulewo_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `task_classz` varchar(100) DEFAULT NULL COMMENT '任务执行的类',
  `task_method` varchar(100) DEFAULT NULL COMMENT '任务执行的方法',
  `task_time` varchar(50) DEFAULT NULL COMMENT '任务调度时间',
  `last_update_time` datetime DEFAULT NULL COMMENT '任务最后更新时间',
  `status` int(2) DEFAULT '0' COMMENT '任务状态 0：初始状态 1:暂停',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='任务调度';

-- ----------------------------
-- Records of ulewo_task
-- ----------------------------
INSERT INTO `ulewo_task` VALUES ('6', 'com.ulewo.task.StatisticsTask', 'statisticsInfo', '0 55 23 * * ? *', '2015-12-19 18:24:43', '0', '统计数量');
INSERT INTO `ulewo_task` VALUES ('7', 'com.ulewo.task.SpiderNewsTask', 'spiderNews', '0 0/10 08 * * ?', '2017-03-24 14:46:32', '0', '抓取新闻');
INSERT INTO `ulewo_task` VALUES ('8', 'com.ulewo.task.StatisticsTask', 'noticeSignInTask', '0 0 12 * * ?', '2016-03-19 22:08:09', '0', '通知用户去签到');

-- ----------------------------
-- Table structure for ulewo_topic
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_topic`;
CREATE TABLE `ulewo_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `topic_type` int(1) DEFAULT '0' COMMENT '帖子类型 0普通帖 1投票贴',
  `p_category_id` int(11) DEFAULT NULL COMMENT '组id',
  `category_id` int(11) DEFAULT NULL COMMENT '分组ID',
  `title` varchar(100) DEFAULT '' COMMENT '标题',
  `keyword` varchar(100) DEFAULT NULL COMMENT '关键字',
  `content` longtext COMMENT '内容',
  `summary` text COMMENT '摘要',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `create_time` datetime DEFAULT NULL,
  `last_comment_time` datetime DEFAULT NULL,
  `read_count` int(11) DEFAULT '0' COMMENT '阅读数',
  `grade` int(2) DEFAULT '0' COMMENT '帖子等级 0普通帖 置顶1',
  `essence` int(1) DEFAULT '0' COMMENT '精华',
  `valid` varchar(2) DEFAULT 'Y' COMMENT '是否有效 Y有效 N无效',
  `topic_image` mediumtext,
  `topic_image_thum` mediumtext COMMENT '文章图标',
  `comment_count` int(11) DEFAULT '0' COMMENT '评论数',
  `like_count` int(11) DEFAULT '0' COMMENT '喜欢数',
  `collection_count` int(11) DEFAULT '0' COMMENT '收藏数',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`topic_id`),
  KEY `topic_index_id` (`topic_id`),
  KEY `topic_index_gid` (`p_category_id`),
  KEY `topic_index_id_gid` (`topic_id`,`p_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10102 DEFAULT CHARSET=utf8 COMMENT='论坛主题';

-- ----------------------------
-- Records of ulewo_topic
-- ----------------------------
INSERT INTO `ulewo_topic` VALUES ('10088', '0', '10049', '10055', 'Jackson 2.9.0.pr2 发布，高性能 JSON 处理', null, '<p>Jackson 2.9.0.pr2 发布了。Jackson 是一个 Java 用来处理 JSON 格式数据的类库。</p><p>该版本暂未提供更新内容。</p><p>下载地址：</p><ul class=\"release-downloads list-paddingleft-2\"><li><p><a href=\"https://github.com/FasterXML/jackson-core/archive/jackson-core-2.9.0.pr2.zip\" rel=\"nofollow\">Source code (zip)</a></p></li><li><p><a href=\"https://github.com/FasterXML/jackson-core/archive/jackson-core-2.9.0.pr2.zip\" rel=\"nofollow\"></a></p><p><a href=\"https://github.com/FasterXML/jackson-core/archive/jackson-core-2.9.0.pr2.tar.gz\" rel=\"nofollow\">Source code (tar.gz)</a></p></li></ul>', 'Jackson 2.9.0.pr2 发布了。Jackson 是一个 Java 用来处理 JSON 格式数据的类库。该版本暂未提供更新内容。下载地址：Source code (zip)Source code (tar.gz)', '10000', '2017-03-24 14:46:33', '2017-03-24 14:46:33', '32', '0', '0', 'Y', '', '', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10089', '0', '10049', '10055', 'Dropwizard 1.1.0 发布，Java 框架', null, '<p>Dropwizard 1.1.0 发布了，Dropwizard 是一个 Java 框架，用于开发易于运维，高性能的 RESTful 网络服务。它由 <a data-cke-saved-href=\"https://www.yammer.com/\" href=\"https://www.yammer.com/\" target=\"_blank\">Yammer</a> 开发，用于增强基于 JVM 的后端服务，集合了来自 Java 生态系统中的稳定和成熟的程序库，组成了简单，轻量级的程序包，使得用户可以专注于把事情做好。</p><p>Dropwizard 是开箱即用的，它支持复杂的配置，应用环境， 日志和运维工具，使得你和你的团队可能在最短的时间内交付一个一流品质 HTTP+JSON 网络服务。</p><p>更新内容：</p><ul class=\"list-paddingleft-2\"><li><p>Upgraded to Hibernate ORM 5.2.7 <a href=\"https://github.com/dropwizard/dropwizard/pull/1871\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1871\" data-id=\"199376194\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1871</a></p></li><li><p>Add runtime certificate reload via admin task <a href=\"https://github.com/dropwizard/dropwizard/pull/1799\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1799\" data-id=\"186970838\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1799</a></p></li><li><p>List available tasks lexically via admin task <a href=\"https://github.com/dropwizard/dropwizard/pull/1939\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1939\" data-id=\"210350729\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1939</a></p></li><li><p>Add support for optional resource protection <a href=\"https://github.com/dropwizard/dropwizard/pull/1931\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1931\" data-id=\"208293953\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1931</a></p></li><li><p>Invalid enum request parameters result in 400 response with possible choices <a href=\"https://github.com/dropwizard/dropwizard/pull/1734\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1734\" data-id=\"178353340\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1734</a></p></li><li><p>Enum request parameters are deserialized in the same fuzzy manner, as the request body <a href=\"https://github.com/dropwizard/dropwizard/pull/1734\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1734\" data-id=\"178353340\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1734</a></p></li><li><p>Request parameter name displayed in response to parse failure <a href=\"https://github.com/dropwizard/dropwizard/pull/1734\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1734\" data-id=\"178353340\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1734</a></p></li><li><p>Add DurationParam as a possible request parameter <a href=\"https://github.com/dropwizard/dropwizard/pull/1734\" class=\"issue-link js-issue-link\" data-url=\"https://github.com/dropwizard/dropwizard/issues/1734\" data-id=\"178353340\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1734</a></p></li><li><p>Add SizeParam as a possible request parameter <a href=\"https://github.com/dropwizard/dropwizard/pull/1751\" class=\"issue-link js-issue-link\" data-id=\"180051195\" data-error-text=\"Failed to load issue title\" data-permission-text=\"Issue title is private\">#1751</a></p></li></ul><p>完整更新内容<a href=\"https://github.com/dropwizard/dropwizard/releases/tag/v1.1.0\" target=\"_self\">点此查看</a>。</p><p>下载地址：</p><ul class=\"release-downloads list-paddingleft-2\"><li><p><a href=\"https://github.com/dropwizard/dropwizard/archive/v1.1.0.zip\" rel=\"nofollow\">Source code (zip)</a></p></li><li><p><a href=\"https://github.com/dropwizard/dropwizard/archive/v1.1.0.zip\" rel=\"nofollow\"></a></p><p><a href=\"https://github.com/dropwizard/dropwizard/archive/v1.1.0.tar.gz\" rel=\"nofollow\">Source code (tar.gz)</a></p></li></ul>', 'Dropwizard 1.1.0 发布了，Dropwizard 是一个 Java 框架，用于开发易于运维，高性能的 RESTful 网络服务。它由 Yammer 开发，用于增强基于 JVM 的后端服务，集合了来自 Java 生态系统中的稳定和成熟的程序库，组成了简单，轻量级的程序包，使得用户可以专注于把事情做好。Dropwizard 是开箱即用的，它支持复杂的配置，应用环境， 日志和运维工具，使得......', '10000', '2017-03-24 14:46:34', '2017-03-24 14:46:34', '68', '0', '0', 'Y', '', '', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10090', '0', '10049', '10055', 'Ubuntu 17.04 Beta 2 发布，现可下载', null, '<p>Ubuntu 17.04 Beta 2 发布了，该版本是 Ubuntu 17.04 桌面、服务器和云产品的最终 Beta 版。Ubuntu 17.04 稳定版将于 2017 年 4 月 13 日发布。</p><p>这次发布的 Beta 版不仅包括 Ubuntu 桌面、服务器和云产品，还包括 Kubuntu, Lubuntu, Ubuntu GNOME, Ubuntu Kylin, Ubuntu MATE, Ubuntu Studio 和 Xubuntu flavours，当然还有最近加入 Ubuntu 的 Ubuntu Budgie。</p><p>已更新的默认应用包括 <a data-cke-saved-href=\"http://www.omgubuntu.co.uk/2017/03/firefox-52-released-available-to-download\" href=\"http://www.omgubuntu.co.uk/2017/03/firefox-52-released-available-to-download\" target=\"_blank\">Mozilla Firefox 52</a>, <a data-cke-saved-href=\"https://wiki.documentfoundation.org/ReleaseNotes/5.3\" href=\"https://wiki.documentfoundation.org/ReleaseNotes/5.3\" target=\"_blank\" data-cke-pa-onclick=\"_gaq.push([&apos;_trackEvent&apos;, &apos;outbound-article&apos;, &apos;https://wiki.documentfoundation.org/ReleaseNotes/5.3&apos;, &apos;LibreOffice 5.3&apos;]);\">LibreOffice 5.3</a> 和 Thunderbird 45。</p><h3>Ubuntu Budgie 17.04 Beta 2</h3><p><img data-original=\"http://localhost:8080/upload/201703/1490337995376_p.jpg\" class=\"lazy-load\" /></p><p>对于第二个测试版，发行版调整了一些粗糙的边缘，使整体体验更好一点。“Budgie Welcome” 应用程序已被调整，Terminix 现在被设置为默认终端模拟器。</p><p>默认情况下会安装一堆 GNOME 3.24 应用程序，包括照片和日历。</p><p><a href=\"http://cdimage.ubuntu.com/ubuntu-budgie/releases/17.04/\" target=\"_blank\" textvalue=\"下载 Ubuntu Budgie 17.04 Beta 2\">下载 Ubuntu Budgie 17.04 Beta 2</a><br /></p><h3>Ubuntu GNOME 17.04 Beta 2<br /></h3><p><img data-original=\"http://localhost:8080/upload/201703/1490337997230_p.jpg\" class=\"lazy-load\" /></p><p>默认情况下安装的是 Flatpak 0.8，现在<a data-cke-saved-href=\"http://www.omgubuntu.co.uk/2017/01/install-gnome-shell-extensions-firefox-chrome\" target=\"_blank\" href=\"http://www.omgubuntu.co.uk/2017/01/install-gnome-shell-extensions-firefox-chrome\">使用 Chrome 和 Firefox 安装 GNOME 扩展</a>，需要最新的 <strong>chrome-gnome-shell 系统工具。</strong><br /></p><p><a data-cke-saved-href=\"http://cdimage.ubuntu.com/ubuntu-gnome/releases/17.04/\" target=\"_blank\" href=\"http://cdimage.ubuntu.com/ubuntu-gnome/releases/17.04/\">下载 Ubuntu GNOME 17.04 Beta 2</a></p><h3>Ubuntu MATE 17.04 Beta 2<br /></h3><p><img data-original=\"http://localhost:8080/upload/201703/1490337998544_p.jpg\" class=\"lazy-load\" /></p><p>锁屏幕现在可加载用户选择的背景，还可以在字体查看器中浏览字体。<br /></p><p><a data-cke-saved-href=\"http://cdimage.ubuntu.com/ubuntu-mate/releases/17.04/\" target=\"_blank\" href=\"http://cdimage.ubuntu.com/ubuntu-mate/releases/17.04/\">下载 Ubuntu MATE 17.04 Beta 2</a></p><p><strong>参考链接：</strong></p><ul class=\"list-paddingleft-2\"><li><p><a data-cke-saved-href=\"http://www.omgubuntu.co.uk/2017/03/donwload-ubuntu-17-04-beta-2-flavors\" target=\"_blank\" href=\"http://www.omgubuntu.co.uk/2017/03/donwload-ubuntu-17-04-beta-2-flavors\">http://www.omgubuntu.co.uk/2017/03/donwload-ubuntu-17-04-beta-2-flavors</a></p></li><li><p><a data-cke-saved-href=\"http://www.omgubuntu.co.uk/2017/03/download-ubuntu-17-04-beta-2-release\" target=\"_blank\" href=\"http://www.omgubuntu.co.uk/2017/03/download-ubuntu-17-04-beta-2-release\">http://www.omgubuntu.co.uk/2017/03/download-ubuntu-17-04-beta-2-release</a></p></li><li><p><a href=\"http://www.omgubuntu.co.uk/2017/03/download-ubuntu-17-04-beta-2-release\" target=\"_blank\" textvalue=\"https://lists.ubuntu.com/archives/ubuntu-announce/2017-March/000219.html\">https://lists.ubuntu.com/archives/ubuntu-announce/2017-March/000219.html</a></p></li></ul><h3><strong><a data-cke-saved-href=\"http://releases.ubuntu.com/17.04/\" target=\"_blank\" href=\"http://releases.ubuntu.com/17.04/\">下载 Ubuntu 17.04 Beta 2</a></strong></h3>', 'Ubuntu 17.04 Beta 2 发布了，该版本是 Ubuntu 17.04 桌面、服务器和云产品的最终 Beta 版。Ubuntu 17.04 稳定版将于 2017 年 4 月 13 日发布。这次发布的 Beta 版不仅包括 Ubuntu 桌面、服务器和云产品，还包括 Kubuntu, Lubuntu, Ubuntu GNOME, Ubuntu Kylin, Ubuntu MATE, Ub......', '10000', '2017-03-24 14:46:40', '2017-03-24 14:46:40', '77', '0', '0', 'Y', 'http://localhost:8080/upload/201703/1490337995376_p.jpg|http://localhost:8080/upload/201703/1490337997230_p.jpg|http://localhost:8080/upload/201703/1490337998544_p.jpg|', '201703/1490337995376_p.jpg_s.jpg|201703/1490337997230_p.jpg_s.jpg|201703/1490337998544_p.jpg_s.jpg|', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10091', '0', '10049', '10055', '商城系统 DBShop V1.0 Release 20170323 发布', null, '<p>商城系统 DBShop V1.0 Release 20170323 发布了。</p><p><strong>更新内容：</strong><br /><br />修正 单独商品更新，虚拟销量没有同步更新到商品索引中的问题<br />修正 后台编辑商品 商品品牌设置后不能取消的问题<br />修正 商品品牌编辑，删除品牌logo，依然存在的问题<br />修正 退货商品id应该为订单商品id<br />修正 修正当开启前台登录验证码时，有时会失效的问题<br />修正 在某些时候，余额提现出现的bug<br /><br />优化 对于退货提交内容的审核<br />优化 退货表中 加入订单id<br />优化 第三方登录，如果有提示信息 返回按钮判断 pc端和手机端<br /></p>', '商城系统 DBShop V1.0 Release 20170323 发布了。更新内容：修正 单独商品更新，虚拟销量没有同步更新到商品索引中的问题修正 后台编辑商品 商品品牌设置后不能取消的问题修正 商品品牌编辑，删除品牌logo，依然存在的问题修正 退货商品id应该为订单商品id修正 修正当开启前台登录验证码时，有时会失效的问题修正 在某些时候，余额提现出现的bug优化 对于退货提交内容的审核优化......', '10000', '2017-03-24 14:46:41', '2017-03-24 14:46:41', '81', '0', '0', 'Y', '', '', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10092', '0', '10049', '10055', 'PHPStorm 2017.1 发布，PHP 集成开发环境', null, '<p>PHPStorm无疑是开发PHP项目的神器，秉承着JetBrains家族的优良传统，是用来写世界上最好的语言的不二选择。在今天迎来了最新的2017.1版本，一接到通知邮件，我就迫不及待的更新了新版本，并查看了新特性，整理成中文分享给大家。</p><p><a href=\"https://www.jetbrains.com/phpstorm\" target=\"_blank\" textvalue=\"点击进入 PHPStorm 官网\">点击进入 PHPStorm 官网</a></p><p>本文章转自 <a href=\"https://panlatent.com/archives/phpstorm-2017-1-features\">行间小筑</a>，是根据 <a href=\"https://www.jetbrains.com/phpstorm/whatsnew/#v2017-1-php-class-names-in-strings\">原文</a> 翻译加工而成。</p><p><a href=\"https://blog.jetbrains.com/phpstorm/2017/03/phpstorm-2017-1-is-now-released/\" target=\"_blank\"><strong>PHPStorm 2017.1 发布主页</strong></a><strong> 和 </strong><a href=\"https://www.jetbrains.com/phpstorm/download/\" target=\"_blank\"><strong>下载地址</strong></a></p><h2><strong>PHPStorm 2017.1 中的新功能</strong></h2><h3><strong>亮眼的新封面</strong></h3><p><img data-original=\"http://localhost:8080/upload/201703/1490338001382_p.png\" class=\"lazy-load\" /></p><h3><strong>概览</strong></h3><ul class=\"list-paddingleft-2\"><li><p>Codeception 支持</p></li><li><p>PHPUnit 6 支持</p></li><li><p>参数提示</p></li><li><p>更好的 PRS-0 支持</p></li><li><p>字符串形式的 PHP 类名解析</p></li><li><p>改进 PHP 代码风格</p></li><li><p>更好的自动导入</p></li><li><p>PHP 7 统一变量语法</p></li><li><p>当前作用域突出显示</p></li><li><p>更好的 PHP 7.1 支持</p></li><li><p>新代码高亮</p></li><li><p>PHP 运行时配置</p></li><li><p>新的检查</p></li><li><p>PHP 调试</p></li><li><p>Web 技术</p></li><li><p>IDE 改进</p></li><li><p>数据库工具</p></li></ul><h3><strong>工具和框架支持</strong></h3><h3><strong>支持 Codeception 全栈测试框架</strong></h3><p>PhpStorm 现在支持 Codeception 测试框架（从 2.2.0版本开始）。你能从 Composer 或者 PHAR 安装它。 PhpStorm运行配置将帮助你在本地运行测试, 测试目标可以在一个 Vagrant 虚拟机上, 也可以在远端解释器或者Docker上.</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338005553_p.png\" class=\"lazy-load\" /></p><h3>支持 PHPUnit 6</h3><p>最近，PHPUnit 6 已经发布，现在我们将在PhpStorm中支持它。在新的PhpStorm中，新的命名空间 TestCase 类是被认可和支持的。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338006650_p.png\" class=\"lazy-load\" /></p><h3>改进 Blade 模版引擎的支持</h3><p>这对 Laravel 开发者是一个好消息: PHPDoc 在 Blade injections 中被支持. 你可以在模板内提供变量类型以使用代码补全。. 其他改进包括支持 @includeWhen, @component, 和 @slot directives 嵌套在文档结构中。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338007720_p.png\" class=\"lazy-load\" /></p><h2>编辑体验</h2><h3>参数提示（ Parameter hints ）</h3><p>参数提示是显示在方法的参数值前面的文字，帮助你更好的理解语义的具体方法。我们还增加了一个选项来定制你的内联参数提示，以符合你的IDE配色方案。感谢PhpStorm的参数提示，代码变得更具可读性，更容易理解。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338008789_p.png\" class=\"lazy-load\" /></p><h3>更好的支持 PSR-0 和重命名/重构功能</h3><p>现在，如果你在psr-0根目录使用重命名重构功能，它也改变了你的类的命名空间，反之亦然。此外，重命名／重构功能会重命名类子类和它们的文件。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338009859_p.gif\" class=\"lazy-load\" /></p><blockquote><p>PS: 简单来说就是当你修改命名空间，IDE会帮助你修改目录；当你你修改目录，IDE会帮助你修改命名空间。这种修改也包括子类和它们的文件。</p></blockquote><h3>识别字符串中PHP类名</h3><p>PhpStorm 2017.1 在字符串中识别 PHP 类名。类引用将被添加到单引号风格的字符串文字中，这将帮助您在重命名重构中维护正确的名称，并更快地导航到类。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338010923_p.png\" class=\"lazy-load\" /></p><h3>改进 PHP 代码风格</h3><p>PhpStorm 2017.1 带来了新的有用的代码样式选项，例如:</p><p>会在配置选项里显示当前配置是数据全局（ IDE ）还是项目 （ Project ）<br />声明声明内的可配置空间<br />为构造函数设置括号 ()<br />else if 和 elseif 的相互转换<br />命名空间前的空行</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338011989_p.png\" class=\"lazy-load\" /></p><h3><strong>改进自动导入功能</strong></h3><h3>自动导入函数和常量</h3><p>PhpStorm 2017.1 现在可以自动导入函数和常量。为了使用这一点，去设置选项Editor | General | Auto Import | PHP中启用auto-import在file scope或是namespace scope。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338013053_p.gif\" class=\"lazy-load\" /></p><h3>从全局命名空间自动导入</h3><p>作为从全局命名空间自动导入函数和常量的替代方法，可以让IDE使用全局引用。这个选项在设置选项Editor | General | Auto Import | PHP中启用Prepend functions和constants from the global space with ‘\\’.</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338014117_p.png\" class=\"lazy-load\" /></p><h3><strong>PHP 语言支持</strong></h3><h3>完全支持 PHP 7 统一变量语法</h3><p>PhpStorm 2017.1 根据PHP RFC: Uniform Variable Syntax带来了改进的 PHP 7 统一变量语法。例如isset/unset，foo()()()..， 和$some-&gt;foo()()风格的调用现在被支持。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338015179_p.png\" class=\"lazy-load\" /></p><h3>替代语法的当前范围高亮显示 Current scope highlighting for alternative syntax</h3><p>在当前作用域内，类似于大括号的替代语法语句现在被高亮显示。现在你可以很容易地查看和导航在 pHTML 模板之间的匹配内容。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338016241_p.png\" class=\"lazy-load\" /></p><h3>PHP 7.1: 改进的匿名类支持</h3><p>我们增加了对匿名类的支持，这些类现在有一个适当的类型，这意味着匿名类的方法将被正确地解释。这解放了phpStorm的全部力量，包括代码检查，寻找用法，重命名重构，导航到声明和更多内容。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338017306_p.png\" class=\"lazy-load\" /></p><h3>新代码高亮</h3><p>PhpStorm 2017.1 为所有extended/implemented方法、使用use导入、或是抛出一个异常（例如try,catch, 和@throws）带来了新的醒目的提醒。这样你的代码更直观，更容易掌握重要的细节。要使用此功能，只需将插入extends，implements，use,catch或者@throws。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338018369_p.png\" class=\"lazy-load\" /></p><h3>配置 PHP 运行时</h3><p>在 PhpStorm 2017.1 中，可以配置在项目中使用的 PHP 扩展。这将有助于保持完成列表整洁，只包含相关 PHP 拓展项目。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338019439_p.png\" class=\"lazy-load\" /></p><h3><strong>代码质量分析</strong></h3><h3>检测和转换 PHP 4 样式的构造函数到 PHP 5.3 +</h3><p>因为 PHP 4 的构造函数已经在 PHP 7 中兼容，我们增加了一个新的检查来帮助检测和转换构造函数到 PHP 5.3 以上的风格__construct。您也可以按ALT + Enter进行编辑位置相关的快速修复。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338020508_p.png\" class=\"lazy-load\" /></p><h3>对于未定义变量的快速修复</h3><p>添加了新的快速修复功能帮助解决未定义的变量，将其声明为use在闭包中导入。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338021570_p.png\" class=\"lazy-load\" /></p><h3><strong>PHP 调试</strong></h3><h3>在调试器中将用户定义常量的分组</h3><p>现在，所有用户定义的常量都在变量列表中的常量节点下进行分组，这有助于减少干扰并保持变量列表的整洁。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338023478_p.png\" class=\"lazy-load\" /></p><h3>允许选择 IP 发送到 Zend Debugger</h3><p>PhpStorm 2017.1 自动检测主机的 IP 发送给 Zend Debugger 。当你在具有多个 IP 的机器工作时是特别重要的。如果你使用Vagrant/VirtualBox/VPN. 您也可以禁用复选框，并且手动提供 IP 。这个选项可以在设置选项中的PHP | Debug | Automatically手动设置 IDE IP.</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338024652_p.png\" class=\"lazy-load\" /></p><h3><strong>热门 Web 技术支持</strong></h3><h3>Vue.js 支持</h3><p>享受 Vue 模板语言和您所选择语言的脚本，以及 .vue 文件样式块的编码协助。IDE自动完成 Vue 组件和添加依赖导入。我们刚刚开始！</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338025771_p.png\" class=\"lazy-load\" /></p><h3>集成 Jest</h3><p>感谢新的 Jest 集成，你现在可以从 IDE 运行 Jese 测试，在一个方便的树状视图查看测试结果，从那里轻松地导航到测试源，还调试 Jest 测试。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338026859_p.png\" class=\"lazy-load\" /></p><h3>package.json 模块完成</h3><p>PhpStorm 现在能在项目的package.json文件提取出包名, 并显示描述和新版本。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338027924_p.png\" class=\"lazy-load\" /></p><pre>像往常一样，WebStorm 的所有功能和改进都可以在 PhpStorm 中使用，包括外部或存储库中提供的免费插件。 其他值得注意的 Web 相关功能包括改进的 React 支持，支持 jspm 导入，转换为箭头功能，以帮助您将代码升级到 ECMAScript 6 等等。</pre><h3><strong>IDE 改进</strong></h3><h3>版本控制</h3><p>版本控制附带以下更改:</p><ul class=\"list-paddingleft-2\"><li><p>更新 Git 和 Mercurial 的日志查看器</p></li><li><p>在Diff 对话框中调用Ignore imports and formatting(忽略导入和格式化) 的新选项。</p></li><li><p>性能更快的 Git 文件历史</p><p><br /></p></li></ul><p><img data-original=\"http://localhost:8080/upload/201703/1490338029166_p.png\" class=\"lazy-load\" /></p><h3>路径搜索</h3><p>早先的在Find in Path中，搜索预览选项卡中的路径查找对话框已回炉重做，现在首先显示即时结果。 更重要的是，现在您可以通过按Enter键在编辑器中打开任何选定的结果。 要在工具窗口中查看结果，请单击底部的按钮或按Ctrl Enter（ Mac OS X 的Cmd-Enter）。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338030228_p.png\" class=\"lazy-load\" /></p><h3>数据库工具</h3><h3><strong>数据库工具的改进</strong></h3><p>在 PhpStorm 中的数据库工具已得到改善，感谢我们在 JetBrains datagrip 团队的同事：</p><ul class=\"list-paddingleft-2\"><li><p>CSV 导入中的字段映射和 DDL 预览</p></li><li><p>在 CSV 中导入 С 柱映射和DDL预览</p></li><li><p>使用拖放操作来导出/导入表</p></li><li><p>SQL 解析范围</p></li><li><p>尊重默认搜索路径</p></li></ul><p><img data-original=\"http://localhost:8080/upload/201703/1490338031290_p.png\" class=\"lazy-load\" /></p>', 'PHPStorm无疑是开发PHP项目的神器，秉承着JetBrains家族的优良传统，是用来写世界上最好的语言的不二选择。在今天迎来了最新的2017.1版本，一接到通知邮件，我就迫不及待的更新了新版本，并查看了新特性，整理成中文分享给大家。点击进入 PHPStorm 官网本文章转自 行间小筑，是根据 原文 翻译加工而成。PHPStorm 2017.1 发布主页 和 下载地址PHPStorm 2017......', '10000', '2017-03-24 14:47:12', '2017-03-24 14:47:12', '94', '0', '0', 'Y', 'http://localhost:8080/upload/201703/1490338001382_p.png|http://localhost:8080/upload/201703/1490338005553_p.png|http://localhost:8080/upload/201703/1490338006650_p.png|http://localhost:8080/upload/201703/1490338007720_p.png|http://localhost:8080/upload/201703/1490338008789_p.png|http://localhost:8080/upload/201703/1490338009859_p.gif|http://localhost:8080/upload/201703/1490338010923_p.png|http://localhost:8080/upload/201703/1490338011989_p.png|http://localhost:8080/upload/201703/1490338013053_p.gif|http://localhost:8080/upload/201703/1490338014117_p.png|http://localhost:8080/upload/201703/1490338015179_p.png|http://localhost:8080/upload/201703/1490338016241_p.png|http://localhost:8080/upload/201703/1490338017306_p.png|http://localhost:8080/upload/201703/1490338018369_p.png|http://localhost:8080/upload/201703/1490338019439_p.png|http://localhost:8080/upload/201703/1490338020508_p.png|http://localhost:8080/upload/201703/1490338021570_p.png|http://localhost:8080/upload/201703/1490338023478_p.png|http://localhost:8080/upload/201703/1490338024652_p.png|http://localhost:8080/upload/201703/1490338025771_p.png|http://localhost:8080/upload/201703/1490338026859_p.png|http://localhost:8080/upload/201703/1490338027924_p.png|http://localhost:8080/upload/201703/1490338029166_p.png|http://localhost:8080/upload/201703/1490338030228_p.png|http://localhost:8080/upload/201703/1490338031290_p.png|', '201703/1490338001382_p.png_s.jpg|', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10093', '0', '10049', '10055', 'Angular 4.0.0 正式版发布， Web 前端框架', null, '<p>经历了 6 个 RC 版本之后，Angular 发布了 4.0.0 正式版。不过不要太在意版本号，因为这次发布没有大的变更，主要是 bug 修复，详细如下：</p><h3>Bug 修复</h3><ul class=\"list-paddingleft-2\"><li><p><strong>compiler:</strong> 假定没有匹配的查询为静态 (<a data-cke-saved-href=\"https://github.com/angular/angular/issues/15429\" href=\"https://github.com/angular/angular/issues/15429\">#15429</a>) (<a data-cke-saved-href=\"https://github.com/angular/angular/commit/c8ab5cb\" href=\"https://github.com/angular/angular/commit/c8ab5cb\">c8ab5cb</a>), 关闭 <a data-cke-saved-href=\"https://github.com/angular/angular/issues/15417\" href=\"https://github.com/angular/angular/issues/15417\">#15417</a></p></li><li><p><strong>compiler:</strong> 导出 <code>toString</code> 时正确处理 (<a data-cke-saved-href=\"https://github.com/angular/angular/issues/15430\" href=\"https://github.com/angular/angular/issues/15430\">#15430</a>) (<a data-cke-saved-href=\"https://github.com/angular/angular/commit/0dda01e\" href=\"https://github.com/angular/angular/commit/0dda01e\">0dda01e</a>), 关闭 <a data-cke-saved-href=\"https://github.com/angular/angular/issues/15420\" href=\"https://github.com/angular/angular/issues/15420\">#15420</a></p></li><li><p><strong>platform-browser:</strong> setAttribute 应该使用 xmlns 命名空间 (<a data-cke-saved-href=\"https://github.com/angular/angular/issues/14874\" href=\"https://github.com/angular/angular/issues/14874\">#14874</a>) (<a data-cke-saved-href=\"https://github.com/angular/angular/commit/92084f2\" href=\"https://github.com/angular/angular/commit/92084f2\">92084f2</a>), 关闭 <a data-cke-saved-href=\"https://github.com/angular/angular/issues/14865\" href=\"https://github.com/angular/angular/issues/14865\">#14865</a></p></li><li><p><strong>router:</strong> 当查询参数更改时，应将新数据传递给 Observable (<a data-cke-saved-href=\"https://github.com/angular/angular/issues/15387\" href=\"https://github.com/angular/angular/issues/15387\">#15387</a>) (<a data-cke-saved-href=\"https://github.com/angular/angular/commit/08f2f08\" href=\"https://github.com/angular/angular/commit/08f2f08\">08f2f08</a>), 关闭 <a data-cke-saved-href=\"https://github.com/angular/angular/issues/15290\" href=\"https://github.com/angular/angular/issues/15290\">#15290</a></p></li><li><p>prevent strictNullChecks support until <a data-cke-saved-href=\"https://github.com/angular/angular/issues/15432\" href=\"https://github.com/angular/angular/issues/15432\">#15432</a> is fixed (<a data-cke-saved-href=\"https://github.com/angular/angular/issues/15434\" href=\"https://github.com/angular/angular/issues/15434\">#15434</a>) (<a data-cke-saved-href=\"https://github.com/angular/angular/commit/b800a0c\" href=\"https://github.com/angular/angular/commit/b800a0c\">b800a0c</a>)</p></li></ul><h3><a href=\"https://github.com/angular/angular/blob/master/CHANGELOG.md\" target=\"_blank\"><strong>完整更新日志</strong></a><br /></h3><h3>下载地址</h3><ul class=\"list-paddingleft-2\" style=\"list-style-type: disc;\"><li><p><a data-cke-saved-href=\"https://github.com/angular/angular/archive/4.0.0.zip\" href=\"https://github.com/angular/angular/archive/4.0.0.zip\"><strong>Source code</strong> (zip)</a></p></li><li><p><a data-cke-saved-href=\"https://github.com/angular/angular/archive/4.0.0.tar.gz\" href=\"https://github.com/angular/angular/archive/4.0.0.tar.gz\"><strong>Source code</strong> (tar.gz)</a></p></li></ul>', '经历了 6 个 RC 版本之后，Angular 发布了 4.0.0 正式版。不过不要太在意版本号，因为这次发布没有大的变更，主要是 bug 修复，详细如下：Bug 修复compiler: 假定没有匹配的查询为静态 (#15429) (c8ab5cb), 关闭 #15417compiler: 导出 toString 时正确处理 (#15430) (0dda01e), 关闭 #15420platfor......', '10000', '2017-03-24 14:47:13', '2017-03-24 14:47:13', '71', '0', '0', 'Y', '', '', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10094', '0', '10049', '10055', 'BoPress v1.2.2 发布，Python 后台管理', null, '<p>主要改进如下：</p><ol class=\"list-paddingleft-2\"><li><p>增加 CSRF 验证</p></li><li><p>使用 BoForm 重写个人资料</p></li><li><p>文件上传、支持大文件上传</p></li><li><p>简化选项页创建、选项初始值设置改进，增加 init_form_options 函数</p></li><li><p>角色权限管理 BugFix 及 UI 交互优化</p></li><li><p>其它功能改进及 UI 交互优化</p></li></ol><p>同时提供 Windows、Linux 解压即可运行的绿色压缩包<br /></p><p><a data-cke-saved-href=\"https://git.oschina.net/yezang/BoPress/attach_files\" href=\"https://git.oschina.net/yezang/BoPress/attach_files\" target=\"_blank\">https://git.oschina.net/yezang/BoPress/attach_files</a><br /></p><p><a data-cke-saved-href=\"https://git.oschina.net/yezang/BoPress/attach_files\" href=\"https://git.oschina.net/yezang/BoPress/attach_files\" target=\"_blank\"></a>后台登录帐号 super、密码 super</p>', '主要改进如下：增加 CSRF 验证使用 BoForm 重写个人资料文件上传、支持大文件上传简化选项页创建、选项初始值设置改进，增加 init_form_options 函数角色权限管理 BugFix 及 UI 交互优化其它功能改进及 UI 交互优化同时提供 Windows、Linux 解压即可运行的绿色压缩包https://git.oschina.net/yezang/BoPress/attach......', '10000', '2017-03-24 14:47:13', '2017-03-24 14:47:13', '21', '0', '0', 'Y', '', '', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10095', '0', '10049', '10055', 'UCKeFu v1.1.0 Preview 1 发布，全渠道客服系统', null, '<p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#FFFFFF;\">\r\n UCKeFu v1.0.0 Preview 1 发布，UCKeFu基于JAVA语言开发，是一个全渠道融合的客户支持服务平台，聚合企业内部多个客服渠道，帮助各种行业各种规模的企业建立完整客服体系。<br />通过将邮件、短信、电话语音、WebIM在线客服、微信、微博、H5页面、APP接口等多个渠道来源的客户服务请求与对话汇聚在一个管理平台，用统一的方式来响应和支撑客户服务。</p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#FFFFFF;\"><a href=\"http://git.oschina.net/uploads/images/2017/0315/075025_c1add38b_1200081.png\" target=\"_blank\"><img style=\"width:auto;height:auto;\" data-original=\"http://localhost:8080/upload/201703/1490338034121_p.png\" class=\"lazy-load\" /></a></p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#FFFFFF;\"><br /></p><p><strong>Bug</strong><strong>修正</strong></p><p style=\"margin-left:18.0pt;text-indent:-18.0pt;\">1、  结束对话时抛出错误的问题</p><p><strong>优化的功能：</strong></p><p style=\"margin-left:18.0pt;text-indent:-18.0pt;\">2、  访客标签修改为与UserID关联</p><p style=\"margin-left:18.0pt;text-indent:-18.0pt;\">3、  增加CROSS支持</p><p><strong>新增联系人管理模块和客户管理模块功能：</strong></p><p><br /></p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#FFFFFF;\"></p><p style=\"text-align:justify;color:#40485B;font-family:-apple-system, &quot;font-size:15px;background-color:#FFFFFF;\">\r\n 优客服项目模块，联系人管理和客户管理功能（Elasticsearch版本）</p><p style=\"text-align:justify;color:#40485B;font-family:-apple-system, &quot;font-size:15px;background-color:#FFFFFF;\">\r\n 联系人管理模块码云地址：<a href=\"https://git.oschina.net/ukewo/UCKeFu-Contacts\" target=\"_blank\">https://git.oschina.net/ukewo/UCKeFu-Contacts</a></p><p style=\"text-align:justify;color:#40485B;font-family:-apple-system, &quot;font-size:15px;background-color:#FFFFFF;\"><strong><a href=\"http://git.oschina.net/uploads/images/2017/0323/132214_b2001a4d_1200081.png\" target=\"_blank\"><img title=\"在这里输入图片标题\" data-original=\"http://localhost:8080/upload/201703/1490338035699_p.png\" class=\"lazy-load\" /></a><a href=\"http://git.oschina.net/uploads/images/2017/0323/132227_258a660c_1200081.png\" target=\"_blank\"><img title=\"在这里输入图片标题\" data-original=\"http://localhost:8080/upload/201703/1490338037637_p.png\" class=\"lazy-load\" /></a><a href=\"http://git.oschina.net/uploads/images/2017/0323/132242_246df11e_1200081.png\" target=\"_blank\"><img title=\"在这里输入图片标题\" data-original=\"http://localhost:8080/upload/201703/1490338039404_p.png\" class=\"lazy-load\" /></a><a href=\"http://git.oschina.net/uploads/images/2017/0323/132303_e5cd9105_1200081.png\" target=\"_blank\"><img title=\"在这里输入图片标题\" data-original=\"http://localhost:8080/upload/201703/1490338041088_p.png\" class=\"lazy-load\" /></a></strong></p><p style=\"text-align:justify;color:#40485B;font-family:-apple-system, &quot;font-size:15px;background-color:#FFFFFF;\"><strong><a href=\"http://git.oschina.net/uploads/images/2017/0323/132320_81ae13f3_1200081.png\" target=\"_blank\"><img title=\"在这里输入图片标题\" data-original=\"http://localhost:8080/upload/201703/1490338042623_p.png\" class=\"lazy-load\" /></a></strong></p>', ' UCKeFu v1.0.0 Preview 1 发布，UCKeFu基于JAVA语言开发，是一个全渠道融合的客户支持服务平台，聚合企业内部多个客服渠道，帮助各种行业各种规模的企业建立完整客服体系。通过将邮件、短信、电话语音、WebIM在线客服、微信、微博、H5页面、APP接口等多个渠道来源的客户服务请求与对话汇聚在一个管理平台，用统一的方式来响应和支撑客户服务。Bug修正1、  结束对话时抛出错误......', '10000', '2017-03-24 14:47:24', '2017-03-24 14:47:24', '79', '0', '0', 'Y', 'http://localhost:8080/upload/201703/1490338034121_p.png|http://localhost:8080/upload/201703/1490338035699_p.png|http://localhost:8080/upload/201703/1490338037637_p.png|http://localhost:8080/upload/201703/1490338039404_p.png|http://localhost:8080/upload/201703/1490338041088_p.png|http://localhost:8080/upload/201703/1490338042623_p.png|', '201703/1490338034121_p.png_s.jpg|201703/1490338035699_p.png_s.jpg|201703/1490338037637_p.png_s.jpg|', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10096', '0', '10049', '10055', 'ZUI 发布 1.6 版本，开源HTML5跨屏UI框架', null, '<p>\r\n 大家好，我们很高兴的向大家推出ZUI的1.6版本。ZUI是我们团队在开发其他几款开源软件过程中衍生出来的一款跨屏HTML5框架。基础的理念基于bootstrap，又结合我们自己实际的应用做了大量的整合工作。我们团队开发的禅道项目管理软件、蝉知cms系统、然之OA系统都是使用的ZUI框架，并得到了大量一线用户的检验。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338045201_p.jpg\" class=\"lazy-load\" /></p><h3>\r\n v 1.6.0</h3><h4>\r\n 更新明细</h4><ul class=\"list-paddingleft-2\"><li><p>\r\n  文件上传：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n      提供多种预设的文件列表界面，包括普通列表、大号列表及网格；     </p></li><li><p>\r\n      支持大文件分片上传；     </p></li><li><p>\r\n      支持显示文件上传进度及上传速度等状态信息；     </p></li><li><p>\r\n      支持自定义界面模板，灵活定制个性化的文件上传界面；     </p></li><li><p>\r\n      支持拖拽文件上传；     </p></li><li><p>\r\n      支持文件队列，可以自动或手动上传文件；     </p></li><li><p>\r\n      支持七牛 js-sdk 上传文件，只需要简单配置；     </p></li><li><p>\r\n      支持图片文件缩略图及显示文件的个性图标；     </p></li><li><p>\r\n      支持过滤图片文件大小及格式、支持预处理图片文件（包括缩放、裁剪等）；     </p></li><li><p>\r\n      支持 HTML5、flash、serverlight 等形式上传文件；     </p></li><li><p>\r\n      支持对服务器结果进行识别，包括上传结果及自动为下载按钮应用下载地址；     </p></li><li><p>\r\n      支持在文件列表显示静态文件；     </p></li><li><p>\r\n    新增文件上传视图，支持如下特色功能：    </p></li></ul><li><p>\r\n  图标：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    新增了一些字体图标，包括.icon-database、.icon-eraser、.icon-strikethrouth、.icon-underline、.icon-header、.icon-italic；   </p></li><li><p>\r\n    修复了图标旋转辅助类在低版本 IE 中无效的问题；   </p></li></ul><li><p>\r\n  开关控件：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    新增开关控件.switch，详情访问 <a href=\"http://zui.sexy/#control/switch\">http://zui.sexy/#control/switch</a> ；   </p></li></ul><li><p>\r\n  文档网站：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    修正当页面打开对话框后按ESC键导致文档页面关闭而对话框背景层还存在导致页面不可用的问题；   </p></li><li><p>\r\n    修正了文档上一些资源的路径，现在文档支持使用网址的二级目录形式访问；   </p></li><li><p>\r\n    文档上了示例代码都增加了语言类型提示；   </p></li></ul><li><p>\r\n  标签页导航：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    新增垂直的标签页式导航，使用.nav.nav-tabs.nav-stacked实现；   </p></li></ul><li><p>\r\n  垂直菜单：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    修复垂直菜单初始状态下展开折叠指示图标不正确的问题；   </p></li><li><p>\r\n    垂直菜单已不建议使用，现在可以使用树形菜单.tree.tree-menu来实现垂直菜单的功能，详情参见文档 <a href=\"http://zui.sexy/#view/tree/1\">视图 → 树形菜单 → 树形导航菜单</a>；   </p></li></ul><li><p>\r\n  树形菜单：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    新增树形导航菜单外观，使用.tree.tree-menu实现；   </p></li><li><p>\r\n    修复了.tree-lines在一些情况下连接线不连贯或超出的问题；   </p></li></ul><li><p>\r\n  图表：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    更改了配置项multiTooltipTemplate的默认值，现在默认情况下会显示数据值所属的数据表名称；   </p></li><li><p>\r\n    优化饼图上的标签显示策略，现在标签会自动选择合适的位置显示，不会重叠在一起；   </p></li></ul><li><p>\r\n  拖拽：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    重做了拖拽插件，支持一次性初始化多个被拖拽元素（包括动态加入的元素），提升了性能；   </p></li></ul><li><p>\r\n  拖放：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    重做了拖放插件，支持为一组元素（包括动态加入的元素）的拖放操作，提升了性能；   </p></li></ul><li><p>\r\n  拖放排序：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    重构了拖放排序插件，提升了插件性能，解决了在多次调用后导致卡顿的问题；   </p></li></ul><li><p>\r\n  拖拽选取：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    修复了有时finish事件没有被触发的问题；   </p></li><li><p>\r\n    修复了在极端情况下mouseup事件没有触发导致选择框无法消失的问题；   </p></li><li><p>\r\n    提升了拖拽选取交互过程中的性能；   </p></li></ul><li><p>\r\n  看板：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    提升了性能，修复了看板上的条目在拖动时重复绑定事件的问题；   </p></li></ul><li><p>\r\n  日历：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    提升了性能，修复了在某些极端情况下导致无法放置拖放的日历事件的问题；   </p></li></ul><li><p>\r\n  仪表盘：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    仪表盘现在支持通过拖拽面板底部边缘来调整面板的高度，在同一行上的面板会在其中一个高度变化时自动调整高度来保持一致；   </p></li><li><p>\r\n    提升了插件运行效率；   </p></li></ul><li><p>\r\n  提示消息：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    增加了tipClass和tipId选项；   </p></li><li><p>\r\n    增加多种颜色主题，可以通过tipClass来指定；   </p></li></ul><li><p>\r\n  弹出面板：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    增加了多种颜色主题；   </p></li><li><p>\r\n    修复了在手动调用$().tooltip(&apos;hide&apos;)之后，没有从页面移除动态生成的.tooltip元素的问题；   </p></li></ul><li><p>\r\n  下拉菜单：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    增加.dropdown-hover类来实现不需要点击触发按钮只需要鼠标悬停就可以展开下拉菜单面板；   </p></li></ul><li><p>\r\n  文字排版：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    有序列表增加.ol-pd-2、.ol-pd-3、.ol-pd-4特殊类来修正左侧边距，并提供了一个 JS 方法$().fixOlPd()来手动修正；   </p></li><li><p>\r\n    &lt;hr&gt;元素增加了.divider和.divider-sm类获得不同外边距的分割线；   </p></li><li><p>\r\n    为 MacOS 增加苹方字体；   </p></li></ul><li><p>\r\n  漂浮消息：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    修复了当指定了自定义操作按钮及onAction事件回调函数时提示result未定义大问题；   </p></li><li><p>\r\n    修复了方法名命名错误，destory更正为destroy；   </p></li><li><p>\r\n    修复了使用new方法创建实例时报错的问题（issue #32）；   </p></li></ul><li><p>\r\n  富文本编辑器：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    重构了打包任务，修复了在dist目录下没有包含插件资源的问题；   </p></li><li><p>\r\n    修复了在一些浏览器上对话框上的按钮在英文界面上文字显示不全的问题；   </p></li></ul><li><p>\r\n  日期选择器：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    修复了在点击清空按钮后，再次弹出日期面板上的左右切换日期按钮失效的问题；   </p></li><li><p>\r\n    新增eleClass和eleId选项来为动态生成的日期选择面板元素指定 CLASS 和 ID 属性；   </p></li></ul><li><p>\r\n  颜色选择器：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    修复了updateColors()和addColor()方法无法直接添加字符串形式的颜色值的问题；   </p></li><li><p>\r\n    修复了在 jQuery 3.x+ 上报错的问题；   </p></li></ul><li><p>\r\n  Chosen：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    增加middle_hightlight选择来让打开下拉面板时选中的选项尽量列表中间位置；   </p></li></ul><li><p>\r\n  主题：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    修复了在主题中为面板组和标签设置圆角值无效的问题；   </p></li></ul><li><p>\r\n  其他：  </p></li><ul class=\"list-paddingleft-2\" style=\"list-style-type: square;\"><li><p>\r\n    .show辅助类会使用!important修饰，避免在一些情况下失效；   </p></li><li><p>\r\n    增加辅助方法$.zui.strCode(str)来将一个字符串转换为一个唯一的数值，任何字符串都对应一个唯一的整数；   </p></li></ul></ul><p>\r\n 具体的文档请访问ZUI的官网：<a href=\"http://zui.sexy\" target=\"_blank\">http://zui.sexy</a> (请放心点击，有惊喜。)</p>', ' 大家好，我们很高兴的向大家推出ZUI的1.6版本。ZUI是我们团队在开发其他几款开源软件过程中衍生出来的一款跨屏HTML5框架。基础的理念基于bootstrap，又结合我们自己实际的应用做了大量的整合工作。我们团队开发的禅道项目管理软件、蝉知cms系统、然之OA系统都是使用的ZUI框架，并得到了大量一线用户的检验。 v 1.6.0 更新明细  文件上传：        提供多种预设的文件列表界面......', '10000', '2017-03-24 14:47:26', '2017-03-24 14:47:26', '79', '0', '0', 'Y', 'http://localhost:8080/upload/201703/1490338045201_p.jpg|', '201703/1490338045201_p.jpg_s.jpg|', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10097', '0', '10049', '10055', 'Presto 0.170 发布，Facebook 开源的大数据查询引擎  ', null, '<p>Presto 0.170 发布了，Presto 是 Facebook 开源的数据查询引擎，可对250PB以上的数据进行快速地交互式分析，查询的速度达到商业数据仓库的级别。据称该引擎的性能是 <a data-cke-saved-href=\"http://www.oschina.net/p/hive\" href=\"http://www.oschina.net/p/hive\" target=\"_blank\">Hive</a> 的 10 倍以上。</p><p>Presto 可以查询包括 Hive、Cassandra 甚至是一些商业的数据存储产品。单个 Presto 查询可合并来自多个数据源的数据进行统一分析。</p><p>更新如下：<br /></p><h2>General Changes</h2><ul class=\"list-paddingleft-2\"><li><p>Fix race condition that could cause queries to fail with InterruptedException in rare cases.</p></li><li><p>Fix a performance regression for GROUP BY queries over UNION.</p></li><li><p>Fix a performance regression that occurs when a significant number of exchange sources produce no data during an exchange (e.g., in a skewed hash join).</p></li></ul><h2>Web UI Changes</h2><ul class=\"list-paddingleft-2\"><li><p>Fix broken rendering when catalog properties are set.</p></li><li><p>Fix rendering of live plan when query is queued.</p></li></ul><p>完整更新内容请查看<a href=\"https://prestodb.io/docs/current/release/release-0.170.html\" target=\"_self\">发布说明</a>。<br /></p><p>下载地址：</p><ul class=\"release-downloads list-paddingleft-2\"><li><p><a href=\"https://github.com/prestodb/presto/archive/0.170.zip\" rel=\"nofollow\">Source code (zip)</a></p></li><li><p><a href=\"https://github.com/prestodb/presto/archive/0.170.zip\" rel=\"nofollow\"></a></p><p><a href=\"https://github.com/prestodb/presto/archive/0.170.tar.gz\" rel=\"nofollow\">Source code (tar.gz)</a></p></li></ul>', 'Presto 0.170 发布了，Presto 是 Facebook 开源的数据查询引擎，可对250PB以上的数据进行快速地交互式分析，查询的速度达到商业数据仓库的级别。据称该引擎的性能是 Hive 的 10 倍以上。Presto 可以查询包括 Hive、Cassandra 甚至是一些商业的数据存储产品。单个 Presto 查询可合并来自多个数据源的数据进行统一分析。更新如下：General Ch......', '10000', '2017-03-24 14:47:27', '2017-03-24 14:47:27', '63', '0', '0', 'Y', '', '', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10098', '0', '10049', '10055', 'xmake v2.1.2 版本更新，增强 Visual Studio 工程支持', null, '<h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 概述</h2><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n 此版本主要增强了vs201x工程的生成，以及支持vs2017编译环境，并且针对archlinux提供更加方便的aur安装。</p><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p><a href=\"http://www.xmake.io/cn/\" target=\"_blank\">项目主页</a></p></li><li><p><a href=\"http://www.xmake.io/#zh/\" target=\"_blank\">查看文档</a></p></li></ul><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n 详细更新内容如下：</p><h3 style=\"font-weight:normal;font-size:26px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 新特性</h3><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p>\r\n  添加aur打包脚本，并支持用yaourt包管理器进行安装。 </p></li><li><p>\r\n  添加<a href=\"http://tboox.org/cn/2017/03/23/update-v2.1.2/#http://xmake.io/#/zh/manual?id=targetset_basename\" target=\"_blank\">set_basename</a>接口，便于定制化修改生成后的目标文件名 </p></li></ul><h3 style=\"font-weight:normal;font-size:26px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 改进</h3><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p>\r\n  支持vs2017编译环境 </p></li><li><p>\r\n  支持编译android版本的rust程序 </p></li><li><p>\r\n  增强vs201x工程生成插件，支持同时多模式、架构编译 </p></li></ul><h3 style=\"font-weight:normal;font-size:26px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n Bugs修复</h3><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p>\r\n  修复编译android程序，找不到系统头文件问题 </p></li><li><p>\r\n  修复检测选项行为不正确问题 </p></li><li><p><a href=\"https://github.com/tboox/xmake/issues/57\" target=\"_blank\">#57</a>: 修复代码文件权限到0644 </p></li></ul><h3 style=\"font-weight:normal;font-size:26px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 构建演示</h3><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\"><a href=\"https://github.com/tboox/xmake\" target=\"_blank\"><img data-original=\"http://localhost:8080/upload/201703/1490338048108_p.gif\" class=\"lazy-load\" /></a></p><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 简介</h2><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n XMake是一个基于Lua的轻量级跨平台自动构建工具，支持在各种主流平台上构建项目</p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n xmake的目标是开发者更加关注于项目本身开发，简化项目的描述和构建，并且提供平台无关性，使得一次编写，随处构建</p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n 它跟cmake、automake、premake有点类似，但是机制不同，它默认不会去生成IDE相关的工程文件，采用直接编译，并且更加的方便易用 采用lua的工程描述语法更简洁直观，支持在大部分常用平台上进行构建，以及交叉编译</p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n 并且xmake提供了创建、配置、编译、打包、安装、卸载、运行等一些actions，使得开发和构建更加的方便和流程化。</p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n 不仅如此，它还提供了许多更加高级的特性，例如插件扩展、脚本宏记录、批量打包、自动文档生成等等。。</p><p style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\">\r\n 如果你想要了解更多，请参考：</p><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p><a href=\"http://xmake.io/#/zh/\" target=\"_blank\">在线文档</a></p></li><li><p><a href=\"https://github.com/waruqi/xmake\" target=\"_blank\">在线源码</a></p></li><li><p><a href=\"http://www.xmake.io/cn\" target=\"_blank\">项目主页</a></p></li></ul><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 简单的工程描述</h2><pre class=\"highlight\">target(&quot;console&quot;)\r\n    set_kind(&quot;binary&quot;)\r\n    add_files(&quot;src/*.c&quot;)</pre><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 构建工程</h2><pre class=\"highlight\">$ xmake</pre><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 运行目标</h2><pre class=\"highlight\">$ xmake run console</pre><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 调试程序</h2><pre class=\"highlight\">$ xmake run -d console</pre><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 支持特性</h2><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p>\r\n  Tasks </p></li><li><p>\r\n  Macros </p></li><li><p>\r\n  Actions </p></li><li><p>\r\n  Options </p></li><li><p>\r\n  Plugins </p></li><li><p>\r\n  Templates </p></li></ul><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 支持平台</h2><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p>\r\n  Windows (x86, x64, amd64, x86_amd64) </p></li><li><p>\r\n  Macosx (i386, x86_64) </p></li><li><p>\r\n  Linux (i386, x86_64, cross-toolchains …) </p></li><li><p>\r\n  Android (armv5te, armv6, armv7-a, armv8-a, arm64-v8a) </p></li><li><p>\r\n  iPhoneOS (armv7, armv7s, arm64, i386, x86_64) </p></li><li><p>\r\n  WatchOS (armv7k, i386) </p></li><li><p>\r\n  Mingw (i386, x86_64) </p></li></ul><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 支持语言</h2><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p>\r\n  C/C++ </p></li><li><p>\r\n  Objc/Objc++ </p></li><li><p>\r\n  Swift </p></li><li><p>\r\n  Assembly </p></li><li><p>\r\n  Golang </p></li><li><p>\r\n  Rust </p></li><li><p>\r\n  Dlang </p></li></ul><h2 style=\"font-weight:normal;font-size:30px;color:#333333;font-family:&quot;background-color:#F8F8FD;\">\r\n 内置插件</h2><ul style=\"color:#333333;font-family:&quot;font-size:16px;background-color:#F8F8FD;\" class=\"list-paddingleft-2\"><li><p>\r\n  宏记录脚本和回放插件 </p></li><li><p>\r\n  加载自定义lua脚本插件 </p></li><li><p>\r\n  生成IDE工程文件插件（makefile, vs2002 - vs2017, …） </p></li><li><p>\r\n  生成doxygen文档插件 </p></li><li><p>\r\n  iOS app2ipa插件 </p></li></ul>', ' 概述 此版本主要增强了vs201x工程的生成，以及支持vs2017编译环境，并且针对archlinux提供更加方便的aur安装。项目主页查看文档 详细更新内容如下： 新特性  添加aur打包脚本，并支持用yaourt包管理器进行安装。   添加set_basename接口，便于定制化修改生成后的目标文件名  改进  支持vs2017编译环境   支持编译android版本的rust程序   增强......', '10000', '2017-03-24 14:47:33', '2017-03-24 14:47:33', '90', '0', '0', 'Y', 'http://localhost:8080/upload/201703/1490338048108_p.gif|', '201703/1490338048108_p.gif_s.jpg|', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10099', '0', '10049', '10055', '4月15-16日，武汉、长沙源创会报名正式启动！', null, '<p>听说四月樱花开，结束深圳大本营，源创会将再次来到武汉和长沙，秉承“自由、开放、分享”的开源精神，邀各位在花开的季节来一场线下面基。</p><p><strong>4月15日武汉源创会报名及详情请点击</strong>：<a href=\"https://www.oschina.net/event/2235307\" target=\"_self\"><strong>https://www.oschina.net/event/2235307</strong></a></p><p><strong>4月16日长沙源创会报名及详情请点击：<a href=\"https://www.oschina.net/event/2235322\" target=\"_self\">https://www.oschina.net/event/2235322</a></strong></p><p><strong><strong>2017年源创会全面计划看这里：<a data-cke-saved-href=\"https://www.oschina.net/news/79275/osc-2017-yuanchuanghui-plan\" href=\"https://www.oschina.net/news/79275/osc-2017-yuanchuanghui-plan\"></a></strong><a data-cke-saved-href=\"https://www.oschina.net/news/79275/osc-2017-yuanchuanghui-plan\" href=\"https://www.oschina.net/news/79275/osc-2017-yuanchuanghui-plan\">https://www.oschina.net/news/79275/osc-2017-yuanchuanghui-plan</a></strong></p><p><strong><img data-original=\"http://localhost:8080/upload/201703/1490338054147_p.png\" class=\"lazy-load\" /></strong></p><h3><strong>一、4月15日武汉源创会主题<br /></strong></h3><p><strong>1.Apache Kylin 2.0 之Spark构建引擎</strong></p><p><strong>主题简介</strong>：作为领先的大数据OLAP分析平台，Apache Kylin 正在步入2.0时代：从星型模型到雪花模型的转变、更加全面的SQL语法、TPC-H 的支持、更加先进的构建引擎Spark Cubing、更好地支持实时流式数据接入等等。Apache Kylin正逐渐从Hadoop上的传统OLAP演变为一个实时数据仓库。尤其是新引入的Spark Cubing构建引擎，大幅度提升了Cube预计算过程的效率。本次演讲将结合实际案例，重点介绍这一技术的设计思考和最佳实践。</p><p><strong>分享嘉宾</strong>：李栋，Kyligence技术合伙人兼高级软件架构师，Apache Kylin Committer &amp; PMC Member，专注于大数据技术研发，KyBot技术负责人。毕业于上海交通大学计算机系；曾任eBay全球分析基础架构部高级工程师、微软云计算和企业产品部软件开发工程师；曾是微软商业产品Dynamics亚太团队核心成员，参与开发了新一代基于云端的ERP解决方案。</p><p><strong>2.Linux桌面操作系统是怎样实现的？</strong></p><p><strong>主题简介</strong>：Unix 和 XFreeDesktop 相关的知识已经随着老一代Linux黑客销声匿迹，希望通过剖析深度桌面环境的技术架构来给各位同学普及一下 Linux 桌面操作系统的实现原理。</p><p><strong>分享嘉宾</strong>：王勇，Deepin操作系统的研发负责人和第一个码农，上班主职产品经理（开发痛恨的产品狗）和编写各项目产品需求文档，下班为了爱好敲代码造轮子， 平常兼职对销售做技术支持和各地旅游。 Emacs中国区的狂热开发者、Gtk2hs核心开发者、Haskell和函数式语言开源贡献者、对图形研究比较深入, 擅长于用 xcb、cairo、gtk、qt编写各种图形化程序。</p><p><strong>3.Caas与Iaas网络融合实践</strong></p><p><strong>主题简介</strong>：Caas作为云服务的新兵，通常情况下都是构建在Iaas之上的。常见的场景就是在OpenStack上部署Caas，OpenStack本身拥有自己的SDN，Caas也具有一定SDN的能力，两层的SDN可能会导致相互掣肘，本议题关注在如何提升这种场景下的Caas网络性能。</p><p><strong>分享嘉宾</strong>：张智博，Rancher Labs资深软件工程师。昵称牛小腩，初出茅庐在阿里巴巴口碑网，参与本地搜索业务研发工作；后联合创办美食点评社区“美食行”，之后长期从事云计算研发工作。Rancher中国社区布道师，MBH树莓派社区成员，OpenStack中国社区长期作者。</p><p><strong>4.EasyApp——安卓开发效率改进新思路？</strong></p><p><strong>主题简介</strong>：本主题以EasyApp为主要例子，分享中小型安卓App开发过程中经历的痛点和解决方案，以及如何通过转变思维方式来改进开发效率。</p><p><strong>分享嘉宾</strong>：霍启圣 斗鱼移动端安卓创新组主管，EasyApp开源项目发起人，九年互联网从业经历，曾担任DotA汉化组组长、供职于斗鱼，对安卓、React/React Native、WP开发有着丰富的经验积累和技术成果。</p><p><strong>#放码过来#</strong>2017年源创会新增环节，个人或团队如有优秀开源项目，请发送站内私信到<a href=\"https://my.oschina.net/Ni-XZ\" target=\"_blank\" rel=\"nofollow\"></a><a href=\"https://my.oschina.net/Ni-XZ\" class=\"referer\" target=\"_blank\">@凝小紫</a> 报名，通过筛选的项目即可在源创会现场上台展示个人项目，更有机会获得开源中国社区码云项目推荐，甚至登上码云周刊的机会，只要你有料，就尽管放码过来！</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338057711_p.png\" class=\"lazy-load\" /><strong><br /></strong></p><h3>二、4月16日长沙源创会主题</h3><p><strong>1.Apache Kylin 2.0 之Spark构建引擎</strong></p><p><strong>主题简介</strong>：作为领先的大数据OLAP分析平台，Apache Kylin 正在步入2.0时代：从星型模型到雪花模型的转变、更加全面的SQL语法、TPC-H 的支持、更加先进的构建引擎Spark Cubing、更好地支持实时流式数据接入等等。Apache Kylin正逐渐从Hadoop上的传统OLAP演变为一个实时数据仓库。尤其是新引入的Spark Cubing构建引擎，大幅度提升了Cube预计算过程的效率。本次演讲将结合实际案例，重点介绍这一技术的设计思考和最佳实践。</p><p><strong>分享嘉宾</strong>：李栋，Kyligence技术合伙人兼高级软件架构师，Apache Kylin Committer &amp; PMC Member，专注于大数据技术研发，KyBot技术负责人。毕业于上海交通大学计算机系；曾任eBay全球分析基础架构部高级工程师、微软云计算和企业产品部软件开发工程师；曾是微软商业产品Dynamics亚太团队核心成员，参与开发了新一代基于云端的ERP解决方案。</p><p><strong>2.一个超大规模的搜索引擎背后的故事</strong></p><p><strong>主题简介</strong>：我们是如何一步一步对分布式索引进行扩容和性能优化的。也许，你可以根据这次技术交流，打造出自己的Google搜索引擎。</p><p><strong>分享嘉宾</strong>：何小成，就职于湖南蚁坊软件股份有限公司平台部。主要从事超大规模分布式搜索引擎的平台搭建及调优等工作。实现在高并发、每天7亿+新数据入库的情况下，对PB级的数据做快速搜索和统计。深度剖析过Solr、ElasticSearch、Lucene 等开源索引框架结构以及其性能调优的工作。</p><p><strong>3.UKUI轻量级桌面环境</strong></p><p><strong>主题简介</strong>：UKUI是一个轻量级的Linux桌面环境，基于GTK和QT进行开发，在国内外得到广泛关注，已经默认使用在优麒麟开源操作系统上。本次报告重点介绍UKUI的设计理念、开发进度以及后续计划。</p><p><strong>分享嘉宾</strong>：余杰(Jack Yu)，国防科技大学博士、优麒麟开源项目负责人，长期从事Linux相关开发和推广活动。</p><p><strong>4.一种可视化的Kubernetes管理工具介绍</strong></p><p><strong>主题简介</strong>：初学K8s通常会碰到部署难管理难等诸多问题，GFW着实增加了不少游戏难度。Rancher实现了一键可视化的部署方式，同时完美绕开GFW的问题，多节点管理的时候也会更加得心应手。本议题会分享一些背后的实现原理和相关实践指南，同时分享如何个性化定制部署过程。</p><p><strong>分享嘉宾</strong>：张智博，Rancher Labs资深软件工程师。昵称牛小腩，初出茅庐在阿里巴巴口碑网，参与本地搜索业务研发工作；后联合创办美食点评社区“美食行”，之后长期从事云计算研发工作。Rancher中国社区布道师，MBH树莓派社区成员，OpenStack中国社区长期作者。</p><p><strong>#放码过来#</strong>2017年源创会新增环节，个人或团队如有优秀开源项目，请发送站内私信到<a href=\"https://my.oschina.net/Ni-XZ\" target=\"_blank\" rel=\"nofollow\"></a><a href=\"https://my.oschina.net/Ni-XZ\" class=\"referer\" target=\"_blank\">@凝小紫</a> 报名，通过筛选的项目即可在源创会现场上台展示个人项目，更有机会获得开源中国社区码云项目推荐，甚至登上码云周刊的机会，只要你有料，就尽管放码过来！</p>', '听说四月樱花开，结束深圳大本营，源创会将再次来到武汉和长沙，秉承“自由、开放、分享”的开源精神，邀各位在花开的季节来一场线下面基。4月15日武汉源创会报名及详情请点击：https://www.oschina.net/event/22353074月16日长沙源创会报名及详情请点击：https://www.oschina.net/event/22353222017年源创会全面计划看这里：https:/......', '10000', '2017-03-24 14:47:39', '2017-03-24 14:47:39', '93', '0', '0', 'Y', 'http://localhost:8080/upload/201703/1490338054147_p.png|http://localhost:8080/upload/201703/1490338057711_p.png|', '201703/1490338054147_p.png_s.jpg|201703/1490338057711_p.png_s.jpg|', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10100', '0', '10049', '10055', 'Brackets 1.9 发布，Web 前端开发 IDE', null, '<p>Brackets 1.9 发布了。Brackets 是 Adobe 的开源 HTML/CSS/JavaScript 集成开发环境。Brackets 当前为 Mac, Windows 以及 Linux (Debian/Ubuntu)提供最新稳定版的二进制发布, 源代码托管在 GitHub 上。</p><p><img data-original=\"http://localhost:8080/upload/201703/1490338060676_p.png\" class=\"lazy-load\" /></p><p><strong>Brackets 1.9 的变化包括：</strong><br /></p><ul class=\"list-paddingleft-2\"><li><p>在实时预览中反向检查（点击实时预览中的元素，高亮显示源代码中的相应标签）</p></li><li><p>现已支持 “查找和替换” 中的 “全部替换” 以及批处理操作</p></li><li><p>扩展管理器现在显示列出的扩展名的下载次数，并允许根据下载次数或发布日期进行排序。因此你可以轻松找到最受欢迎和最新的扩展功能</p></li><li><p>现在可以使用键盘快捷键（Alt + w）在窗格之间交换焦点</p></li><li><p>现在可以更改无标题文档的语言模式（而且这些文档支持代码着色和代码提示）</p></li><li><p>GitHub 组织现在可以拥有 Brackets 扩展并更新它们</p></li></ul><p><a data-cke-saved-href=\"https://github.com/adobe/brackets/releases/tag/release-1.9\" target=\"_blank\" href=\"https://github.com/adobe/brackets/releases/tag/release-1.9\"><strong>发布主页</strong></a><strong> 和 </strong><a data-cke-saved-href=\"https://github.com/adobe/brackets/wiki/Release-Notes:-1.9\" href=\"https://github.com/adobe/brackets/wiki/Release-Notes:-1.9\" target=\"_blank\"><strong>完整更新日志</strong></a><br /></p><p>值得注意的是 Linux 上的 Brackets 有两个问题：<br /></p><ul class=\"list-paddingleft-2\" style=\"list-style-type: disc;\"><li><p>要关闭应用程序，必须单击关闭按钮两次。<br /></p></li><li><p>官方的 Brackets Debian/Ubuntu debs 依赖于 libgcrypt11，这在高于 14.10 的 Ubuntu 版本中不可用。<br /></p></li></ul><p>下载地址：</p><ul class=\"list-paddingleft-2\"><li><p><a data-cke-saved-href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.32-bit.deb\" href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.32-bit.deb\">Brackets.Release.1.9.32-bit.deb</a></p></li><li><p><a data-cke-saved-href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.64-bit.deb\" href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.64-bit.deb\">Brackets.Release.1.9.64-bit.deb</a></p></li><li><p><a data-cke-saved-href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.dmg\" href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.dmg\">Brackets.Release.1.9.dmg</a></p></li><li><p><a data-cke-saved-href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.msi\" href=\"https://github.com/adobe/brackets/releases/download/release-1.9/Brackets.Release.1.9.msi\">Brackets.Release.1.9.msi</a></p></li><li><p><a data-cke-saved-href=\"https://github.com/adobe/brackets/archive/release-1.9.zip\" href=\"https://github.com/adobe/brackets/archive/release-1.9.zip\">Source code (zip)</a></p></li><li><p><a data-cke-saved-href=\"https://github.com/adobe/brackets/archive/release-1.9.tar.gz\" href=\"https://github.com/adobe/brackets/archive/release-1.9.tar.gz\">Source code (tar.gz)</a></p></li></ul><p>参考链接：<a href=\"http://www.webupd8.org/2017/03/code-editor-brackets-19-released.html\" target=\"_blank\" textvalue=\"http://www.webupd8.org/2017/03/code-editor-brackets-19-released.html\">http://www.webupd8.org/2017/03/code-editor-brackets-19-released.html</a></p>', 'Brackets 1.9 发布了。Brackets 是 Adobe 的开源 HTML/CSS/JavaScript 集成开发环境。Brackets 当前为 Mac, Windows 以及 Linux (Debian/Ubuntu)提供最新稳定版的二进制发布, 源代码托管在 GitHub 上。Brackets 1.9 的变化包括：在实时预览中反向检查（点击实时预览中的元素，高亮显示源代码中的相应标签......', '10000', '2017-03-24 14:47:48', '2017-03-24 14:47:48', '14', '0', '0', 'Y', 'http://localhost:8080/upload/201703/1490338060676_p.png|', '201703/1490338060676_p.png_s.jpg|', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');
INSERT INTO `ulewo_topic` VALUES ('10101', '0', '10049', '10055', 'Python 3.6.1 发布，3.6 系列首个维护版本', null, '<p>Python 3.6.1 发布了。Python 3.6.1 是 Python 3.6 的第一个维护版本。Python 3.6 系列包含许多新功能和优化。有关更多信息，<a data-cke-saved-href=\"https://docs.python.org/3.6/whatsnew/3.6.html\" target=\"_blank\" href=\"https://docs.python.org/3.6/whatsnew/3.6.html\">请参阅 Python 3.6 文档中的新增功能</a>。</p><h3><strong>更新内容：</strong></h3><p><strong>Core and Builtins</strong></p><ul class=\"list-paddingleft-2\"><li><p><a data-cke-saved-href=\"https://bugs.python.org/issue29723\" href=\"https://bugs.python.org/issue29723\">bpo-29723</a>: The <code>sys.path[0]</code> initialization change for <a data-cke-saved-href=\"https://bugs.python.org/issue29139\" href=\"https://bugs.python.org/issue29139\">bpo-29139</a> caused a regression by revealing an inconsistency in how sys.path is initialized when executing <code>__main__</code> from a zipfile, directory, or other import location. The interpreter now consistently avoids ever adding the import location’s parent directory to <code>sys.path</code>, and ensures no other <code>sys.path</code> entries are inadvertently modified when inserting the import location named on the command line.</p></li></ul><p><strong>Build</strong></p><ul class=\"list-paddingleft-2\"><li><p><a data-cke-saved-href=\"https://bugs.python.org/issue27593\" href=\"https://bugs.python.org/issue27593\">bpo-27593</a>: fix format of git information used in sys.version</p></li><li><p>Fix incompatible comment in python.h</p></li></ul><h3><strong>发行日志：</strong></h3><ul class=\"list-paddingleft-2\"><li><p>While it should be generally transparent to you, 3.6.1 is the first release produced using a major change to our software development and release processes. Please report any issues via our issue tracker at <a data-cke-saved-href=\"https://bugs.python.org/\" href=\"https://bugs.python.org/\">https://bugs.python.org</a>.</p></li><li><p>Windows users: The binaries for AMD64 will also work on processors that implement the Intel 64 architecture. (Also known as the &quot;x64&quot; architecture, and formerly known as both &quot;EM64T&quot; and &quot;x86-64&quot;.) They will not work on Intel Itanium Processors (formerly &quot;IA-64&quot;).</p></li><li><p>Windows users: If installing Python 3.6.0 as a non-privileged user, you may need to escalate to administrator privileges to install an update to your C runtime libraries.</p></li><li><p>Windows users: There are now &quot;web-based&quot; installers for Windows platforms; the installer will download the needed software components at installation time.</p></li><li><p>Windows Users: There are redistributable zip files containing the Windows builds, making it easy to redistribute Python as part of another software package. Please see the documentation regarding <a data-cke-saved-href=\"https://docs.python.org/3.6/using/windows.html#embedded-distribution\" href=\"https://docs.python.org/3.6/using/windows.html#embedded-distribution\">Embedded Distribution</a> for more information.</p></li><li><p>macOS users: If you are using the Python 3.6 from the python.org binary installer linked on this page, please carefully read the Important Information displayed during installation; this information is also available after installation by clicking on /Applications/Python 3.6/ReadMe.rtf. There is important information there about changes in the 3.6 installer-supplied Python, particularly with regard to SSL certificate validation.</p></li><li><p>macOS users: There is <a data-cke-saved-href=\"https://www.python.org/download/mac/tcltk\" href=\"https://www.python.org/download/mac/tcltk\">important information about IDLE, Tkinter, and Tcl/Tk on macOS here</a>.</p></li></ul><h3><strong>相关链接：</strong></h3><ul class=\"list-paddingleft-2\"><li><p><a data-cke-saved-href=\"https://docs.python.org/3.6/\" href=\"https://docs.python.org/3.6/\" target=\"_blank\">Online Documentation</a></p></li><li><p><a data-cke-saved-href=\"https://www.python.org/dev/peps/pep-0494/\" href=\"https://www.python.org/dev/peps/pep-0494/\" target=\"_blank\">3.6 Release Schedule</a></p></li><li><p>Report bugs at <a data-cke-saved-href=\"https://bugs.python.org/\" href=\"https://bugs.python.org/\" target=\"_blank\">https://bugs.python.org</a>.</p></li><li><p><a data-cke-saved-href=\"https://www.python.org/psf/donations/\" href=\"https://www.python.org/psf/donations/\" target=\"_blank\">Help fund Python and its community</a>.</p></li></ul><p><a data-cke-saved-href=\"https://www.python.org/downloads/release/python-361/\" target=\"_blank\" href=\"https://www.python.org/downloads/release/python-361/\">发布主页</a> 和 <a data-cke-saved-href=\"https://docs.python.org/3.6/whatsnew/changelog.html#python-3-6-1\" target=\"_blank\" href=\"https://docs.python.org/3.6/whatsnew/changelog.html#python-3-6-1\">完整更新内容</a></p><p><a data-cke-saved-href=\"https://www.python.org/downloads/\" target=\"_blank\" href=\"https://www.python.org/downloads/\">下载地址</a></p>', 'Python 3.6.1 发布了。Python 3.6.1 是 Python 3.6 的第一个维护版本。Python 3.6 系列包含许多新功能和优化。有关更多信息，请参阅 Python 3.6 文档中的新增功能。更新内容：Core and Builtinsbpo-29723: The sys.path[0] initialization change for bpo-29139 caused a......', '10000', '2017-03-24 14:47:49', '2017-03-24 14:47:49', '10', '0', '0', 'Y', '', '', '0', '0', '0', 'ulewo', 'avatars/10000.jpg');

-- ----------------------------
-- Table structure for ulewo_topic_vote
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_topic_vote`;
CREATE TABLE `ulewo_topic_vote` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '投票ID',
  `topic_id` int(11) NOT NULL,
  `vote_type` int(11) DEFAULT NULL COMMENT '1 单选 2多选',
  `end_date` date NOT NULL,
  PRIMARY KEY (`vote_id`),
  KEY `idx_topic_id` (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='投票';

-- ----------------------------
-- Records of ulewo_topic_vote
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_topic_vote_dtl
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_topic_vote_dtl`;
CREATE TABLE `ulewo_topic_vote_dtl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `count` int(11) DEFAULT '0' COMMENT '投票数',
  `vote_id` int(11) DEFAULT NULL COMMENT '投票ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='投票详情';

-- ----------------------------
-- Records of ulewo_topic_vote_dtl
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_topic_vote_user
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_topic_vote_user`;
CREATE TABLE `ulewo_topic_vote_user` (
  `vote_dtl_id` int(11) DEFAULT NULL COMMENT '投票详情ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `vote_date` datetime NOT NULL COMMENT '投票时间',
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='投票人';

-- ----------------------------
-- Records of ulewo_topic_vote_user
-- ----------------------------

-- ----------------------------
-- Table structure for ulewo_user
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_user`;
CREATE TABLE `ulewo_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL DEFAULT '' COMMENT '密码',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `user_bg` varchar(100) DEFAULT '0' COMMENT '背景图',
  `birthday` varchar(50) DEFAULT NULL COMMENT '生日',
  `sex` varchar(1) DEFAULT 'M' COMMENT '性别 M男 F女',
  `characters` varchar(200) DEFAULT NULL COMMENT '个性签名',
  `mark` int(11) DEFAULT '0' COMMENT '积分',
  `address` varchar(50) DEFAULT NULL COMMENT '籍贯',
  `work` varchar(50) DEFAULT NULL COMMENT '职业',
  `register_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `isvalid` varchar(1) DEFAULT 'Y' COMMENT '是否有效 Y有效 N无效',
  `activation_code` varchar(25) DEFAULT NULL COMMENT '激活码',
  `last_login_ip` varchar(50) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `user_bg_color` varchar(100) DEFAULT NULL COMMENT '背景颜色',
  `theme_type` int(1) DEFAULT '0' COMMENT '主题风格',
  `editor_type` int(1) DEFAULT '1' COMMENT '1:百度编辑器 2：markdown编辑器',
  PRIMARY KEY (`user_id`,`email`(1),`password`),
  KEY `user_index_userid` (`user_id`),
  KEY `user_index_username` (`user_name`),
  KEY `user_index_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='用户信息';

-- ----------------------------
-- Records of ulewo_user
-- ----------------------------
INSERT INTO `ulewo_user` VALUES ('10000', 'admin@ulewo.com', 'ulewo', 'E10ADC3949BA59ABBE56E057F20F883E', 'avatars/10000.jpg', 'defbg/bg8.jpg', null, 'M', null, '2', null, null, '2017-03-24 12:57:29', '2017-03-24 15:58:26', 'Y', null, '127.0.0.1', '', '#E7E1D3', '0', '1');

-- ----------------------------
-- Table structure for ulewo_user_friend
-- ----------------------------
DROP TABLE IF EXISTS `ulewo_user_friend`;
CREATE TABLE `ulewo_user_friend` (
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `friend_user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'friend用户ID',
  `create_time` datetime DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  `friend_user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `friend_user_icon` varchar(50) DEFAULT NULL COMMENT '用户小图像',
  PRIMARY KEY (`user_id`,`friend_user_id`),
  KEY `friend_index_userid` (`user_id`),
  KEY `friend_index_friendid` (`friend_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户粉丝，关注';

-- ----------------------------
-- Records of ulewo_user_friend
-- ----------------------------

-- ----------------------------
-- Table structure for user_op_threshold
-- ----------------------------
DROP TABLE IF EXISTS `user_op_threshold`;
CREATE TABLE `user_op_threshold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `op_date` date DEFAULT NULL,
  `op_count` int(11) DEFAULT '1' COMMENT '操作记录数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `indx` (`user_id`,`op_date`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户操作阈值';

-- ----------------------------
-- Records of user_op_threshold
-- ----------------------------
INSERT INTO `user_op_threshold` VALUES ('1', '10000', '2017-03-24', '1');
