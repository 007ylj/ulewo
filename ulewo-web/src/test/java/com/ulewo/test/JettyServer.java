package com.ulewo.test;

import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;

/**
 * 开发调试使用的 Jetty Server
 * @author badqiu
 */
public class JettyServer {

    static final String absloute_path = "E:\\opensource\\ulewo\\ulewo-web";

    public static void main(String[] args) throws Exception {
        Server server = buildNormalServer(8080, "/");
        server.start();
    }

    public static Server buildNormalServer(int port, String contextPath) {
        Server server = new Server(port);
        WebAppContext webContext = new WebAppContext(absloute_path + "/src/main/webapp", contextPath);
        webContext.setClassLoader(Thread.currentThread().getContextClassLoader());
        webContext.setMaxFormContentSize(-1);
        server.setHandler(webContext);
        server.setStopAtShutdown(true);
        return server;
    }
}
