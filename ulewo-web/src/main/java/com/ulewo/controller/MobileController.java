/**
 * Project Name:ulewo-web
 * File Name:HomeController.java
 * Package Name:com.ulewo.controller
 * Date:2015年10月25日下午3:01:15
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ulewo.po.model.SignInInfo;
import com.ulewo.service.SignInService;
import com.ulewo.service.UserService;
import com.ulewo.utils.Constants;

/**
 * ClassName:HomeController <br/>
 * Date:     2015年10月25日 下午3:01:15 <br/>
 * @author 多多洛
 * Copyright (c) 2015, ulewo.com All Rights Reserved. 
 */
@Controller
@RequestMapping(value = "mobile")
public class MobileController extends BaseController {
    @Resource
    private SignInService signInService;

    @Resource
    private UserService userService;

    @RequestMapping(value = "/sign_in")
    public ModelAndView sign_in(HttpSession session) {
        ModelAndView view = new ModelAndView("/page/mobile/signin");
        SignInInfo signInInfo = signInService.findSignInInfoByUserId(this.getUserId(session));
        view.addObject("signInInfo", signInInfo);
        return view;
    }

    @RequestMapping(value = "mobile_spitslot_post.action")
    public ModelAndView mobile_spitslot_post() {
        ModelAndView view = new ModelAndView("/page/mobile/spitslot_post");
        return view;
    }

    @RequestMapping(value = "mobile_spitslot")
    public ModelAndView mobile_load_spitslot() {
        ModelAndView view = new ModelAndView("/page/mobile/spitslot");
        return view;
    }

    @RequestMapping(value = "mobile_login")
    public ModelAndView login(HttpServletRequest request, HttpSession session) {
        ModelAndView view = new ModelAndView("/page/mobile/login");
        if (null != session.getAttribute(Constants.SESSION_ERROR_LOGIN_COUNT) && (Integer) session.getAttribute(Constants.SESSION_ERROR_LOGIN_COUNT) >= Constants.MAX_LOGIN_ERROR_COUNT) {
            view.addObject("checkCode", "checkCode");
        }
        return view;
    }
}
