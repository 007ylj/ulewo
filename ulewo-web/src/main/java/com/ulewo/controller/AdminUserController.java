/**
 * Project Name:ulewo-web
 * File Name:CommentController.java
 * Package Name:com.ulewo.controller
 * Date:2015年11月2日下午9:30:38
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ulewo.exception.BusinessException;
import com.ulewo.po.enums.ResponseCode;
import com.ulewo.po.enums.UserBackground;
import com.ulewo.po.model.User;
import com.ulewo.po.vo.AjaxResponse;
import com.ulewo.service.BlogCategoryService;
import com.ulewo.service.BlogService;
import com.ulewo.service.UserService;
import com.ulewo.utils.Constants;
import com.ulewo.utils.ScaleFilter;
import com.ulewo.utils.ServerUtils;
import com.ulewo.utils.StringTools;

@Controller
@RequestMapping("/admin")
public class AdminUserController extends BaseController {

    Logger logger = LoggerFactory.getLogger(AdminUserController.class);

    @Resource
    private UserService userService;

    @Resource
    private BlogCategoryService blogCategoryService;

    @Resource
    private BlogService blogService;

    private static final int IMAGE_WIDTH = 180;

    @RequestMapping(value = "update_user.action")
    public ModelAndView user_update(HttpSession session) {
        ModelAndView view = new ModelAndView("/page/admin/user_update");
        view.addObject("user", userService.findUserByUserId(this.getUserId(session).toString()));
        return view;
    }

    @ResponseBody
    @RequestMapping(value = "save_user.action")
    public AjaxResponse<?> save_user(HttpSession session, User user) {
        AjaxResponse<?> result = new AjaxResponse<Object>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            user.setUserId(this.getUserId(session));
            userService.updateInfo(user);
        } catch (BusinessException e) {
            logger.error("更新用户信息异常", e);
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            logger.error("更新用户信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
        }
        return result;
    }

    @RequestMapping(value = "change_pwd.action")
    public ModelAndView change_pwd(HttpSession session) {
        ModelAndView view = new ModelAndView("/page/admin/user_change_pwd");
        return view;
    }

    @ResponseBody
    @RequestMapping(value = "save_pwd.action")
    public AjaxResponse<?> save_pwd(HttpSession session, String oldPwd, String newPwd) {
        AjaxResponse<?> result = new AjaxResponse<Object>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            userService.changePwd(this.getUserId(session), oldPwd, newPwd);
        } catch (BusinessException e) {
            logger.error("修改密码异常", e);
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            result.setResponseCode(ResponseCode.SERVERERROR);
            logger.error("修改密码异常", e);
        }
        return result;
    }

    @RequestMapping(value = "change_user_icon.action")
    public ModelAndView change_user_icon(HttpSession session) {
        ModelAndView view = new ModelAndView("/page/admin/user_change_icon");
        return view;
    }

    @ResponseBody
    @RequestMapping(value = "/save_user_icon.action")
    public AjaxResponse<?> save_user_icon(HttpSession session, HttpServletRequest request, Integer x1, Integer y1, Integer width, Integer height, String img) {
        AjaxResponse<?> result = new AjaxResponse<Object>();
        result.setResponseCode(ResponseCode.SUCCESS);
        if (StringTools.isEmpty(img)) {
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg("请求参数错误");
            return result;
        }
        Integer userId = this.getUserId(session);
        InputStream tempIn = null;
        ByteArrayOutputStream out = null;
        OutputStream imgOut = null;
        String imgType = Constants.IMAGE_SUFFIX_JPG;
        String srcpath = ServerUtils.getImageFolder() + img;
        File file = new File(srcpath);
        try {
            BufferedImage image = ImageIO.read(file);
            // 裁剪图片
            BufferedImage subimg = image.getSubimage(x1, y1, width, height);
            BufferedImage imgResult = ScaleFilter.getThumbnail(subimg, IMAGE_WIDTH, IMAGE_WIDTH);
            // 将图片转为字节数组
            String okSrcPath = ServerUtils.getImageFolder() + Constants.PATH_AVATARS;
            File imagePathFile = new File(okSrcPath);
            if (!imagePathFile.exists()) {
                imagePathFile.mkdirs();
            }
            String userIconName = userId + imgType;
            File okfile = new File(okSrcPath + userIconName);
            ImageIO.write(imgResult, "JPEG", okfile);
            return result;
        } catch (Exception e) {
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
            return result;
        } finally {
            try {
                if (null != tempIn) {
                    tempIn.close();
                    tempIn = null;
                }
                if (null != out) {
                    out.close();
                    out = null;
                }
                if (imgOut != null) {
                    imgOut.close();
                }
            } catch (Exception e) {

            }
        }
    }

    @ResponseBody
    @RequestMapping(value = "/save_sys_user_icon.action")
    public AjaxResponse<?> save_sys_user_icon(HttpSession session, HttpServletRequest request, String userIcon) {
        AjaxResponse<?> result = new AjaxResponse<Object>();
        try {
            Integer userId = this.getUserId(session);
            User user = new User();
            user.setUserId(userId);
            String targetIcon = userId + Constants.IMAGE_SUFFIX_JPG;
            userService.copyUserIcon(userIcon, targetIcon);
            user.setUserIcon(Constants.PATH_AVATARS_SUFFIX + targetIcon);
            userService.updateInfo(user);
            result.setResponseCode(ResponseCode.SUCCESS);
        } catch (Exception e) {
            result.setResponseCode(ResponseCode.SERVERERROR);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/user_center_background.action")
    public ModelAndView user_center_background(HttpSession session) {
        ModelAndView mv = new ModelAndView();
        try {
            User user = userService.findUserByUserId(this.getUserId(session).toString());
            mv.addObject("background", user.getUserBg());
            mv.setViewName("/page/admin/user_center_background");
        } catch (Exception e) {
            logger.error(e.getMessage());
            mv.setViewName("redirect:" + Constants.ERROR_404);
            return mv;
        }
        return mv;
    }

    @ResponseBody
    @RequestMapping(value = "/save_user_center_background.action")
    public AjaxResponse<?> save_user_center_background(HttpSession session, HttpServletRequest request, String background) {
        AjaxResponse<?> result = new AjaxResponse<Object>();
        try {
            if (StringUtils.isEmpty(background)) {
                result.setResponseCode(ResponseCode.BUSINESSERROR);
                result.setErrorMsg("参数错误");
                return result;
            }
            UserBackground backGround = UserBackground.getBackGroundByType(background);
            if (null == backGround) {
                result.setResponseCode(ResponseCode.BUSINESSERROR);
                result.setErrorMsg("参数错误");
                return result;
            }
            Integer userId = this.getUserId(session);
            User user = new User();
            user.setUserId(userId);
            user.setUserBg(background);
            user.setUserBgColor(backGround.getColor());
            userService.updateInfo(user);
            result.setResponseCode(ResponseCode.SUCCESS);
        } catch (Exception e) {
            result.setResponseCode(ResponseCode.SERVERERROR);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/user_center_theme.action")
    public ModelAndView user_center_theme(HttpSession session) {
        ModelAndView mv = new ModelAndView();
        try {
            User user = userService.findUserByUserId(this.getUserId(session).toString());
            mv.addObject("themeType", user.getThemeType());
            mv.setViewName("/page/admin/user_center_theme");
        } catch (Exception e) {
            logger.error(e.getMessage());
            mv.setViewName("redirect:" + Constants.ERROR_404);
            return mv;
        }
        return mv;
    }

    @ResponseBody
    @RequestMapping(value = "/save_user_center_theme.action")
    public AjaxResponse<?> save_user_center_theme(HttpSession session, HttpServletRequest request, Integer themeType) {
        AjaxResponse<?> result = new AjaxResponse<Object>();
        try {
            if (null == themeType || (themeType != 0 && themeType != 1)) {
                result.setResponseCode(ResponseCode.BUSINESSERROR);
                result.setErrorMsg("参数错误");
                return result;
            }
            Integer userId = this.getUserId(session);
            User user = new User();
            user.setThemeType(themeType);
            user.setUserId(userId);
            userService.updateInfo(user);
            result.setResponseCode(ResponseCode.SUCCESS);
        } catch (Exception e) {
            result.setResponseCode(ResponseCode.SERVERERROR);
        }
        return result;
    }
}
