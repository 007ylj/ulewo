package com.ulewo.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ulewo.po.enums.ResponseCode;
import com.ulewo.po.vo.AjaxResponse;
import com.ulewo.service.AskService;
import com.ulewo.service.BlogService;
import com.ulewo.service.KnowledgeService;
import com.ulewo.service.TopicService;

/**
 * ClassName: TaskController
 * date: 2015年8月9日 上午11:38:19
 * @author 多多洛
 * @since JDK 1.7
 */
@Controller
@RequestMapping(value = "/manage/email")
public class ManageEmailController extends BaseController {

    Logger logger = LoggerFactory.getLogger(ManageEmailController.class);

    @Resource
    private BlogService blogService;

    @Resource
    private TopicService topicService;

    @Resource
    private AskService askService;

    @Resource
    private KnowledgeService knowledgeService;

    @RequestMapping(value = "/createEmail")
    public String task_list() {
        return "page/manage/create_email";
    }

    @ResponseBody
    @RequestMapping(value = "/createEmailList")
    public AjaxResponse<Map<String, Object>> createEmailList(String blogIds, String bbsIds, String askIds, String knowledgeIds) {
        AjaxResponse<Map<String, Object>> response = new AjaxResponse<Map<String, Object>>();
        try {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            if (!StringUtils.isEmpty(blogIds)) {
                resultMap.put("blogList", blogService.findBlogByIds(convert(blogIds.split(","))));
            }
            if (!StringUtils.isEmpty(bbsIds)) {
                resultMap.put("bbsList", topicService.findTopicByIds(convert(bbsIds.split(","))));
            }
            if (!StringUtils.isEmpty(askIds)) {
                resultMap.put("askList", askService.findAskByIds(convert(askIds.split(","))));
            }
            if (!StringUtils.isEmpty(knowledgeIds)) {
                resultMap.put("knowledgeList", knowledgeService.findKnowledgeByIds(convert(knowledgeIds.split(","))));
            }
            response.setData(resultMap);
            response.setResponseCode(ResponseCode.SUCCESS);
        } catch (Exception e) {
            logger.error("查询任务失败：{}", e);
            response.setResponseCode(ResponseCode.SERVERERROR);
            response.setErrorMsg("查询任务失败");
        }
        return response;
    }

    private Integer[] convert(String[] ids) {
        Integer[] idArr = new Integer[ids.length];
        int idx = 0;
        for (String id : ids) {
            idArr[idx] = Integer.parseInt(id);
            idx++;
        }
        return idArr;
    }
}
