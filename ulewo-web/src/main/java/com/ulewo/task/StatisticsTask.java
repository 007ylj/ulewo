/**
 * Project Name:ulewo-web
 * File Name:StatisticsTask.java
 * Package Name:com.ulewo.task
 * Date:2015年12月17日下午9:28:27
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.task;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ulewo.mapper.SignInMapper;
import com.ulewo.po.config.ConfigInfo;
import com.ulewo.po.enums.EmailMessageType;
import com.ulewo.po.model.MessageSettings;
import com.ulewo.po.model.SignIn;
import com.ulewo.po.query.MessageSettingsQuery;
import com.ulewo.po.query.SignInQuery;
import com.ulewo.service.MessageSettingsService;
import com.ulewo.service.StatisticsService;
import com.ulewo.utils.SendMailUtils;
import com.ulewo.utils.SpringContextUtil;

/**
 * ClassName:StatisticsTask <br/>
 * Date:     2015年12月17日 下午9:28:27 <br/>
 * @author 多多洛
 * Copyright (c) 2015, ulewo.com All Rights Reserved. 
 */
public class StatisticsTask {

    Logger logger = LoggerFactory.getLogger(StatisticsTask.class);

    private StatisticsService statisticsService = (StatisticsService) SpringContextUtil.getBean("statisticsService");

    private ConfigInfo configInfo = (ConfigInfo) SpringContextUtil.getBean("configInfo");

    @SuppressWarnings("unchecked")
    private SignInMapper<SignIn, SignInQuery> signInMapper = (SignInMapper<SignIn, SignInQuery>) SpringContextUtil.getBean("signInMapper");

    private MessageSettingsService messageSettingsService = (MessageSettingsService) SpringContextUtil.getBean("messageSettingsService");

    public void statisticsInfo() {
        statisticsService.statistAllInfo();
    }

    public void noticeSignInTask() {
        MessageSettingsQuery query = new MessageSettingsQuery();
        query.setNoticeSignInType(EmailMessageType.RECEIVE.getType());
        List<MessageSettings> list = messageSettingsService.selectList(query);

        Date curDate = new Date();
        for (MessageSettings settings : list) {
            SignInQuery signInQuery = new SignInQuery();
            signInQuery.setUserId(settings.getUserId());
            signInQuery.setCurDate(curDate);
            int userSignInCount = signInMapper.selectCount(signInQuery);
            if (userSignInCount > 0) {
                continue;
            }
            String html = "<table style=\"margin: 25px auto;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"648\" align=\"center\">" + "<tbody>" + "<tr><td style=\"color:#40AA53;\"><h1 style=\"margin-bottom:10px;\">有乐窝</h1></td></tr>" + "<tr>" + "<td style=\"border-left: 1px solid #D1FFD1; padding: 20px 20px 0px; background: none repeat scroll 0% 0% #ffffff; border-top: 5px solid #40AA53; border-right: 1px solid #D1FFD1;\">" + "<p>你好,多多洛喊你去网站签到，签到传送门：<a style=\"color:#060;font-size:9pt;\" href='http://www.ulewo.com/signIn'>点击传送</a></p>" + "</td>" + "</tr>" + "<tr>" + "<td style=\"border-bottom: 1px solid #D1FFD1; border-left: 1px solid #D1FFD1; padding: 0px 20px 20px; background: none repeat scroll 0% 0% #ffffff; border-right: 1px solid #D1FFD1;\">" + "<hr style=\"color:#ccc;\">" + "<p style=\"color:#060;font-size:9pt;\">" + "想了解更多信息，请访问<a href=\"http://www.ulewo.com\" target=\"_blank\"> http://www.ulewo.com" + "</a>" + "</p>" + "</td>" + "</tr>" + "</tbody>" + "</table>";
            try {
                SendMailUtils.sendEmail(configInfo.getFindemail(), configInfo.getFindpwd(), "有乐窝消息提醒", html, new String[]{settings.getEmail()});
            } catch (Exception e) {
                logger.info("发送邮件异常", e);
            }
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
