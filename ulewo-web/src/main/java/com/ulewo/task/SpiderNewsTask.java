/**
 * Project Name:ulewo-web
 * File Name:StatisticsTask.java
 * Package Name:com.ulewo.task
 * Date:2015年12月17日下午9:28:27
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.task;

import com.ulewo.service.SpiderService;
import com.ulewo.utils.SpringContextUtil;

/**
 * ClassName:StatisticsTask <br/>
 * Date:     2015年12月17日 下午9:28:27 <br/>
 * @author 多多洛
 * Copyright (c) 2015, ulewo.com All Rights Reserved. 
*/
public class SpiderNewsTask {
    private SpiderService spiderService = (SpiderService) SpringContextUtil.getBean("spiderService");

    public void spiderNews() {
        spiderService.spiderNews();
    }
}
