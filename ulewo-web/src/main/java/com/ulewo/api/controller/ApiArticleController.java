/**
 * Project Name:ulewo-web
 * File Name:CommentController.java
 * Package Name:com.ulewo.controller
 * Date:2015年11月2日下午9:30:38
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.api.controller;

import com.ulewo.controller.BaseController;
import com.ulewo.exception.BusinessException;
import com.ulewo.po.enums.*;
import com.ulewo.po.model.Ask;
import com.ulewo.po.model.Blog;
import com.ulewo.po.model.Knowledge;
import com.ulewo.po.model.Topic;
import com.ulewo.po.query.AskQuery;
import com.ulewo.po.query.BlogQuery;
import com.ulewo.po.query.KnowledgeQuery;
import com.ulewo.po.query.TopicQuery;
import com.ulewo.po.vo.AjaxResponse;
import com.ulewo.po.vo.PaginationResult;
import com.ulewo.po.vo.api.ArticleVO;
import com.ulewo.service.AskService;
import com.ulewo.service.BlogService;
import com.ulewo.service.KnowledgeService;
import com.ulewo.service.TopicService;
import com.ulewo.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/api")
public class ApiArticleController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(ApiArticleController.class);

    @Resource
    private BlogService blogBlogService;

    @Resource
    private TopicService topicService;

    @Resource
    private AskService askService;

    @Resource
    private KnowledgeService knowledgeService;

    /**
     * 获取博客
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping("loadBlogs")
    public AjaxResponse<PaginationResult<ArticleVO>> loadBlogs(Integer pageNo) {
        AjaxResponse<PaginationResult<ArticleVO>> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            BlogQuery blogQuery = new BlogQuery();
            blogQuery.setPageNo(pageNo);
            blogQuery.setStatus(BlogStatusEnum.PASS);
            blogQuery.setOrderBy(OrderByEnum.CREATE_TIME_DESC);
            PaginationResult<Blog> paginationResult = blogBlogService.findBlogByPage(blogQuery);
            PaginationResult<ArticleVO> paginationResultVO = new PaginationResult(paginationResult.getPage(), Blog.convert2VOList
                    (paginationResult.getList()));
            result.setData(paginationResultVO);
        } catch (Exception e) {
            logger.error("api获取博客信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 博客详情
     * @param id
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping("blogDetail")
    public AjaxResponse<ArticleVO> blogDetail(Integer id) {
        AjaxResponse<ArticleVO> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            Blog blog = blogBlogService.getBlogDetail4Api(id);
            result.setData(Blog.convert2VO(blog));
        } catch (BusinessException e) {
            logger.error("api获取博客详情信息异常", e);
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            logger.error("api获取博客详情信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("loadNews")
    public AjaxResponse<PaginationResult<ArticleVO>> loadNews(Integer pageNo) {
        AjaxResponse<PaginationResult<ArticleVO>> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            TopicQuery query = new TopicQuery();
            query.setPageNo(pageNo);
            query.setOrderBy(OrderByEnum.CREATE_TIME_DESC);
            query.setCategoryId(Constants.NEWS_CATEGORYID);
            PaginationResult<Topic> paginationResult = topicService.findTopicsByPage(query);
            PaginationResult<ArticleVO> paginationResultVO = new PaginationResult(paginationResult.getPage(), Topic.convert2VOList
                    (paginationResult.getList()));
            result.setData(paginationResultVO);
        } catch (Exception e) {
            logger.error("api获取论坛信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 论坛列表
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping("loadTopics")
    public AjaxResponse<PaginationResult<ArticleVO>> loadTopics(Integer pageNo) {
        AjaxResponse<PaginationResult<ArticleVO>> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            TopicQuery query = new TopicQuery();
            query.setPageNo(pageNo);
            query.setTopicType(TopicType.NORMAL.getType());
            query.setOrderBy(OrderByEnum.LAST_COMMENT_TIME_DESC_AND_CREATE_TIME_DESC);
            query.setNotContainCategoryId(Constants.NEWS_CATEGORYID);
            PaginationResult<Topic> paginationResult = topicService.findTopicsByPage(query);
            PaginationResult<ArticleVO> paginationResultVO = new PaginationResult(paginationResult.getPage(), Topic.convert2VOList
                    (paginationResult.getList()));
            result.setData(paginationResultVO);
        } catch (Exception e) {
            logger.error("api获取论坛信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 论坛详情
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("topicDetail")
    public AjaxResponse<ArticleVO> topicDetail(Integer id) {
        AjaxResponse<ArticleVO> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            Topic topic = topicService.showTopic(id);
            result.setData(Topic.convert2VO(topic));
        } catch (BusinessException e) {
            logger.error("api获取论坛详情信息异常", e);
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            logger.error("api获取论坛详情信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 问答列表
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping("loadAsks")
    public AjaxResponse<PaginationResult<ArticleVO>> loadAsks(Integer pageNo) {
        AjaxResponse<PaginationResult<ArticleVO>> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            AskQuery query = new AskQuery();
            query.setPageNo(pageNo);
            query.setOrderBy(OrderByEnum.CREATE_TIME_DESC);
            PaginationResult<Ask> paginationResult = askService.findAskByPage(query);
            PaginationResult<ArticleVO> paginationResultVO = new PaginationResult(paginationResult.getPage(), Ask.convert2VOList
                    (paginationResult.getList()));
            result.setData(paginationResultVO);
        } catch (Exception e) {
            logger.error("api获取问答信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 问答详情
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("askDetail")
    public AjaxResponse<ArticleVO> askDetail(Integer id) {
        AjaxResponse<ArticleVO> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            Ask ask = askService.showAsk(id);
            result.setData(Ask.convert2VO(ask));
        } catch (BusinessException e) {
            logger.error("api获取问答详情信息异常", e);
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            logger.error("api获取问答详情信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 知识库列表
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping("loadKnowledge")
    public AjaxResponse<PaginationResult<Knowledge>> loadKnowledge(Integer pageNo) {
        AjaxResponse<PaginationResult<Knowledge>> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            KnowledgeQuery query = new KnowledgeQuery();
            query.setPageNo(pageNo);
            query.setStatus(StatusEnum.AUDIT);
            PaginationResult<Knowledge> paginationResult = knowledgeService.findKnowledgesByPage(query);
            result.setData(paginationResult);
        } catch (Exception e) {
            logger.error("api获取知识库信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 知识库详情
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("knowledgeDetail")
    public AjaxResponse<Knowledge> knowledgeDetail(Integer id) {
        AjaxResponse<Knowledge> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            Knowledge knowledge = knowledgeService.showKnowledge(id, null);
            knowledge.setContent(knowledge.getContent());
            result.setData(knowledge);
        } catch (BusinessException e) {
            logger.error("api获取知识库详情信息异常", e);
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            logger.error("api获取知识库详情信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }
}
