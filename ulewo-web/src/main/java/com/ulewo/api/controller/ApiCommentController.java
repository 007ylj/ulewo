/**
 * Project Name:ulewo-web
 * File Name:CommentController.java
 * Package Name:com.ulewo.controller
 * Date:2015年11月2日下午9:30:38
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.api.controller;

import com.ulewo.controller.BaseController;
import com.ulewo.exception.BusinessException;
import com.ulewo.po.enums.ArticleType;
import com.ulewo.po.enums.ResponseCode;
import com.ulewo.po.model.Comment;
import com.ulewo.po.query.CommentQuery;
import com.ulewo.po.vo.AjaxResponse;
import com.ulewo.po.vo.PaginationResult;
import com.ulewo.po.vo.api.CommentVO;
import com.ulewo.service.CommentService;
import com.ulewo.utils.StringTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/api")
public class ApiCommentController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(ApiCommentController.class);

    @Resource
    private CommentService commentService;

    /**
     * 加载评论
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping("loadComments")
    public AjaxResponse<PaginationResult<CommentVO>> loadCommens(Integer id, String type, Integer pageNo) {
        AjaxResponse<PaginationResult<CommentVO>> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            CommentQuery query = new CommentQuery();
            query.setPageNo(pageNo);
            query.setArticleId(id);
            query.setPid(0);
            query.setArticleType(ArticleType.getArticleTypeByType(type));
            PaginationResult<Comment> paginationResult = commentService.findCommentByPage(query);
            PaginationResult<CommentVO> paginationResultVo = new PaginationResult<>(paginationResult.getPage(), Comment.convert2VOList
                    (paginationResult.getList()));
            result.setData(paginationResultVo);
        } catch (Exception e) {
            logger.error("api获取评论信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    /**
     * 添加评论
     * @param pid
     * @return
     */
    @ResponseBody
    @RequestMapping("postComment.action")
    public AjaxResponse<CommentVO> postComment(HttpSession session, Integer articleId, Integer pid,
                                               String content, String type, String sourceFrom) {
        AjaxResponse<CommentVO> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            if (StringTools.filterEmoji(content)) {
                throw new BusinessException("系统不允许插入emoji表情");
            }
            Comment comment = new Comment();
            this.setUserBaseInfo(Comment.class, comment, session);
            comment.setArticleId(articleId);
            comment.setPid(pid);
            comment.setContent(content);
            comment.setArticleType(ArticleType.getArticleTypeByType(type));
            comment.setSourceFrom(sourceFrom);
            commentService.addComment(comment);
            result.setData(Comment.convert2VO(comment));
        } catch (BusinessException e) {
            logger.error("api添加评论信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            logger.error("api添加评论信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }
}
