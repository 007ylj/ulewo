/**
 * Project Name:ulewo-web
 * File Name:CommentController.java
 * Package Name:com.ulewo.controller
 * Date:2015年11月2日下午9:30:38
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.api.controller;

import com.ulewo.controller.BaseController;
import com.ulewo.exception.BusinessException;
import com.ulewo.po.enums.BlogStatusEnum;
import com.ulewo.po.enums.OrderByEnum;
import com.ulewo.po.enums.ResponseCode;
import com.ulewo.po.model.*;
import com.ulewo.po.query.*;
import com.ulewo.po.vo.AjaxResponse;
import com.ulewo.po.vo.PaginationResult;
import com.ulewo.po.vo.api.SignInVO;
import com.ulewo.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/api")
public class ApiSiginController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(ApiSiginController.class);

    @Resource
    private SignInService signInService;


    /**
     * 获取签到
     * @param pageNo
     * @return
     */
    @ResponseBody
    @RequestMapping("loadSignins")
    public AjaxResponse<PaginationResult<SignInVO>> loadSignins(Integer pageNo) {
        AjaxResponse<PaginationResult<SignInVO>> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            SignInQuery query = new SignInQuery();
            query.setPageNo(pageNo);
            PaginationResult<SignIn> paginationResult = signInService.findCurDaySigin(query);
            PaginationResult<SignInVO> voResult = new PaginationResult<>(paginationResult.getPage(), SignIn.convert2VOList(paginationResult
                    .getList()));
            result.setData(voResult);
        } catch (Exception e) {
            logger.error("api获取签到信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }


    /**
     * 签到
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("doSignin.action")
    public AjaxResponse<SignInVO> doSignin(HttpSession session, String sourceFrom) {
        AjaxResponse<SignInVO> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            SignIn signIn = signInService.doSignIn(this.getSessionUser(session), sourceFrom);
            result.setData(SignIn.convert2VO(signIn));
        } catch (BusinessException e) {
            logger.error("api签到异常", e);
            result.setResponseCode(ResponseCode.BUSINESSERROR);
            result.setErrorMsg(e.getMessage());
        } catch (Exception e) {
            logger.error("api签到异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("getSignInInfo4Today")
    public AjaxResponse<SignInInfo> getSignInInfo4Today(HttpSession session) {
        AjaxResponse<SignInInfo> result = new AjaxResponse<>();
        result.setResponseCode(ResponseCode.SUCCESS);
        try {
            SignInInfo signInInfo = signInService.findSignInInfoByUserId(this.getUserId(session));
            result.setData(signInInfo);
        } catch (Exception e) {
            logger.error("api获取签到信息异常", e);
            result.setResponseCode(ResponseCode.SERVERERROR);
            result.setErrorMsg("系统异常");
        }
        return result;
    }
}
