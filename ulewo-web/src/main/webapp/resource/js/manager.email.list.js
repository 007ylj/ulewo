ulewo.searchParam = "";
ulewo.url = [
    ulewo.absolutePath + "/manage/blog/load_blog",
    ulewo.absolutePath + "/manage/topic/load_topic",
    ulewo.absolutePath + "/manage/ask/load_ask",
    ulewo.absolutePath + "/manage/knowledge/load_knowledge"
];
ulewo.createEmailUrl = ulewo.absolutePath + "/manage/email/createEmailList";
ulewo.curLodUrl = ulewo.url[0];
$(function () {
    initPage(1);
    $(document).on("change", "#part", function () {
        ulewo.curLodUrl = ulewo.url[$(this).val()];
        initPage(1);
    });

    $(document).on("click", ".no-check-topic", function () {
        var type = $("#part").val();
        if ($(this).is(":checked")) {
            $("<div class='checked-data-item' dataId=" + $(this).val() + " id='part-" + type + "-" + $(this).val() + "'>" + $(this).prop("title") + "</div>").appendTo($(".checked-data").eq(type));
        } else {
            $("#part-" + type + "-" + $(this).val()).remove();
        }
    });

    $(document).on("click", ".checked-data-item", function () {
        $(this).remove();
    });

})

//生成邮件列表
function createMailList() {
    var d = ulewo.dialog({
        title: "邮件列表",
        showButton: false,
        width: 1300
    });
    var blogItmes = $("#blog-list").find(".checked-data-item");
    var bbsItmes = $("#bbs-list").find(".checked-data-item");
    var askItmes = $("#ask-list").find(".checked-data-item");
    var knowledgeItmes = $("#knowledge-list").find(".checked-data-item");

    ulewo.ajaxRequest({
        url: ulewo.createEmailUrl, data: {
            "blogIds": getIds(blogItmes),
            "bbsIds": getIds(bbsItmes),
            "askIds": getIds(askItmes),
            "knowledgeIds": getIds(knowledgeItmes)
        }, fun: function (res) {
            var blogList = res.data.blogList;
            var bbsList = res.data.bbsList;
            var askList = res.data.askList;
            var knowledgeList = res.data.knowledgeList;
            var allHtml = "<div><a href='javascript:;' id='copy' class='btn'>复制</a></div>";
            var html = "<div style='padding:0px 20px'><div style='color:#259;margin: 10px 0;font-size:20px;border-bottom:1px solid #F1F4F6;padding:10px;'>有乐窝 (www.ulewo.com) 一周精彩回顾</div>";
            if (blogList != null) {
                for (var i = 0, _len = blogList.length; i < _len; i++) {
                    html = html + "<div style='border-bottom:1px solid #F1F4F6;padding:10px;'>" +
                        "<div>" +
                        "<a target='_blank' href='" + ulewo.absolutePath + "/user/" + blogList[i].userId + "/blog/" + blogList[i].blogId + "' style='font-size: 15px;font-weight: bold;text-decoration: none;color: #259;border: none;outline: none'>" + blogList[i].title + "</a>" +
                        "</div>" +
                        "<div style='font-size: 13px;line-height: 22px;text-decoration: none;color: #333;margin-top:5px;'>" + blogList[i].summary + "</div>" +
                        "</div>";
                }
            }
            if (bbsList != null) {
                for (var i = 0, _len = bbsList.length; i < _len; i++) {
                    html = html + "<div style='border-bottom:1px solid #F1F4F6;padding:10px;'>" +
                        "<div>" +
                        "<a target='_blank' href='" + ulewo.absolutePath + "/bbs/" + bbsList[i].topicId + "' style='font-size: 15px;font-weight: bold;text-decoration: none;color: #259;border: none;outline: none'>" + bbsList[i].title + "</a>" +
                        "</div>" +
                        "<div style='font-size: 13px;line-height: 22px;text-decoration: none;color: #333;margin-top:5px;'>" + bbsList[i].summary + "</div>" +
                        "</div>";
                }
            }
            if (askList != null) {
                for (var i = 0, _len = askList.length; i < _len; i++) {
                    html = html + "<div style='border-bottom:1px solid #F1F4F6;padding:10px;'>" +
                        "<div>" +
                        "<a target='_blank' href='" + ulewo.absolutePath + "/ask/" + askList[i].askId + "' style='font-size: 15px;font-weight: bold;text-decoration: none;color: #259;border: none;outline: none'>" + askList[i].title + "</a>" +
                        "</div>" +
                        "<div style='font-size: 13px;line-height: 22px;text-decoration: none;color: #333;margin-top:5px;'>" + askList[i].summary + "</div>" +
                        "</div>";
                }
            }
            if (knowledgeList != null) {
                for (var i = 0, _len = knowledgeList.length; i < _len; i++) {
                    html = html + "<div style='border-bottom:1px solid #F1F4F6;padding:10px;'>" +
                        "<div>" +
                        "<a target='_blank' href='" + ulewo.absolutePath + "/knowledge/" + knowledgeList[i].topicId + "' style='font-size: 15px;font-weight: bold;text-decoration: none;color: #259;border: none;outline: none'>" + knowledgeList[i].title + "</a>" +
                        "</div>" +
                        "<div style='font-size: 13px;line-height: 22px;text-decoration: none;color: #333;margin-top:5px;'>" + knowledgeList[i].summary + "</div>" +
                        "</div>";
                }
            }
            html = html + "</div>";
            d.content(allHtml + html);

            $("#copy").zclip({
                path: ulewo.absolutePath + "/resource/clipboard/ZeroClipboard.swf",
                copy: function () {
                    return html;
                },
                afterCopy: function () {/* 复制成功后的操作 */
                    ulewo.tipMsg({
                        type: "success", content: "复制成功", timeout: 1500
                    });
                }
            });
        }
    });
}

function getIds(datas) {
    var ids = [];
    for (var i = 0, _len = datas.length; i < _len; i++) {
        ids.push(datas.eq(i).attr("dataId"));
    }
    return ids.join(",");
}

function search() {
    ulewo.searchParam = "title=" + $("#search-input").val();
    initPage(1);
}

function initPage(pageNo) {
    ulewo.pageNo = pageNo;
    var data = ulewo.searchParam + "&pageNo=" + pageNo;
    ulewo.ajaxRequest({
        async: true, url: ulewo.curLodUrl, data: data, fun: function (response) {
            fillPage(response);
        }
    });
}

function fillPage(response) {
    var data = response.data;
    var simplePage = data.page;
    var contentArr = [];
    var list = data.list;
    $("#pager_content tr").remove();
    if (list.length > 0) {
        var curPart = $("#part").val();
        for (var i = 0, _len = list.length, d; i < _len, d = list[i]; i++) {
            var url = "";
            var id = "";
            if (curPart == 0) {
                id = d.blogId;
                url = ulewo.absolutePath + "/user/" + d.userId + "/blog/" + id;
            } else if (curPart == 1) {
                id = d.topicId;
                url = ulewo.absolutePath + "/bbs/" + id;
            } else if (curPart == 2) {
                id = d.askId;
                url = ulewo.absolutePath + "/ask/" + id;
            } else if (curPart == 3) {
                id = d.topicId;
                url = ulewo.absolutePath + "/knowledge/" + id;
            }
            $('<tr><td width="30"><input class="not-input no-check-topic" type="checkbox" title=' + d.title + ' value="' + id + '"></td><td colspan="100"><a target="_blank" href=' + url + '>' + d.title + '</a></td><td colspan="100">' + d.showCreateTime + '</td></tr>').appendTo($("#pager_content"));
        }
    } else {
        $('<tr><td  colspan="100"><div  class="no-data">没有数据</div></td></tr>').appendTo($("#pager_content"));
    }

    ulewo.pagination({
        pagePanelId: "pager", pageObj: simplePage, fun: initPage
    });
}