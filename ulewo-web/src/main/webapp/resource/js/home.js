$(function () {
    $("#doSignIn").click(function () {
        var haveSignInType = $(this).attr("haveSignInType");
        if (haveSignInType == "true") {
            return;
        }
        if (ulewo.user.userId == "") {
            var url = ulewo.curUrl;
            url = encodeURI(url);
            document.location.href = ulewo.absolutePath + "/login?redirect=" + url;
            return;
        }
        ulewo.ajaxRequest({
            async: true, url: ulewo.absolutePath + "/doSignIn.action", fun: function (res) {
                $("#index_count").text(parseInt($("#index_count").text()) + 1);
                setSignInInfo(res);
                var msg = "2积分已到碗里";
                if (res.data.continueSigIn) {
                    msg = "连续7天签到10积分已到碗里";
                }
                ulewo.tipMsg({
                    type: "success", content: msg, timeout: 3000
                });
            }
        });
    });
})

function setSignInInfo(res) {
    var signInBtn = $("#doSignIn");
    signInBtn.find("#sign-info .sign-tit").text("已签到");
    signInBtn.attr("haveSignInType", true);
    $("#todaySigInCount").text(parseInt($("#todaySigInCount").text()) + 1);
    $("#userSignInCount").text(parseInt($("#userSignInCount").text()) + 1);
    $("#sign-days").text(parseInt($("#sign-days").text()) + 1);
}