ulewo.attachmentUrl = {
    downloadUser: ulewo.absolutePath + "/findAttachmentDonwload",
    checkdownload: ulewo.absolutePath + "/checkDownload.action",
    downloadAttachment: ulewo.absolutePath + "/downloadAttachment.action"
}

$(function () {
    $("#dcount").click(function () {
        ulewo.attachmentid = $(this).attr("attachmentid");
        showDonwloadUserDialog($(this)[0], ulewo.attachmentid);
    });

    $("#downloadFile").click(function () {
        var topicid = $(this).attr("topicid");
        var attachmentid = $(this).attr("attachmentid");
        checkDownload(topicid, attachmentid);
    });

    $(document).on("click", "#load-more-donwload-user", function () {
        loadDonwloadUser(ulewo.attachmentid);
    });
})

function checkDownload(topicid, attachmentid) {
    ulewo.ajaxRequest({
        url: ulewo.attachmentUrl.checkdownload, data: {
            attachmentId: attachmentid, topicId: topicid
        }, fun: function (res) {
            document.location.href = ulewo.attachmentUrl.downloadAttachment + "?attachmentId=" + attachmentid;
            var dcount = $("#dcount").text();
            $("#dcount").text(parseInt(dcount) + 1)
        }
    });
}

function showDonwloadUserDialog(curObj, attachmentid) {
    var d = ulewo.popDialog({
        width: 280, align: 'bottom', obj: curObj
    });
    var at_panel = $("<div id='download-user'></div>")
    loadDonwloadUser(attachmentid);
    d.content(at_panel);
}

function loadDonwloadUser(attachmentid) {
    $("#load-more-donwload-user").remove();

    ulewo.ajaxRequest({
        url: ulewo.attachmentUrl.downloadUser, showLoad: false, data: {
            attachmentId: attachmentid,
            pageNo: ulewo.donwloadUserPageNo == null ? 1 : ulewo.donwloadUserPageNo + 1
        }, fun: function (res) {
            var content = "";
            var list = res.data.list;
            var simplePage = res.data.page;
            ulewo.donwloadUserPageNo = simplePage.page;
            if (simplePage.countTotal == 0) {
                content = "没有用户下载";
            } else {
                for (var i = 0, _len = list.length, item; i < _len, item = list[i]; i++) {
                    content = content + "<a class='download-user' href='" + ulewo.absolutePath + "/user/" + item.userId + "' title='" + item.userName + "' target='_blank'><img src='" + ulewo.imageDomain + "upload/" + item.userIcon + "'></a>";
                }
                if (simplePage.page < simplePage.pageTotal) {
                    content = content + "<div id='load-more-donwload-user' style='clear:left;' class='load-more'><a href='javascript:;'>⇓加载更多</a></div>";
                }
            }
            $(content).appendTo($("#download-user"));
        }
    });
}