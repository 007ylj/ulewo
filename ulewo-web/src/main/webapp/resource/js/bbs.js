$(function () {
    $("#post-btn").click(function () {
        if (ulewo.user.userId == "") {
            ulewo.goLogin();
            return;
        }
        var cateInfo = "";
        if (typeof(pCategoryId) != "undefined" && pCategoryId != "") {
            cateInfo = "?pCategoryId=" + pCategoryId;
        }
        if (typeof(categoryId) != "undefined" && categoryId != "") {
            cateInfo = cateInfo + "&categoryId=" + categoryId;
        }
        document.location.href = ulewo.absolutePath + "/bbs/post_topic.action" + cateInfo;
    });
})