ulewo.searchParam = "";
ulewo.url = {
    bindEmail: ulewo.absolutePath + "/admin/message_settings_bind.action",
    saveSettings: ulewo.absolutePath + "/admin/message_settings_save.action"
}
$(function () {
    $(document).on("click", "#bind-btn", function () {
        bindEmail();
    });
    $(document).on("click", "#save-btn", function () {
        saveSettings();
    });
})

function bindEmail() {
    var result = ulewo.checkForm($(".bind"));
    if (!result) {
        return;
    }
    ulewo.ajaxRequest({
        url: ulewo.url.bindEmail,
        data: {
            email: $("input[name='email']").val()
        },
        fun: function (response) {
            ulewo.tipMsg({
                type: "success", content: "保存邮箱成功", timeout: 1500
            });
        }
    });
}

function saveSettings() {
    ulewo.ajaxRequest({
        url: ulewo.url.saveSettings,
        data: {
            msgType: $("input[name='msgType']:checked ").val(),
            noticeSignInType: $("input[name='noticeSignInType']:checked ").val()
        },
        fun: function (response) {
            ulewo.tipMsg({
                type: "success", content: "保存设置成功", timeout: 1500
            });
        }
    });
}
