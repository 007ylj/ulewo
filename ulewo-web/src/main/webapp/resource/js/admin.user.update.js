ulewo.userUrl = {
    saveUser: ulewo.absolutePath + "/admin/save_user.action"
}
$(function () {
    $("#post-btn").click(function () {
        saveUserInfo();
    });
})

function saveUserInfo() {
    ulewo.ajaxRequest({
        url: ulewo.userUrl.saveUser,
        data: $("#post-form").serialize(),
        fun: function (res) {
            ulewo.tipMsg({
                type: "success", content: "保存用户信息成功", timeout: 1500
            });
        }
    })
}