$.extend(ulewo, {
    checkForm: function (formObj) {
        var inputs = formObj.find("input[checkData]");
        var resutl = true;
        formObj.find(".error").remove();
        formObj.find("input").removeClass("warn");
        formObj.find("select").removeClass("warn");
        formObj.find("textarea").removeClass("warn");
        var selectes = formObj.find("select[checkData]");
        var textareas = formObj.find("textarea[checkData]");
        for (var i = 0, _len = selectes.length; i < _len; i++) {
            inputs.push(selectes.eq(i));
        }

        for (var i = 0, _len = textareas.length; i < _len; i++) {
            inputs.push(textareas.eq(i));
        }

        for (var i = 0, _len = inputs.length; i < _len; i++) {
            var input = $(inputs[i]);
            var checkDataStr = input.attr("checkData");
            var placeholderData = input.attr("placeholder");
            var value = $.trim(input.val());
            value = value == placeholderData ? "" : value;
            if (null != checkDataStr) {
                var checkData = eval('(' + checkDataStr + ')');
                var msg = checkData.msg;
                msg = msg == null ? "输入不合法" : msg;
                /**
                 * 判断最大长度
                 */
                var max = checkData.max;
                if (null != max && value.length > max) {
                    this.setError(input, msg);
                    resutl = false;
                }

                /**
                 * 判断最小长度
                 */
                var min = checkData.min;
                if (null != min && value.length < min) {
                    this.setError(input, msg);
                    resutl = false;
                }

                /**
                 * 正则
                 */
                var reg = checkData.reg;
                if (null != reg && !this.regs()[reg].reg.test(value)) {
                    this.setError(input, msg);
                    resutl = false;
                }

                /**
                 * 必填
                 */
                var rq = checkData.rq;
                if (rq != null && value == "") {
                    this.setError(input, msg);
                    resutl = false;
                }
                var number = checkData.number;
                if (number && !this.regs().number.reg.test(value)) {
                    this.setError(input, msg);
                    resutl = false;
                }
            }
        }
        return resutl;
    },
    /**
     * 设置表单错误提示
     */
    setError: function (obj, msg) {
        obj.addClass("warn");
        /*if (obj.next().length == 0) {
         obj.after($("<div class='error'>" + msg + "</div>"));
         }*/
        obj.focus();
    },
    /**
     * 提示框
     */
    tipMsg: function (config) {
        var type = config.type;
        var content = config.content;
        var timeout = config.timeout;
        var fun = config.fun;
        var icon = "info";
        switch (type) {
            case "loading":
                icon = "loading";
                break;
            case "info":
                icon = "info";
                break;
            case "warn":
                icon = "warn";
                break;
            case "success":
                icon = "success";
                break;
            case "error":
                icon = "error-icon";
                break;
            default:
                icon = "info";
        }
        var content = "<div class='dialog-tip-msg'><div class='dialog-tip-msg-icon " + icon + "'></div><div class='dialog-tip-msg-content'>" + content + "</div></div>";
        var d = dialog({
            content: content
        });
        if (type == "loading") {
            d.showModal();
        } else {
            d.show();
        }
        if (null !== timeout && typeof (timeout) != "undefined" && timeout != 0) {
            setTimeout(function () {
                d.remove();
                fun == null ? "" : fun();
            }, timeout)
        }
        return d;
    },
    dialog: function (config) {
        var title = config.title
        var content = config.content;
        var okfun = config.okfun;
        var cancelfun = config.cancelfun;
        var align = config.align;
        var d = dialog({
            title: title, content: content, button: [{
                value: '取消'
            }, {
                value: '确定', callback: function () {
                    if (okfun != null) {
                        okfun();
                        return false;
                    }
                }, autofocus: true
            }]
        });
        d.showModal();
        return d;
    },
    /**
     * ajax请求
     */
    ajaxRequest: function (config) {
        var async = config.asycn = null ? true : config.asycn;
        var url = config.url;
        var data = config.data || "";
        var fun = config.fun;
        var showLoad = config.showLoad == null ? true : config.showLoad;
        if (showLoad) {
            var d = this.tipMsg({
                type: "loading", content: "加载中......"
            });
        }

        var that = this;
        $.ajax({
            async: async, cache: false, type: 'POST', dataType: "json", data: data, url: url,
            // 请求的action路径
            success: function (response) {
                if (showLoad) {
                    d.remove();
                }
                if (response.responseCode.code == ulewo.resultCode.LOGINTIMEOUT) { // 超时
                    var url = ulewo.curUrl;
                    that.goLogin();
                } else if (response.responseCode.code == ulewo.resultCode.SUCCESS) { // 请求成功
                    fun(response);
                } else if (response.responseCode.code == ulewo.resultCode.MOREMAXLOGINCOUNT) {
                    $("#refreshCheckCode").find("img").attr("src", "checkCode.do?" + new Date().getTime());
                    fun(response);
                } else if (response.responseCode.code == ulewo.resultCode.NOPERMISSION) { // 没有权限
                    that.tipMsg({
                        type: "error", content: response.errorMsg, timeout: 3000
                    });
                } else if (response.responseCode.code == ulewo.resultCode.CODEERROR) { // 验证码错误
                    that.tipMsg({
                        type: "error", content: response.errorMsg, timeout: 3000
                    });
                    $("#refreshCheckCode").find("img").attr("src", "checkCode.do?" + new Date().getTime());
                } else { // 错误
                    that.tipMsg({
                        type: "error", content: response.errorMsg, timeout: 3000
                    });
                    $("#refreshCheckCode").find("img").attr("src", "checkCode.do?" + new Date().getTime());
                }
            }
        });
    },
    goLogin: function () {
        var url = ulewo.curUrl;
        url = encodeURI(url);
        document.location.href = ulewo.absolutePath + "/mobile/mobile_login?redirect=" + url;
    }
});
