ulewo.url = {
    loadSpitSlot: ulewo.absolutePath + "/loadSpitSlot", saveSpitSlot: ulewo.absolutePath + "/addSpitSlot.action"
}

$(function () {
    loadSpitSlot(1);
    /**
     * 删除图片
     */
    $(document).on("click", "#image-con span a", function () {
        $(this).parent().remove();
    });

    /**
     * 发布吐槽
     */
    $("#post-btn").click(function () {
        postSpitSlot();
    });

    $("#spit-slot-area").keydown(function (event) {
        e = event ? event : (window.event ? window.event : null);
        if (e.ctrlKey && e.keyCode == 13) {
            postSpitSlot();
        }
    })

    var frame_main_height = $(window).height() - $(".header").outerHeight() - $(".footer").outerHeight() - $(".send-spit").outerHeight();
    $("#spit-slot-list").css({"height": frame_main_height + "px"});

});

/**
 * 加载吐槽
 */
function loadSpitSlot(page) {
    ulewo.pageNo = page;
    $("#load-more").remove();
    $('<div id="loading"> <div class="loading-con"><img src="' + ulewo.absolutePath + '/resource/images/loading.gif"/><span>正在加载.......</span></div></div>').appendTo($("#spit-slot-list"));
    ulewo.ajaxRequest({
        url: ulewo.url.loadSpitSlot, data: {
            pageNo: page
        }, showLoad: false, fun: function (res) {
            $("#loading").remove();
            var list = res.data.list;
            var simplePage = res.data.page;
            for (var i = 0, _len = list.length, data; i < _len, data = list[i]; i++) {
                new SpitSlotItem(data).appendTo($("#spit-slot-list"));
            }
            if (simplePage.pageTotal > simplePage.page) {
                $('<div id="load-more" class="load-more"><a href="javascript:;">⇓加载更多</a></div>').appendTo($("#spit-slot-list"));
            }
            /* $("img.lazy-load").lazyload({ 
             effect: "fadeIn"
             }); */
        }
    });
}

/**
 * 提交吐槽
 */
function postSpitSlot() {
    if (ulewo.user.userId == "") {
        var url = ulewo.curUrl;
        url = encodeURI(url);
        document.location.href = "../login?redirect=" + url;
        return;
    }
    var content = $.trim($("#spit-slot-area").val());
    var imageArry = [];
    var imageItems = $("#image-con .imageItem");
    for (var i = 0, _len = imageItems.length; i < _len; i++) {
        imageArry.push(imageItems.eq(i).attr("data"));
    }
    if (content == "") {
        ulewo.tipMsg({
            type: "warn", content: "吐槽内容不能为空", timeout: 1500
        });
        return;
    }
    if (content.length > 1000) {
        ulewo.tipMsg({
            type: "warn", content: "吐槽内容不能超过1000", timeout: 1500
        });
        return;
    }
    ulewo.ajaxRequest({
        url: ulewo.url.saveSpitSlot, data: {
            content: content, imageUrl: imageArry.join("|")
        }, fun: function (res) {
            var item = new SpitSlotItem(res.data)
            $(".spit-item").eq(0).before(item);
            ulewo.tipMsg({
                type: "success", content: "2分已到碗里", timeout: 1500
            });
            clearForm();

            item.find("img").lazyload({
                effect: "fadeIn"
            });
        }
    });
}

function clearForm() {
    $("#spit-slot-area").val("");
    $("#image-con").empty();
}