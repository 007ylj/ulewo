package com.ulewo.service;

import java.util.List;

import com.ulewo.po.query.BookQuery;
import com.ulewo.po.model.Book;
import com.ulewo.exception.BusinessException;
import com.ulewo.po.vo.PaginationResult;


/**
 * 书、专题 业务接口
 */
public interface BookService {

    /**
     * 根据条件查询列表
     */
    public List<Book> findListByParam(BookQuery param);

    /**
     * 根据条件查询列表
     */
    public Integer findCountByParam(BookQuery param);

    /**
     * 分页查询
     */
    public PaginationResult<Book> findListByPage(BookQuery param);

    /**
     * 新增
     */
    public Integer add(Book bean) throws BusinessException;

    /**
     * 批量新增
     */
    public Integer addBatch(List<Book> listBean);

    /**
     * 根据BookId修改
     */
    public Integer updateByBookId(Book bean, Long bookId) throws BusinessException;


    /**
     * 根据BookId删除
     */
    public Integer deleteByBookId(Long bookId) throws BusinessException;


    /**
     * 根据BookId查询对象
     */
    public Book getBookByBookId(Long bookId);

}