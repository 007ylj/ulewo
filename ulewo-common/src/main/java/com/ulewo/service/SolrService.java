package com.ulewo.service;

import java.util.List;

import org.springframework.scheduling.annotation.Async;

import com.ulewo.exception.BusinessException;
import com.ulewo.po.model.SolrBean;
import com.ulewo.po.vo.PaginationResult;

public interface SolrService {
    @Async
    public void addArticle(SolrBean bean);

    public PaginationResult<SolrBean> selectArticle(String keyword, String articleType, Integer pageNo, Integer countTotal) throws BusinessException;

    public void addArticleBatch(List<SolrBean> list);
}
