package com.ulewo.service;

import com.ulewo.exception.BusinessException;
import com.ulewo.po.model.BookChapter;
import com.ulewo.po.query.BookChapterQuery;
import com.ulewo.po.vo.PaginationResult;

import java.util.List;


/**
 * 章节 业务接口
 */
public interface BookChapterService {

    /**
     * 根据条件查询列表
     */
    List<BookChapter> findListByParam(BookChapterQuery param);

    /**
     * 根据条件查询列表
     */
    Integer findCountByParam(BookChapterQuery param);

    /**
     * 分页查询
     */
    PaginationResult<BookChapter> findListByPage(BookChapterQuery param);

    /**
     * 新增
     */
    Integer add(BookChapter bean) throws BusinessException;


    Long saveDraftsArticle(BookChapter bean) throws BusinessException;

    /**
     * 批量新增
     */
    Integer addBatch(List<BookChapter> listBean);

    /**
     * 根据ArticleId修改
     */
    Integer updateByArticleId(BookChapter bean, Long articleId);


    /**
     * 根据ArticleId删除
     */
    Integer deleteByArticleId(Long articleId);


    /**
     * 根据ArticleId查询对象
     */
    BookChapter getBookChapterByArticleId(Long articleId);

}