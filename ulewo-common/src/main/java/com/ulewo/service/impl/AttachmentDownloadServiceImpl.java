/**
 * Project Name:ulewo-web
 * File Name:AttachmentDownloadServiceImpl.java
 * Package Name:com.ulewo.service.impl
 * Date:2015年10月30日下午9:23:55
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ulewo.mapper.AttachmentDownloadMapper;
import com.ulewo.po.enums.OrderByEnum;
import com.ulewo.po.enums.PageSize;
import com.ulewo.po.model.AttachmentDownload;
import com.ulewo.po.query.AttachmentDownloadQuery;
import com.ulewo.po.vo.PaginationResult;
import com.ulewo.po.vo.SimplePage;
import com.ulewo.service.AttachmentDownloadService;

/**
 * ClassName:AttachmentDownloadServiceImpl <br/>
 * Date:     2015年10月30日 下午9:23:55 <br/>
 * @author 多多洛
 * Copyright (c) 2015, ulewo.com All Rights Reserved. 
 */
@Service("attachmentDownloadService")
public class AttachmentDownloadServiceImpl implements AttachmentDownloadService {

    @Resource
    private AttachmentDownloadMapper<AttachmentDownload, AttachmentDownloadQuery> attachmentDownloadMapper;

    @Override
    public PaginationResult<AttachmentDownload> findAttachmentDonwload(AttachmentDownloadQuery query) {
        int count = this.attachmentDownloadMapper.selectCount(query);
        int pageSize = PageSize.SIZE40.getSize();
        int pageNo = 0;
        if (null != query.getPageNo()) {
            pageNo = query.getPageNo();
        }
        SimplePage page = new SimplePage(pageNo, count, pageSize);
        query.setPage(page);
        query.setOrderBy(OrderByEnum.CREATE_TIME_DESC);
        List<AttachmentDownload> list = this.attachmentDownloadMapper.selectList(query);
        PaginationResult<AttachmentDownload> result = new PaginationResult<AttachmentDownload>(page, list);
        return result;
    }
}
