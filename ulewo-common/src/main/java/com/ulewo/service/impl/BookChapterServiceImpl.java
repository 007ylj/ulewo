package com.ulewo.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.alibaba.druid.sql.visitor.functions.Length;
import com.ulewo.mapper.BookChapterMapper;
import com.ulewo.po.enums.*;
import com.ulewo.po.model.Blog;
import com.ulewo.po.model.BookChapter;
import com.ulewo.po.query.BookChapterQuery;
import com.ulewo.po.vo.PaginationResult;
import com.ulewo.po.vo.SimplePage;
import com.ulewo.utils.DateUtil;
import com.ulewo.utils.StringTools;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Service;

import com.ulewo.exception.BusinessException;
import org.springframework.dao.DuplicateKeyException;
import com.ulewo.service.BookChapterService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


/**
 * 章节 业务接口实现
 */
@Service("bookChapterService")
public class BookChapterServiceImpl implements BookChapterService {

    @Resource
    private BookChapterMapper<BookChapter, BookChapterQuery> bookChapterMapper;

    /**
     * 根据条件查询列表
     */
    public List<BookChapter> findListByParam(BookChapterQuery param) {
        List<BookChapter> list = this.bookChapterMapper.selectList(param);
        return list;
    }

    /**
     * 根据条件查询列表
     */
    public Integer findCountByParam(BookChapterQuery param) {
        Integer count = this.bookChapterMapper.selectCount(param);
        return count;
    }

    /**
     * 分页查询方法
     */
    public PaginationResult<BookChapter> findListByPage(BookChapterQuery param) {
        int count = this.bookChapterMapper.selectCount(param);
        int pageSize = param.getPageSize() == null ? PageSize.SIZE15.getSize() : param.getPageSize();
        int pageNo = 0;
        if (null != param.getPageNo()) {
            pageNo = param.getPageNo();
        }
        SimplePage page = new SimplePage(pageNo, count, pageSize);
        param.setPage(page);
        List<BookChapter> list = this.bookChapterMapper.selectList(param);
        PaginationResult<BookChapter> result = new PaginationResult<BookChapter>(page, list);
        return result;
    }


    /**
     * 新增
     */
    public Integer add(BookChapter bean) throws BusinessException {
        try {
            return this.bookChapterMapper.insert(bean);
        } catch (DuplicateKeyException e) {
            throw new BusinessException("信息已经存在");
        }
    }


    @Override
    public Long saveDraftsArticle(BookChapter bean) throws BusinessException {
        if (!StringTools.isEmpty(bean.getTitle()) && bean.getTitle().length() < TextLengthEnum.LENGTH_200.getLength() ||
                !StringTools.isEmpty(bean.getContent()) && bean.getContent().length() < TextLengthEnum.LONGTEXT.getLength()) {
            if (StringTools.isEmpty(bean.getTitle())) {
                bean.setTitle(DateUtil.format(new Date(), DateTimePatternEnum.YYYY_MM_DD_HH_MM_SS.getPattern()) + "编写");
            }
            if (bean.getArticleId() == null) {
                bean.setCreateTime(new Date());
                bean.setStatus(Long.valueOf(BlogStatusEnum.DRAFTS.getType()));
            }
            bookChapterMapper.insertOrUpdate(bean);
            return bean.getArticleId();
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void postArticle(BookChapter bean) throws BusinessException {

    }

    /**
     * 批量新增
     */
    public Integer addBatch(List<BookChapter> listBean) {
        if (listBean == null || listBean.isEmpty()) {
            return 0;
        }
        return this.bookChapterMapper.insertBatch(listBean);
    }

    /**
     * 根据ArticleId修改
     */
    public Integer updateByArticleId(BookChapter bean, Long articleId) {
        return this.bookChapterMapper.updateByArticleId(bean, articleId);
    }


    /**
     * 根据ArticleId删除
     */
    public Integer deleteByArticleId(Long articleId) {
        return this.bookChapterMapper.deleteByArticleId(articleId);
    }


    /**
     * 根据ArticleId获取对象
     */
    public BookChapter getBookChapterByArticleId(Long articleId) {
        return this.bookChapterMapper.selectByArticleId(articleId);
    }

}