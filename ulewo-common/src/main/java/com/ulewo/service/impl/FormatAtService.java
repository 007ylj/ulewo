package com.ulewo.service.impl;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.springframework.stereotype.Component;

import com.ulewo.po.model.User;
import com.ulewo.service.UserService;
import com.ulewo.utils.Constants;
import com.ulewo.utils.ServerUtils;

@Component
public class FormatAtService {

    @Resource
    private UserService userService;

    private static Pattern referer_pattern = Pattern.compile("@([^@\\s\\:\\;\\,\\\\.\\<\\?\\？\\{\\}\\&]{1,})");// @.+?[\\s:]

    private static String userUrl = "/user/";

    public String generateRefererLinks(String msg, Set<Integer> userIds, Integer sendUserId) {

        StringBuilder html = new StringBuilder();
        int lastIdx = 0;
        Matcher matchr = referer_pattern.matcher(msg);
        while (matchr.find()) {
            String origion_str = matchr.group();
            String userName = origion_str.substring(1, origion_str.length()).trim();
            html.append(msg.substring(lastIdx, matchr.start()));
            User user = userService.findUserByUserName(userName);
            if (null != user) {
                html.append("<a href=\"" + ServerUtils.getDomain() + userUrl + user.getUserId() + "\" class=\"referer\" target=\"_blank\">@");
                html.append(userName.trim());
                html.append("</a> ");
                userIds.add(user.getUserId());
            } else if (Constants.AT_ALL.equalsIgnoreCase(userName) && sendUserId.intValue() == Constants.SEND_USERID) {
                html.append("<a href=\"javascript:;\" class=\"referer\" target=\"_blank\">@");
                html.append(userName.trim());
                html.append("</a> ");
                //查询所有用户
                List<User> userList = userService.findAllActiveUser();
                for (User u : userList) {
                    userIds.add(u.getUserId());
                }
            } else {
                html.append(origion_str);
            }
            lastIdx = matchr.end();
        }
        html.append(msg.substring(lastIdx));
        String resultHtml = html.toString();
        resultHtml = formateContentImgTag(resultHtml);
        return resultHtml;
    }

    public String formateContentImgTag(String content) {
        HtmlCleaner htmlCleaner = new HtmlCleaner();
        TagNode allNode = htmlCleaner.clean(content);
        TagNode[] images = allNode.getElementsByName("img", true);
        if (images.length > 0) {
            for (TagNode tag : images) {
                tag.removeAttribute("alt");
                String src = tag.getAttributeByName("src");
                if (null != src && !src.contains("emotion")) {
                    tag.addAttribute("data-original", src);
                    tag.removeAttribute("src");
                    tag.addAttribute("class", "lazy-load");
                }
            }
        }
        content = htmlCleaner.getInnerHtml(allNode).replace("<head />", "").replace("<body>", "").replace("</body>", "");
        return content;
    }
}
