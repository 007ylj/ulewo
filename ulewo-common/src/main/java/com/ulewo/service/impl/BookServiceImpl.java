package com.ulewo.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.soap.Text;

import com.ulewo.po.enums.BookCoverColorEnum;
import com.ulewo.po.enums.PageSize;
import com.ulewo.po.enums.TextLengthEnum;
import com.ulewo.po.vo.PaginationResult;
import com.ulewo.po.vo.SimplePage;
import com.ulewo.utils.StringTools;
import org.springframework.remoting.caucho.BurlapServiceExporter;
import org.springframework.scripting.bsh.BshScriptUtils;
import org.springframework.stereotype.Service;

import com.ulewo.po.query.BookQuery;
import com.ulewo.po.model.Book;
import com.ulewo.exception.BusinessException;
import org.springframework.dao.DuplicateKeyException;
import com.ulewo.mapper.BookMapper;
import com.ulewo.service.BookService;
import org.springframework.util.StringUtils;


/**
 * 书、专题 业务接口实现
 */
@Service("bookService")
public class BookServiceImpl implements BookService {

    private static final int length_150 = TextLengthEnum.LENGTH_150.getLength().intValue();
    private static final int length_500 = TextLengthEnum.LENGTH_500.getLength().intValue();

    @Resource
    private BookMapper<Book, BookQuery> bookMapper;

    /**
     * 根据条件查询列表
     */
    public List<Book> findListByParam(BookQuery param) {
        List<Book> list = this.bookMapper.selectList(param);
        return list;
    }

    /**
     * 根据条件查询列表
     */
    public Integer findCountByParam(BookQuery param) {
        Integer count = this.bookMapper.selectCount(param);
        return count;
    }

    /**
     * 分页查询方法
     */
    public PaginationResult<Book> findListByPage(BookQuery param) {
        int count = this.bookMapper.selectCount(param);
        int pageSize = param.getPageSize() == null ? PageSize.SIZE15.getSize() : param.getPageSize();
        int pageNo = 0;
        if (null != param.getPageNo()) {
            pageNo = param.getPageNo();
        }
        SimplePage page = new SimplePage(pageNo, count, pageSize);
        param.setPage(page);
        List<Book> list = this.bookMapper.selectList(param);
        PaginationResult<Book> result = new PaginationResult<Book>(page, list);
        return result;
    }

    /**
     * 新增
     */
    public Integer add(Book bean) throws BusinessException {
        if (StringTools.isEmpty(bean.getBookName()) || bean.getBookName().length() > length_150) {
            throw new BusinessException("名称不能为空，且不能超过" + length_150);
        }
        if (StringTools.isEmpty(bean.getBookDesc()) || bean.getBookName().length() > length_500) {
            throw new BusinessException("描述不能为空，且不能超过" + length_150);
        }
        BookCoverColorEnum coverColorEnum = BookCoverColorEnum.getColorByColorCode(bean.getCoverBackground());
        if (coverColorEnum == null) {
            throw new BusinessException("背景颜色不正确");
        }
        try {
            bean.setCreateTime(new Date());
            return this.bookMapper.insert(bean);
        } catch (DuplicateKeyException e) {
            throw new BusinessException("信息已经存在");
        }
    }

    /**
     * 批量新增
     */
    public Integer addBatch(List<Book> listBean) {
        if (listBean == null || listBean.isEmpty()) {
            return 0;
        }
        return this.bookMapper.insertBatch(listBean);
    }

    /**
     * 根据BookId修改
     */
    public Integer updateByBookId(Book bean, Long bookId) throws BusinessException {
        if (null == bookId) {
            throw new BusinessException("请求参数错误");
        }
        if (StringTools.isEmpty(bean.getBookName()) || bean.getBookName().length() > length_150) {
            throw new BusinessException("名称不能为空，且不能超过" + length_150);
        }
        if (StringTools.isEmpty(bean.getBookDesc()) || bean.getBookName().length() > length_500) {
            throw new BusinessException("描述不能为空，且不能超过" + length_150);
        }
        BookCoverColorEnum coverColorEnum = BookCoverColorEnum.getColorByColorCode(bean.getCoverBackground());
        if (coverColorEnum == null) {
            throw new BusinessException("背景颜色不正确");
        }
        return this.bookMapper.updateByBookId(bean, bookId);
    }


    /**
     * 根据BookId删除
     */
    public Integer deleteByBookId(Long bookId) throws BusinessException {
        if (null == bookId) {
            throw new BusinessException("请求参数错误");
        }
        Book book = getBookByBookId(bookId);
        if (book == null) {
            throw new BusinessException("记录不存在");
        }
        if (book.getChapterCount() > 0) {
            throw new BusinessException("请先删除文章再删除此项");
        }
        return this.bookMapper.deleteByBookId(bookId);
    }

    /**
     * 根据BookId获取对象
     */
    public Book getBookByBookId(Long bookId) {
        return this.bookMapper.selectByBookId(bookId);
    }
}