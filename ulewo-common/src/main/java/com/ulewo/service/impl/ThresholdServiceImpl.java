/**
 * Project Name:ulewo-common
 * File Name:ThresholdServiceImpl.java
 * Package Name:com.ulewo.service.impl
 * Date:2016年1月16日下午4:23:36
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ulewo.exception.BusinessException;
import com.ulewo.mapper.ThresholdMapper;
import com.ulewo.po.config.ConfigInfo;
import com.ulewo.po.model.Threshold;
import com.ulewo.po.query.ThresholdQuery;
import com.ulewo.service.ThresholdService;

/**
 * ClassName:ThresholdServiceImpl <br/>
 * Date:     2016年1月16日 下午4:23:36 <br/>
 * @author 多多洛
 *         Copyright (c) 2016, ulewo.com All Rights Reserved.
 */
@Service
public class ThresholdServiceImpl implements ThresholdService {

    @Resource
    private ConfigInfo configInfo;

    @Resource
    private ThresholdMapper<Threshold, ThresholdQuery> thresholdMapper;

    @Override
    public void canOp(Integer userId) throws BusinessException {
        ThresholdQuery query = new ThresholdQuery();
        query.setUserId(userId);
        List<Threshold> list = thresholdMapper.selectList(query);
        Threshold threshold = new Threshold();
        threshold.setUserId(userId);
        if (!list.isEmpty() && (list.get(0).getOpCount()) >= configInfo.getOpThresholdCount()) {
            throw new BusinessException("你今天的写入操作已经超过20次，我怀疑你在刷积分");
        }
        thresholdMapper.insert(threshold);
    }
}
