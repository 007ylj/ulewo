/**
 * Project Name:ulewo-common
 * File Name:SpiderServiceImpl.java
 * Package Name:com.ulewo.service.impl
 * Date:2016年2月25日上午11:50:50
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.mysql.jdbc.log.Log;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ulewo.mapper.TopicMapper;
import com.ulewo.po.enums.TextLengthEnum;
import com.ulewo.po.enums.TopicType;
import com.ulewo.po.model.Spider;
import com.ulewo.po.model.Topic;
import com.ulewo.po.query.TopicQuery;
import com.ulewo.service.SpiderService;
import com.ulewo.utils.ImageUtils;
import com.ulewo.utils.SpiderUtil;
import com.ulewo.utils.StringTools;

/**
 * ClassName:SpiderServiceImpl <br/>
 * Date:     2016年2月25日 上午11:50:50 <br/>
 * @author 多多洛
 *         Copyright (c) 2016, ulewo.com All Rights Reserved.
 */
@Service("spiderService")
public class SpiderServiceImpl implements SpiderService {

    Logger logger = LoggerFactory.getLogger(SpiderServiceImpl.class);

    @Resource
    private TopicMapper<Topic, TopicQuery> topicMapper;

    private static final int NEWS_CATEGORYID = 10055;

    private static final int NEWS_P_CATEGORYID = 10049;

    private static final Integer USERID = 10000;

    private static final String USER_ICON = "avatars/10000.jpg";

    private static final String USER_NAME = "ulewo";

    @Override
    public void spiderNews() {
        try {
            List<Spider> spiderList = SpiderUtil.getOSCNewsList();
            TopicQuery query = new TopicQuery();
            for (Spider spider : spiderList) {
                try {
                    String title = spider.getTitle();
                    query.setCategoryId(NEWS_CATEGORYID);
                    query.setTitle(title);
                    int count = topicMapper.selectCount(query);
                    if (count > 0) {
                        continue;
                    }
                    Topic topic = new Topic();
                    topic.setTopicType(TopicType.NORMAL);
                    topic.setContent(SpiderUtil.getDetail(spider.getUrl()));
                    topic.setTitle(title);
                    topic.setCategoryId(NEWS_CATEGORYID);
                    topic.setpCategoryId(NEWS_P_CATEGORYID);
                    topic.setUserIcon(USER_ICON);
                    topic.setUserName(USER_NAME);
                    topic.setUserId(USERID);
                    topic.setReadCount((int) (Math.random() * 100));
                    String content = topic.getContent();
                    String summary = StringTools.clearHtmlTag(content);
                    if (summary.length() > TextLengthEnum.LENGTH_200.getLength()) {
                        summary = summary.substring(0, TextLengthEnum.LENGTH_200.getLength().intValue()) + "......";
                    }
                    topic.setSummary(summary);
                    topic.setContent(content);
                    String topicImage = ImageUtils.getImages(content);
                    topic.setTopicImage(topicImage);
                    String topicImageSmall = ImageUtils.createThumbnail(topicImage, true);
                    topic.setTopicImageThum(topicImageSmall);
                    Date curDate = new Date();
                    topic.setCreateTime(curDate);
                    topic.setLastCommentTime(curDate);
                    this.topicMapper.insert(topic);
                } catch (Exception e) {
                    logger.error("抓取详情异常:url:{}", e, spider.getUrl());
                }
            }
        } catch (Exception e) {
            logger.error("抓取列表异常:", e);
        }
    }
}
