/**
 * Project Name:ulewo-common
 * File Name:SpiderService.java
 * Package Name:com.ulewo.service
 * Date:2016年2月25日上午10:21:51
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.service;

/**
 * ClassName:SpiderService <br/>
 * Date:     2016年2月25日 上午10:21:51 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public interface SpiderService {
    public void spiderNews();
}
