/**
 * Project Name:ulewo-common
 * File Name:ThresholdService.java
 * Package Name:com.ulewo.service
 * Date:2016年1月16日下午4:22:38
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.service;

import com.ulewo.exception.BusinessException;

/**
 * ClassName:ThresholdService <br/>
 * Date:     2016年1月16日 下午4:22:38 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public interface ThresholdService {
    public void canOp(Integer userId) throws BusinessException;
}
