/**
 * Project Name:ulewo-web
 * File Name:AttachmentDownloadService.java
 * Package Name:com.ulewo.service
 * Date:2015年10月30日下午9:20:48
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.service;

import com.ulewo.po.model.AttachmentDownload;
import com.ulewo.po.query.AttachmentDownloadQuery;
import com.ulewo.po.vo.PaginationResult;

/**
 * ClassName:AttachmentDownloadService <br/>
 * Date:     2015年10月30日 下午9:20:48 <br/>
 * @author 多多洛
 * Copyright (c) 2015, ulewo.com All Rights Reserved. 
 */
public interface AttachmentDownloadService {
    /**
     *
     * findAttachmentDonwload:(查看下载). <br/>
     *
     * @author 多多洛
     * @param query
     * @return
     * @since JDK 1.7
     */
    public PaginationResult<AttachmentDownload> findAttachmentDonwload(AttachmentDownloadQuery query);
}
