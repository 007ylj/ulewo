/**
 * Project Name:ulewo-web
 * File Name:UserService.java
 * Package Name:com.ulewo.service.impl
 * Date:2015年9月19日下午4:50:03
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.service;

import java.util.List;

import com.ulewo.exception.BusinessException;
import com.ulewo.po.model.User;
import com.ulewo.po.query.UserQuery;
import com.ulewo.po.vo.PaginationResult;
import com.ulewo.po.vo.api.UserLoginInfoVO;

/**
 * ClassName:UserService <br/>
 * Date:     2015年9月19日 下午4:50:03 <br/>
 * @author 多多洛
 *         Copyright (c) 2015, ulewo.com All Rights Reserved.
 */
public interface UserService {
    /**
     * restister:(注册)
     * @param user
     * @exception BusinessException
     * @since JDK 1.7
     */
    public void restister(User user) throws BusinessException;

    /**
     * findUserByUserName:(根据用户名查询用户)
     * @param userName
     * @return
     * @since JDK 1.7
     */
    public User findUserByUserName(String userName);

    /**
     * findUserByEmail:(根据邮箱查询用户)
     * @param email
     * @return
     * @since JDK 1.7
     */
    public User findUserByEmail(String email);

    /**
     * findUserByUserID:(通过userId获取用户信息). <br/>
     * @param userId
     * @return
     * @since JDK 1.7
     */
    public User findUserByUserId(String userId);

    /**
     * login:(登录)
     * @param account
     * @param password
     * @param encodePwd   TODO
     * @param lastLoginIp TODO
     * @return
     * @exception BusinessException
     * @since JDK 1.7
     */
    public User login(String account, String password, Boolean encodePwd, String lastLoginIp) throws BusinessException;

    /**
     * sendCheckCode:(发送验证码)
     * @param email
     * @exception BusinessException
     * @since JDK 1.7
     */
    public void sendCheckCode(String email) throws BusinessException;

    /**
     * resetPwd:(重置密码)
     * @param email
     * @param password
     * @param checkCode
     * @exception BusinessException
     * @since JDK 1.7
     */
    public void resetPwd(String email, String password, String checkCode) throws BusinessException;

    /**
     * addMark:(加积分)
     * @param userId
     * @param mark
     * @return TODO
     * @exception BusinessException
     * @since JDK 1.7
     */
    public int changeMark(Integer userId, Integer mark) throws BusinessException;

    /**
     * update:(更新用户信息). <br/>
     * @exception BusinessException
     * @since JDK 1.7
     */
    public void updateInfo(User user) throws BusinessException;

    /**
     * 更新最后登录时间
     * updateLastLoginTime:(这里用一句话描述这个方法的作用)
     * @param lastLoginIp TODO
     * @param ipAddress   TODO
     * @param user
     * @since JDK 1.7
     */
    public void updateLastLoginInfo(Integer userId, String lastLoginIp, String ipAddress);

    /**
     * changePwd:(修改密码)
     * @param userId
     * @param oldPwd
     * @param newPwd
     * @since JDK 1.7
     */
    public void changePwd(Integer userId, String oldPwd, String newPwd) throws BusinessException;

    /**
     * copyUserIcon:(复制用户头像)
     * @param sourceIcon
     * @param targetIcon
     * @since JDK 1.7
     */
    public void copyUserIcon(String sourceIcon, String targetIcon);

    /**
     * findUserInfo4UserHome:(查询个人主页信息)
     * @param userId
     * @param sessionUserId TODO
     * @return
     * @exception BusinessException
     * @since JDK 1.7
     */
    public User findUserInfo4UserHome(Integer userId, Integer sessionUserId) throws BusinessException;

    /**
     * findUserByPage:(分页查询用户)
     * @param query
     * @return
     * @since JDK 1.7
     */
    public PaginationResult<User> findUserByPage(UserQuery query);

    /**
     * rewardMark:(奖励积分)
     * @param userId
     * @param mark
     * @since JDK 1.7
     */
    public void rewardMark(Integer userId, Integer mark, String message);

    /**
     * 查询活跃用户
     * findAllActiveUser:(这里用一句话描述这个方法的作用). <br/>
     * @return
     * @since JDK 1.7
     */
    public List<User> findAllActiveUser();

    /**
     * warnUser:(发送警告信息)
     * @param userId
     * @param message
     * @since JDK 1.7
     */
    public void warnUser(Integer userId, String message);

    /**
     * 删除用户
     * delete:(这里用一句话描述这个方法的作用)
     * @param userId
     * @since JDK 1.7
     */
    public void delete(Integer userId);

    /**
     * api登录
     * @param account
     * @param password
     * @param deviceNo
     * @return
     * @exception BusinessException
     */
    public UserLoginInfoVO login4Api(String account, String password) throws BusinessException;


    public String recaptureToken(String userId, String password) throws BusinessException;
}
