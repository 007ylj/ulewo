/**
 * Project Name:ulewo-common
 * File Name:MessageSettingsService.java
 * Package Name:com.ulewo.service
 * Date:2016年3月19日下午6:11:49
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.service;

import java.util.List;

import com.ulewo.exception.BusinessException;
import com.ulewo.po.model.MessageSettings;
import com.ulewo.po.query.MessageSettingsQuery;

/**
 * ClassName:MessageSettingsService <br/>
 * Date:     2016年3月19日 下午6:11:49 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public interface MessageSettingsService {
    public MessageSettings getMessageSettingsByUserId(Integer userId);

    public void insert(MessageSettings settings) throws BusinessException;

    public void saveSettings(MessageSettings settings) throws BusinessException;

    public List<MessageSettings> selectList(MessageSettingsQuery query);

}
