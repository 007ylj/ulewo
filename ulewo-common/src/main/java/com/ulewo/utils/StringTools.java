/**
 * Project Name:ulewo-common
 * File Name:StringTools.java
 * Package Name:com.ulewo.utils
 * Date:2015年9月19日下午5:19:30
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.utils;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ClassName:StringTools <br/>
 * Date:     2015年9月19日 下午5:19:30 <br/>
 * @author 多多洛
 *         Copyright (c) 2015, ulewo.com All Rights Reserved.
 */
public class StringTools {

    private final static String[] hexDigits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
            "e", "f"};
    private final static String[] static_ext = {"jpg", "png", "gif", "bmp", "JPG", "PNG", "GIF", "BMP"};

    // MD5加密
    public static String encodeByMD5(String originString) {

        if (originString != null) {
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] results = md.digest(originString.getBytes());
                String resultString = byteArrayToHexString(results);
                return resultString.toUpperCase();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private static String byteArrayToHexString(byte[] b) {

        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            resultSb.append(byteToHexString(b[i]));
        }
        return resultSb.toString();
    }

    private static String byteToHexString(byte b) {

        int n = b;
        if (n < 0) n = 256 + n;
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }

    /**
     * 判断为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {

        if (null == str || "".equals(str) || "null".equals(str)) {
            return true;
        } else if ("".equals(str.trim())) {
            return true;
        }
        return false;
    }

    public static boolean isNumber(String str) {

        String checkPassWord = "^[0-9]+$";
        if (null == str) {
            return false;
        }
        if (!str.matches(checkPassWord)) {
            return false;
        }

        return true;
    }

    public static boolean checkEmail(String email) {
        String checkEmail = "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$";
        if (!isEmpty(email)) {
            return email.matches(checkEmail);
        } else {
            return false;
        }
    }

    public static boolean checkUserName(String userName) {
        String checkUserName = "^[\\w\\u4e00-\\u9fa5]+$";
        if (!isEmpty(userName)) {
            return userName.matches(checkUserName);
        } else {
            return false;
        }
    }

    public static boolean checkPassWord(String password) {
        String checkPassWord = "^[0-9a-zA-Z]+$";
        if (!isEmpty(password)) {
            return password.matches(checkPassWord);
        } else {
            return false;
        }
    }

    public static String addLink(String str) {
        String regex = "((http:|https:)//|www.)[^[A-Za-z0-9\\._\\?%&+\\-=/#;]]*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        StringBuffer result = new StringBuffer();
        while (matcher.find()) {
            StringBuffer replace = new StringBuffer();
            if (matcher.group().contains("http")) {
                replace.append("<a href=").append(matcher.group().replace("&nbsp;", ""));
            } else {
                replace.append("<a href=http://").append(matcher.group().replace("&nbsp;", ""));
            }
            replace.append(" target=\"_blank\">" + matcher.group().replace("&nbsp;", "") + "</a>&nbsp;");
            matcher.appendReplacement(result, replace.toString());
        }
        matcher.appendTail(result);
        return result.toString();
    }

    public static String escapeHtml(String html) {

        if (!isEmpty(html)) {
            html = html.replaceAll("\n", "<br>");
            html = html.replaceAll(" ", "&nbsp;");
            return html;
        }
        return html;
    }

    public static String reFormateHtml(String html) {
        if (!isEmpty(html)) {
            html = html.replaceAll("&nbsp;", " ");
            html = html.replaceAll("&lt;", "<");
            html = html.replaceAll("<br>", "\n");
            return html;
        }
        return html;
    }

    /**
     * isNew:(是否是两天以内). <br/>
     * @param dateStr
     * @return
     * @since JDK 1.7
     */
    public static boolean isNew(Date date) {
        if (null == date) {
            return false;
        }
        Calendar c = Calendar.getInstance();
        long max = c.getTimeInMillis() - date.getTime();
        if (max / 86400000 <= 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * clearHtml:(清除html标签). <br/>
     * @param str
     * @return
     * @since JDK 1.7
     */
    public static String clearHtmlTag(String str) {

        if (!isEmpty(str)) {
            return str.replaceAll("<[.[^<]]*>", "").replaceAll("[\\n|\\r]", "").replaceAll("&nbsp;", "");
        } else {
            return str;
        }
    }

    public static String getUUID() {
        String s = UUID.randomUUID().toString();
        //去掉“-”符号
        return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
    }

    public static String cleanATag(String str) {
        if (null != str) {
            str = str.replaceAll("<a href[^>]*>", "");
            str = str.replaceAll("</a>", "");
        }
        return str;
    }

    public static boolean isIP(String addr) {
        if (addr.length() < 7 || addr.length() > 15 || "".equals(addr)) {
            return false;
        }
        /**
         * 判断IP格式和范围
         */
        String rexp = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";

        Pattern pat = Pattern.compile(rexp);

        Matcher mat = pat.matcher(addr);

        boolean ipAddress = mat.find();

        return ipAddress;
    }


    public static boolean filterEmoji(String source) {
        if (source != null) {
            Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE |
                    Pattern.CASE_INSENSITIVE);
            Matcher emojiMatcher = emoji.matcher(source);
            if (emojiMatcher.find()) {
                return true;
            }
            return false;
        }
        return false;
    }

    public static void main(String[] args) {
        /*String str = "sdfshttp://www.ulewo.com/sdfsd/ddd";
        System.out.println(addLink(str));*/

       /* String s = "<a href=hjkhkhhk>daafadfafdadfa</a><img src='dsafasdfa'/>";
        s = s.replaceAll("<a href[^>]*>", "");
        s = s.replaceAll("</a>", "");
        s = s.replaceAll("<img[^>]*//*>", " img");
        System.out.println(s);*/

        System.out.print("有人使用Linux 进行日常的开发吗？公司里面的一些人安装win 10系统（然并卵，装逼的意义大于实际意义，出现了问题有时候并不会解决），前段时间一个同事也买了苹果电脑进行开发（电脑的配置也是一般，装".length());
    }
}
