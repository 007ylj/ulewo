/**
 * Project Name:ulewo-common
 * File Name:MessageSettings.java
 * Package Name:com.ulewo.po.model
 * Date:2016年3月19日下午6:00:04
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.model;

/**
 * ClassName:MessageSettings <br/>
 * Date:     2016年3月19日 下午6:00:04 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public class MessageSettings {
    private Integer userId;
    private String email;
    private Integer msgType;
    private Integer noticeSignInType;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public Integer getNoticeSignInType() {
        return noticeSignInType;
    }

    public void setNoticeSignInType(Integer noticeSignInType) {
        this.noticeSignInType = noticeSignInType;
    }

}
