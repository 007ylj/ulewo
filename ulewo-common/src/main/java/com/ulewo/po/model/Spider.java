/**
 * Project Name:ulewo-common
 * File Name:Spider.java
 * Package Name:com.ulewo.po.model
 * Date:2016年2月25日上午11:43:27
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.model;

/**
 * ClassName:Spider <br/>
 * Date:     2016年2月25日 上午11:43:27 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public class Spider {
    private String url;
    private String title;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
