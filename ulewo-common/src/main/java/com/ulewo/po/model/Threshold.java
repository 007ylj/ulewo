/**
 * Project Name:ulewo-common
 * File Name:Threshold.java
 * Package Name:com.ulewo.po.model
 * Date:2016年1月16日下午4:13:26
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.model;

import java.util.Date;

/**
 * ClassName:Threshold <br/>
 * Date:     2016年1月16日 下午4:13:26 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public class Threshold {
    private Integer id;
    private Integer userId;
    private Date opDate;
    private Integer opCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getOpDate() {
        return opDate;
    }

    public void setOpDate(Date opDate) {
        this.opDate = opDate;
    }

    public Integer getOpCount() {
        return opCount;
    }

    public void setOpCount(Integer opCount) {
        this.opCount = opCount;
    }

}
