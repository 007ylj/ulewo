package com.ulewo.po.model;

import java.io.Serializable;


/**
 * 
 * 章节
 * 
 */
@SuppressWarnings("serial")
public class BookChapter implements Serializable {


	/**
	 * 
	 */
	private Long articleId;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 用户小图像
	 */
	private String userIcon;

	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 书ID
	 */
	private Long bookId;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 摘要
	 */
	private String summary;

	/**
	 * 内容
	 */
	private String content;

	/**
	 * 阅读数
	 */
	private Long readCount;

	/**
	 * 关键词
	 */
	private String keyWord;

	/**
	 * 是否允许评论 0：允许 1不允许
	 */
	private Long allowComment;

	/**
	 * 缩略图
	 */
	private String articleImageThum;

	/**
	 * 图片
	 */
	private String articleImage;

	/**
	 * 评论数
	 */
	private Long commentCount;

	/**
	 * 喜欢数
	 */
	private Long likeCount;

	/**
	 * 收藏数
	 */
	private Long collectionCount;

	/**
	 * 状态 0：草稿 1：已经发布
	 */
	private Long status;

	/**
	 * 创建时间
	 */
	private java.util.Date createTime;

	/**
	 * 排序
	 */
	private Long sort;


	public void setArticleId(Long articleId){
		this.articleId = articleId;
	}

	public Long getArticleId(){
		return this.articleId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserIcon(String userIcon){
		this.userIcon = userIcon;
	}

	public String getUserIcon(){
		return this.userIcon;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return this.userName;
	}

	public void setBookId(Long bookId){
		this.bookId = bookId;
	}

	public Long getBookId(){
		return this.bookId;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return this.title;
	}

	public void setSummary(String summary){
		this.summary = summary;
	}

	public String getSummary(){
		return this.summary;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return this.content;
	}

	public void setReadCount(Long readCount){
		this.readCount = readCount;
	}

	public Long getReadCount(){
		return this.readCount;
	}

	public void setKeyWord(String keyWord){
		this.keyWord = keyWord;
	}

	public String getKeyWord(){
		return this.keyWord;
	}

	public void setAllowComment(Long allowComment){
		this.allowComment = allowComment;
	}

	public Long getAllowComment(){
		return this.allowComment;
	}

	public void setArticleImageThum(String articleImageThum){
		this.articleImageThum = articleImageThum;
	}

	public String getArticleImageThum(){
		return this.articleImageThum;
	}

	public void setArticleImage(String articleImage){
		this.articleImage = articleImage;
	}

	public String getArticleImage(){
		return this.articleImage;
	}

	public void setCommentCount(Long commentCount){
		this.commentCount = commentCount;
	}

	public Long getCommentCount(){
		return this.commentCount;
	}

	public void setLikeCount(Long likeCount){
		this.likeCount = likeCount;
	}

	public Long getLikeCount(){
		return this.likeCount;
	}

	public void setCollectionCount(Long collectionCount){
		this.collectionCount = collectionCount;
	}

	public Long getCollectionCount(){
		return this.collectionCount;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setCreateTime(java.util.Date createTime){
		this.createTime = createTime;
	}

	public java.util.Date getCreateTime(){
		return this.createTime;
	}

	public void setSort(Long sort){
		this.sort = sort;
	}

	public Long getSort(){
		return this.sort;
	}

	public String toString (){
		return "articleId:"+(articleId == null ? "空" : articleId)+"，用户ID:"+(userId == null ? "空" : userId)+"，用户小图像:"+(userIcon == null ? "空" : userIcon)+"，用户名:"+(userName == null ? "空" : userName)+"，书ID:"+(bookId == null ? "空" : bookId)+"，标题:"+(title == null ? "空" : title)+"，摘要:"+(summary == null ? "空" : summary)+"，内容:"+(content == null ? "空" : content)+"，阅读数:"+(readCount == null ? "空" : readCount)+"，关键词:"+(keyWord == null ? "空" : keyWord)+"，是否允许评论 0：允许 1不允许:"+(allowComment == null ? "空" : allowComment)+"，缩略图:"+(articleImageThum == null ? "空" : articleImageThum)+"，图片:"+(articleImage == null ? "空" : articleImage)+"，评论数:"+(commentCount == null ? "空" : commentCount)+"，喜欢数:"+(likeCount == null ? "空" : likeCount)+"，收藏数:"+(collectionCount == null ? "空" : collectionCount)+"，状态 0：草稿 1：已经发布:"+(status == null ? "空" : status)+"，创建时间:"+(createTime == null ? "空" : createTime)+"，排序:"+(sort == null ? "空" : sort);
	}
}
