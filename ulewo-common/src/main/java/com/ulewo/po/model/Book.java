package com.ulewo.po.model;

import java.io.Serializable;


/**
 * 
 * 书、专题
 * 
 */
@SuppressWarnings("serial")
public class Book implements Serializable {


	/**
	 * bookId
	 */
	private Long bookId;

	/**
	 * 封面图片路径
	 */
	private String coverImage;

	/**
	 * 封面背景颜色
	 */
	private String coverBackground;

	/**
	 * 书名
	 */
	private String bookName;

	/**
	 * 描述
	 */
	private String bookDesc;

	/**
	 * 章节数
	 */
	private Long chapterCount;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 用户小图像
	 */
	private String userIcon;

	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 创建时间
	 */
	private java.util.Date createTime;


	public void setBookId(Long bookId){
		this.bookId = bookId;
	}

	public Long getBookId(){
		return this.bookId;
	}

	public void setCoverImage(String coverImage){
		this.coverImage = coverImage;
	}

	public String getCoverImage(){
		return this.coverImage;
	}

	public void setCoverBackground(String coverBackground){
		this.coverBackground = coverBackground;
	}

	public String getCoverBackground(){
		return this.coverBackground;
	}

	public void setBookName(String bookName){
		this.bookName = bookName;
	}

	public String getBookName(){
		return this.bookName;
	}

	public void setBookDesc(String bookDesc){
		this.bookDesc = bookDesc;
	}

	public String getBookDesc(){
		return this.bookDesc;
	}

	public void setChapterCount(Long chapterCount){
		this.chapterCount = chapterCount;
	}

	public Long getChapterCount(){
		return this.chapterCount;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserIcon(String userIcon){
		this.userIcon = userIcon;
	}

	public String getUserIcon(){
		return this.userIcon;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return this.userName;
	}

	public void setCreateTime(java.util.Date createTime){
		this.createTime = createTime;
	}

	public java.util.Date getCreateTime(){
		return this.createTime;
	}

	public String toString (){
		return "bookId:"+(bookId == null ? "空" : bookId)+"，封面图片路径:"+(coverImage == null ? "空" : coverImage)+"，封面背景颜色:"+(coverBackground == null ? "空" : coverBackground)+"，书名:"+(bookName == null ? "空" : bookName)+"，描述:"+(bookDesc == null ? "空" : bookDesc)+"，章节数:"+(chapterCount == null ? "空" : chapterCount)+"，用户ID:"+(userId == null ? "空" : userId)+"，用户小图像:"+(userIcon == null ? "空" : userIcon)+"，用户名:"+(userName == null ? "空" : userName)+"，创建时间:"+(createTime == null ? "空" : createTime);
	}
}
