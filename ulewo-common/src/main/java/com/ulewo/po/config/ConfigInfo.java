package com.ulewo.po.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * ClassName: ConfigInfo
 * date: 2015年8月9日 下午12:03:51
 * @author 多多洛
 * @since JDK 1.7
 */
@Component("configInfo")
public class ConfigInfo {
    /**
     * 找回密码发送的邮箱地址
     */
    @Value("#{applicationProperties['ulewo.email.findemail']}")
    private String findemail;

    /**
     * 找回密码发送邮箱的密码
     */
    @Value("#{applicationProperties['ulewo.emial.findpwd']}")
    private String findpwd;

    @Value("#{applicationProperties['ulewo.solr.server.url']}")
    private String solrServerUrl;

    @Value("#{applicationProperties['ulewo.solr.time.out']}")
    private int solrTimeOut;

    @Value("#{applicationProperties['ulewo.solr.max.totalconnections']}")
    private int maxTotalConnections;

    @Value("#{applicationProperties['ulewo.solr.max.connections.per.host']}")
    private int maxConnectionsPerHost;

    @Value("#{applicationProperties['ulewo.solr.open']}")
    private boolean openSolr;

    @Value("#{applicationProperties['ulewo.op.threshold.count']}")
    private int opThresholdCount;

    @Value("#{applicationProperties['ulewo.image.domain']}")
    private String imageDomain;

    @Value("#{applicationProperties['ulewo.image.folder']}")
    private String imageFolder;

    @Value("#{applicationProperties['api.login.aes.key']}")
    private String apiLoginAesKey;

    @Value("#{applicationProperties['api.update.android.version']}")
    private String apiUpdateAndroidVersion;

    @Value("#{applicationProperties['api.update.android.desc']}")
    private String apiUpdateAndroidDesc;

    public String getFindemail() {
        return findemail;
    }

    public void setFindemail(String findemail) {
        this.findemail = findemail;
    }

    public String getFindpwd() {
        return findpwd;
    }

    public void setFindpwd(String findpwd) {
        this.findpwd = findpwd;
    }

    public String getSolrServerUrl() {
        return solrServerUrl;
    }

    public void setSolrServerUrl(String solrServerUrl) {
        this.solrServerUrl = solrServerUrl;
    }

    public int getSolrTimeOut() {
        return solrTimeOut;
    }

    public void setSolrTimeOut(int solrTimeOut) {
        this.solrTimeOut = solrTimeOut;
    }

    public int getMaxTotalConnections() {
        return maxTotalConnections;
    }

    public void setMaxTotalConnections(int maxTotalConnections) {
        this.maxTotalConnections = maxTotalConnections;
    }

    public int getMaxConnectionsPerHost() {
        return maxConnectionsPerHost;
    }

    public void setMaxConnectionsPerHost(int maxConnectionsPerHost) {
        this.maxConnectionsPerHost = maxConnectionsPerHost;
    }

    public boolean isOpenSolr() {
        return openSolr;
    }

    public void setOpenSolr(boolean openSolr) {
        this.openSolr = openSolr;
    }

    public int getOpThresholdCount() {
        return opThresholdCount;
    }

    public void setOpThresholdCount(int opThresholdCount) {
        this.opThresholdCount = opThresholdCount;
    }

    public String getImageDomain() {
        return imageDomain;
    }

    public void setImageDomain(String imageDomain) {
        this.imageDomain = imageDomain;
    }

    public String getImageFolder() {
        return imageFolder;
    }

    public void setImageFolder(String imageFolder) {
        this.imageFolder = imageFolder;
    }

    public String getApiLoginAesKey() {
        return apiLoginAesKey;
    }

    public void setApiLoginAesKey(String apiLoginAesKey) {
        this.apiLoginAesKey = apiLoginAesKey;
    }

    public String getApiUpdateAndroidVersion() {
        return apiUpdateAndroidVersion;
    }

    public void setApiUpdateAndroidVersion(String apiUpdateAndroidVersion) {
        this.apiUpdateAndroidVersion = apiUpdateAndroidVersion;
    }

    public String getApiUpdateAndroidDesc() {
        return apiUpdateAndroidDesc;
    }

    public void setApiUpdateAndroidDesc(String apiUpdateAndroidDesc) {
        this.apiUpdateAndroidDesc = apiUpdateAndroidDesc;
    }
}