package com.ulewo.po.query;


/**
 * 章节参数
 */
public class BookChapterQuery extends BaseQuery {


    /**
     *
     */
    private Long articleId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户小图像
     */
    private String userIcon;

    private String userIconFuzzy;

    /**
     * 用户名
     */
    private String userName;

    private String userNameFuzzy;

    /**
     * 书ID
     */
    private Long bookId;

    /**
     * 标题
     */
    private String title;

    private String titleFuzzy;

    /**
     * 摘要
     */
    private String summary;

    private String summaryFuzzy;

    /**
     * 内容
     */
    private String content;

    private String contentFuzzy;

    /**
     * 阅读数
     */
    private Long readCount;

    /**
     * 关键词
     */
    private String keyWord;

    private String keyWordFuzzy;

    /**
     * 是否允许评论 0：允许 1不允许
     */
    private Long allowComment;

    /**
     * 缩略图
     */
    private String articleImageThum;

    private String articleImageThumFuzzy;

    /**
     * 图片
     */
    private String articleImage;

    private String articleImageFuzzy;

    /**
     * 评论数
     */
    private Long commentCount;

    /**
     * 喜欢数
     */
    private Long likeCount;

    /**
     * 收藏数
     */
    private Long collectionCount;

    /**
     * 状态 0：草稿 1：已经发布
     */
    private Long status;

    /**
     * 创建时间
     */
    private java.util.Date createTime;

    private String createTimeStart;

    private String createTimeEnd;

    /**
     * 排序
     */
    private Long sort;


    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Long getArticleId() {
        return this.articleId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getUserIcon() {
        return this.userIcon;
    }

    public void setUserIconFuzzy(String userIconFuzzy) {
        this.userIconFuzzy = userIconFuzzy;
    }

    public String getUserIconFuzzy() {
        return this.userIconFuzzy;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserNameFuzzy(String userNameFuzzy) {
        this.userNameFuzzy = userNameFuzzy;
    }

    public String getUserNameFuzzy() {
        return this.userNameFuzzy;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getBookId() {
        return this.bookId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitleFuzzy(String titleFuzzy) {
        this.titleFuzzy = titleFuzzy;
    }

    public String getTitleFuzzy() {
        return this.titleFuzzy;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummaryFuzzy(String summaryFuzzy) {
        this.summaryFuzzy = summaryFuzzy;
    }

    public String getSummaryFuzzy() {
        return this.summaryFuzzy;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setContentFuzzy(String contentFuzzy) {
        this.contentFuzzy = contentFuzzy;
    }

    public String getContentFuzzy() {
        return this.contentFuzzy;
    }

    public void setReadCount(Long readCount) {
        this.readCount = readCount;
    }

    public Long getReadCount() {
        return this.readCount;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getKeyWord() {
        return this.keyWord;
    }

    public void setKeyWordFuzzy(String keyWordFuzzy) {
        this.keyWordFuzzy = keyWordFuzzy;
    }

    public String getKeyWordFuzzy() {
        return this.keyWordFuzzy;
    }

    public void setAllowComment(Long allowComment) {
        this.allowComment = allowComment;
    }

    public Long getAllowComment() {
        return this.allowComment;
    }

    public void setArticleImageThum(String articleImageThum) {
        this.articleImageThum = articleImageThum;
    }

    public String getArticleImageThum() {
        return this.articleImageThum;
    }

    public void setArticleImageThumFuzzy(String articleImageThumFuzzy) {
        this.articleImageThumFuzzy = articleImageThumFuzzy;
    }

    public String getArticleImageThumFuzzy() {
        return this.articleImageThumFuzzy;
    }

    public void setArticleImage(String articleImage) {
        this.articleImage = articleImage;
    }

    public String getArticleImage() {
        return this.articleImage;
    }

    public void setArticleImageFuzzy(String articleImageFuzzy) {
        this.articleImageFuzzy = articleImageFuzzy;
    }

    public String getArticleImageFuzzy() {
        return this.articleImageFuzzy;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    public Long getCommentCount() {
        return this.commentCount;
    }

    public void setLikeCount(Long likeCount) {
        this.likeCount = likeCount;
    }

    public Long getLikeCount() {
        return this.likeCount;
    }

    public void setCollectionCount(Long collectionCount) {
        this.collectionCount = collectionCount;
    }

    public Long getCollectionCount() {
        return this.collectionCount;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatus() {
        return this.status;
    }

    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTimeStart(String createTimeStart) {
        this.createTimeStart = createTimeStart;
    }

    public String getCreateTimeStart() {
        return this.createTimeStart;
    }

    public void setCreateTimeEnd(String createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
    }

    public String getCreateTimeEnd() {
        return this.createTimeEnd;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Long getSort() {
        return this.sort;
    }

}
