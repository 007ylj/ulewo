package com.ulewo.po.query;


/**
 * 参数
 */
public class Spider360Query extends BaseQuery {


    /**
     *
     */
    private Long id;

    /**
     *
     */
    private Long uid;

    /**
     *
     */
    private String name;

    private String nameFuzzy;

    /**
     *
     */
    private String title;

    private String titleFuzzy;

    /**
     *
     */
    private java.util.Date createTime;

    private String createTimeStart;

    private String createTimeEnd;

    /**
     *
     */
    private Long readCount;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getUid() {
        return this.uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setNameFuzzy(String nameFuzzy) {
        this.nameFuzzy = nameFuzzy;
    }

    public String getNameFuzzy() {
        return this.nameFuzzy;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitleFuzzy(String titleFuzzy) {
        this.titleFuzzy = titleFuzzy;
    }

    public String getTitleFuzzy() {
        return this.titleFuzzy;
    }

    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTimeStart(String createTimeStart) {
        this.createTimeStart = createTimeStart;
    }

    public String getCreateTimeStart() {
        return this.createTimeStart;
    }

    public void setCreateTimeEnd(String createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
    }

    public String getCreateTimeEnd() {
        return this.createTimeEnd;
    }

    public void setReadCount(Long readCount) {
        this.readCount = readCount;
    }

    public Long getReadCount() {
        return this.readCount;
    }

}
