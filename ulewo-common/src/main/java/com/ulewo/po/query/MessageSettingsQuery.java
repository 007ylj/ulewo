/**
 * Project Name:ulewo-common
 * File Name:MessageSettings.java
 * Package Name:com.ulewo.po.model
 * Date:2016年3月19日下午6:00:04
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.query;

/**
 * ClassName:MessageSettings <br/>
 * Date:     2016年3月19日 下午6:00:04 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public class MessageSettingsQuery extends BaseQuery {
    private Integer userId;
    private Integer noticeSignInType;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getNoticeSignInType() {
        return noticeSignInType;
    }

    public void setNoticeSignInType(Integer noticeSignInType) {
        this.noticeSignInType = noticeSignInType;
    }

}
