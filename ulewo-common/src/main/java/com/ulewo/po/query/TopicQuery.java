/**
 * Project Name:ulewo-common
 * File Name:TopicQuery.java
 * Package Name:com.ulewo.po.query
 * Date:2015年10月25日下午3:38:45
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.query;

import com.ulewo.po.enums.OrderByEnum;

/**
 * ClassName:TopicQuery <br/>
 * Date:     2015年10月25日 下午3:38:45 <br/>
 * @author 多多洛
 *         Copyright (c) 2015, ulewo.com All Rights Reserved.
 */
public class TopicQuery extends BaseQuery {
    private Integer topicId;
    private Integer topicType;
    private Integer notContainCategoryId;
    private Integer categoryId;
    private Integer pCategoryId;
    private boolean showContent;
    private OrderByEnum orderBy;
    private String startDate;
    private String endDate;
    private Integer userId;
    private String userName;
    private String title;
    private Integer[] ids;

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public boolean isShowContent() {
        return showContent;
    }

    public void setShowContent(boolean showContent) {
        this.showContent = showContent;
    }

    public OrderByEnum getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(OrderByEnum orderBy) {
        this.orderBy = orderBy;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getpCategoryId() {
        return pCategoryId;
    }

    public void setpCategoryId(Integer pCategoryId) {
        this.pCategoryId = pCategoryId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNotContainCategoryId() {
        return notContainCategoryId;
    }

    public void setNotContainCategoryId(Integer notContainCategoryId) {
        this.notContainCategoryId = notContainCategoryId;
    }

    public Integer[] getIds() {
        return ids;
    }

    public void setIds(Integer[] ids) {
        this.ids = ids;
    }

    public Integer getTopicType() {
        return topicType;
    }

    public void setTopicType(Integer topicType) {
        this.topicType = topicType;
    }
}
