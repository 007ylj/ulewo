/**
 * Project Name:ulewo-common
 * File Name:Threshold.java
 * Package Name:com.ulewo.po.model
 * Date:2016年1月16日下午4:13:26
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.query;

/**
 * ClassName:Threshold <br/>
 * Date:     2016年1月16日 下午4:13:26 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public class ThresholdQuery {
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
