package com.ulewo.po.query;


/**
 * 书、专题参数
 */
public class BookQuery extends BaseQuery {


    /**
     * bookId
     */
    private Long bookId;

    /**
     * 封面图片路径
     */
    private String coverImage;

    private String coverImageFuzzy;

    /**
     * 封面背景颜色
     */
    private String coverBackground;

    private String coverBackgroundFuzzy;

    /**
     * 书名
     */
    private String bookName;

    private String bookNameFuzzy;

    /**
     * 描述
     */
    private String bookDesc;

    private String bookDescFuzzy;

    /**
     * 章节数
     */
    private Long chapterCount;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户小图像
     */
    private String userIcon;

    private String userIconFuzzy;

    /**
     * 用户名
     */
    private String userName;

    private String userNameFuzzy;

    /**
     * 创建时间
     */
    private java.util.Date createTime;

    private String createTimeStart;

    private String createTimeEnd;


    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getBookId() {
        return this.bookId;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getCoverImage() {
        return this.coverImage;
    }

    public void setCoverImageFuzzy(String coverImageFuzzy) {
        this.coverImageFuzzy = coverImageFuzzy;
    }

    public String getCoverImageFuzzy() {
        return this.coverImageFuzzy;
    }

    public void setCoverBackground(String coverBackground) {
        this.coverBackground = coverBackground;
    }

    public String getCoverBackground() {
        return this.coverBackground;
    }

    public void setCoverBackgroundFuzzy(String coverBackgroundFuzzy) {
        this.coverBackgroundFuzzy = coverBackgroundFuzzy;
    }

    public String getCoverBackgroundFuzzy() {
        return this.coverBackgroundFuzzy;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookName() {
        return this.bookName;
    }

    public void setBookNameFuzzy(String bookNameFuzzy) {
        this.bookNameFuzzy = bookNameFuzzy;
    }

    public String getBookNameFuzzy() {
        return this.bookNameFuzzy;
    }

    public void setBookDesc(String bookDesc) {
        this.bookDesc = bookDesc;
    }

    public String getBookDesc() {
        return this.bookDesc;
    }

    public void setBookDescFuzzy(String bookDescFuzzy) {
        this.bookDescFuzzy = bookDescFuzzy;
    }

    public String getBookDescFuzzy() {
        return this.bookDescFuzzy;
    }

    public void setChapterCount(Long chapterCount) {
        this.chapterCount = chapterCount;
    }

    public Long getChapterCount() {
        return this.chapterCount;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getUserIcon() {
        return this.userIcon;
    }

    public void setUserIconFuzzy(String userIconFuzzy) {
        this.userIconFuzzy = userIconFuzzy;
    }

    public String getUserIconFuzzy() {
        return this.userIconFuzzy;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserNameFuzzy(String userNameFuzzy) {
        this.userNameFuzzy = userNameFuzzy;
    }

    public String getUserNameFuzzy() {
        return this.userNameFuzzy;
    }

    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTimeStart(String createTimeStart) {
        this.createTimeStart = createTimeStart;
    }

    public String getCreateTimeStart() {
        return this.createTimeStart;
    }

    public void setCreateTimeEnd(String createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
    }

    public String getCreateTimeEnd() {
        return this.createTimeEnd;
    }

}
