package com.ulewo.po.query;

import com.ulewo.po.enums.OrderByEnum;

/**
 * TODO: 增加描述
 * @author luohaili
 * @version 0.1.0
 */
public class AttachmentDownloadQuery extends BaseQuery {
    private Integer attachmentId;
    private Integer userId;
    private OrderByEnum orderBy;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public OrderByEnum getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(OrderByEnum orderBy) {
        this.orderBy = orderBy;
    }
}
