package com.ulewo.po.vo;/**
 * Created by Administrator on 2016/12/16.
 */

import java.util.List;

/**
 * @author Administrator
 **/

public class NewsArticleVo {


    /**
     *
     */
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 缩略图以竖线分割
     */
    private List<String> thumbnails;

    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(List<String> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
