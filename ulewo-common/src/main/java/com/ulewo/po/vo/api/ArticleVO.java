package com.ulewo.po.vo.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ulewo.po.enums.TopicType;
import com.ulewo.po.model.Attachment;
import com.ulewo.po.model.Like;
import com.ulewo.po.serializ.CustomDateSerializer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */
public class ArticleVO {
    private Integer topicId; // id

    private String title; // 标题

    private String summary;

    private String content; // 内容

    private Integer userId; // 作者

    private String userName;

    private String userIcon;

    @JsonSerialize(using = CustomDateSerializer.class)
    private Date createTime; // 发布时间

    private Integer readCount = 0; // 阅读次数

    private Integer commentCount = 0; // 回复数量

    private String topicImageThum; // 缩略图

    private Integer likeCount = 0;

    private Integer collectionCount = 0;

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getTopicImageThum() {
        return topicImageThum;
    }

    public void setTopicImageThum(String topicImageThum) {
        this.topicImageThum = topicImageThum;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(Integer collectionCount) {
        this.collectionCount = collectionCount;
    }
}
