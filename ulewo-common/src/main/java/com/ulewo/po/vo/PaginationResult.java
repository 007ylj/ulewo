package com.ulewo.po.vo;

import java.util.ArrayList;
import java.util.List;

import com.ulewo.po.enums.ResponseCode;

/**
 * ClassName: PaginationResult
 * date: 2015年8月9日 下午12:06:48
 * @author 多多洛
 * @version @param <T>
 * @since JDK 1.7
 */
public class PaginationResult<T> {
    private SimplePage page;
    private List<T> list = new ArrayList<T>();

    public PaginationResult(SimplePage page, List<T> list, ResponseCode ResponseCode, StringBuilder msg) {
        this.list = list;
        this.page = page;
    }

    public PaginationResult(SimplePage page, List<T> list) {
        this.list = list;
        this.page = page;
    }

    public PaginationResult(List<T> list, ResponseCode ResponseCode, StringBuilder msg) {
        this.list = list;
    }


    public PaginationResult() {

    }

    public SimplePage getPage() {
        return page;
    }

    public void setPage(SimplePage page) {
        this.page = page;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

}
