package com.ulewo.po.vo.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ulewo.po.enums.ArticleType;
import com.ulewo.po.model.Comment;
import com.ulewo.po.model.SpitSlotComment;
import com.ulewo.po.serializ.CustomDateSerializer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */
public class CommentVO {
    private Integer id;

    private Integer pid;

    private Integer articleId; // 主题帖Id

    private Integer userId;

    private String userName;

    private String userIcon;

    private String content;

    @JsonSerialize(using = CustomDateSerializer.class)
    private Date createTime;

    private List<CommentVO> children = new ArrayList<>();

    private String sourceFrom;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public List<CommentVO> getChildren() {
        return children;
    }

    public void setChildren(List<CommentVO> children) {
        this.children = children;
    }

    public String getSourceFrom() {
        return sourceFrom;
    }

    public void setSourceFrom(String sourceFrom) {
        this.sourceFrom = sourceFrom;
    }

}
