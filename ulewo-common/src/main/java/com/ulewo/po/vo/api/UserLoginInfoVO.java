package com.ulewo.po.vo.api;

/**
 * Created by Administrator on 2017/3/9.
 */
public class UserLoginInfoVO {
    private String token;
    private String aesPwd;
    private Integer userId;
    private String userName;
    private String avatar;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAesPwd() {
        return aesPwd;
    }

    public void setAesPwd(String aesPwd) {
        this.aesPwd = aesPwd;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
