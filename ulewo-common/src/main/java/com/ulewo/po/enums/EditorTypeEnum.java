package com.ulewo.po.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EditorTypeEnum {
    UEDITOR(1, "UEditor百度编辑器"), MARKDOWN(2, "markdown编辑器");
    private Integer type;
    private String desc;

    EditorTypeEnum(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static EditorTypeEnum getEditorTypeByType(Integer type) {
        if (null == type) {
            return null;
        }
        for (EditorTypeEnum editorType : EditorTypeEnum.values()) {
            if (editorType.getType().intValue() == type.intValue()) {
                return editorType;
            }
        }
        return null;
    }
}
