package com.ulewo.po.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SourceFrom {
    PC("P", "PC"), A("A", "Android"), I("I", "iPhone");
    private String type;
    private String desc;

    SourceFrom(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static SourceFrom getSourceFromByValue(String type) {
        if (null == type) {
            return null;
        }
        for (SourceFrom s : SourceFrom.values()) {
            if (s.getType().equals(type)) {
                return s;
            }
        }
        return null;
    }
}
