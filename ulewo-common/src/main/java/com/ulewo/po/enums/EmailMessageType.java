/**
 * Project Name:ulewo-common
 * File Name:AllowPost.java
 * Package Name:com.ulewo.po.enums
 * Date:2015年10月17日下午12:59:04
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.enums;

/**
 * ClassName:AllowPost <br/>
 * Date:     2015年10月17日 下午12:59:04 <br/>
 * @author 多多洛
 *         Copyright (c) 2015, ulewo.com All Rights Reserved.
 */
public enum EmailMessageType {
    RECEIVE(0, "接受消息"), NOTRECEIVE(1, "不接受消息");

    private Integer type;
    private String desc;

    EmailMessageType(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static EmailMessageType getEmailMessageType(Integer type) {
        if (type == null) {
            return null;
        }
        for (EmailMessageType at : EmailMessageType.values()) {
            if (at.getType().equals(type)) {
                return at;
            }
        }
        return null;
    }
}
