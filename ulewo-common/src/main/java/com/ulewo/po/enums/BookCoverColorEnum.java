package com.ulewo.po.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by Administrator on 2017/3/22.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum BookCoverColorEnum {
    COLOR_01("", ""), COLOR_02("", "");
    private String colorCode;
    private String desc;

    BookCoverColorEnum(String colorCode, String desc) {
        this.colorCode = colorCode;
        this.desc = desc;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static BookCoverColorEnum getColorByColorCode(String colorCode) {
        if (null == colorCode) {
            return null;
        }
        for (BookCoverColorEnum code : BookCoverColorEnum.values()) {
            if (code.getColorCode().equals(colorCode)) {
                return code;
            }
        }
        return null;
    }
}
