package com.ulewo.po.enums;

/**
 * ClassName: 积分
 * date: 2015年8月9日 下午12:04:13
 * @author 多多洛
 * @since JDK 1.7
 */
public enum NewsStatusEnum {
    NO_POST(0, "已发布"), POST(1, "已发布");

    private int status;
    private String desc;

    NewsStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return this.status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
