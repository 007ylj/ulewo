/**
 * Project Name:ulewo-common
 * File Name:AllowPost.java
 * Package Name:com.ulewo.po.enums
 * Date:2015年10月17日下午12:59:04
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.po.enums;

import com.ulewo.utils.StringTools;

/**
 * ClassName:AllowPost <br/>
 * Date:     2015年10月17日 下午12:59:04 <br/>
 * @author 多多洛
 *         Copyright (c) 2015, ulewo.com All Rights Reserved.
 */
public enum UserBackground {
    BG1("defbg/bg1.jpg", "#95A3A6"), BG2("defbg/bg2.jpg", "#EAE7D8"), BG3("defbg/bg3.jpg", "#060007"), BG4("defbg/bg4.jpg", "#D1C4B3"), BG5("defbg/bg5.jpg", "#E5E7DC"), BG6("defbg/bg6.jpg", "#42332E"), BG7("defbg/bg7.jpg", "#1E7FA0"), BG8("defbg/bg8.jpg", "#E7E1D3"), BG9("defbg/bg9.jpg", "#996ED7"), BG10("defbg/bg10.jpg", "#E6E0D2");

    private String type;
    private String color;

    UserBackground(String type, String color) {
        this.type = type;
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public static UserBackground getBackGroundByType(String type) {
        if (StringTools.isEmpty(type)) {
            return null;
        }
        for (UserBackground at : UserBackground.values()) {
            if (at.getType().equals(type)) {
                return at;
            }
        }
        return null;
    }
}
