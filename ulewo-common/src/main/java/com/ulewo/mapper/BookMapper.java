package com.ulewo.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * 书、专题 数据库操作接口
 */
public interface BookMapper<T, P> extends BaseMapper<T, P> {

    /**
     * 根据BookId更新
     */
    public Integer updateByBookId(@Param("bean") T t, @Param("bookId") Long bookId);


    /**
     * 根据BookId删除
     */
    public Integer deleteByBookId(@Param("bookId") Long bookId);


    /**
     * 根据BookId获取对象
     */
    public T selectByBookId(@Param("bookId") Long bookId);

    /**
     * 更新记录数
     * @param bookId
     * @param count
     */
    public void updateBookChapterCount(@Param("bookId") Long bookId, int count);
}
