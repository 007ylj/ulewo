/**
 * Project Name:ulewo-common
 * File Name:ThresholdMapper.java
 * Package Name:com.ulewo.mapper
 * Date:2016年1月16日下午4:13:08
 * Copyright (c) 2016, ulewo.com All Rights Reserved.
 */

package com.ulewo.mapper;

/**
 * ClassName:ThresholdMapper <br/>
 * Date:     2016年1月16日 下午4:13:08 <br/>
 * @author 多多洛
 * Copyright (c) 2016, ulewo.com All Rights Reserved. 
 */
public interface ThresholdMapper<T, Q> extends BaseMapper<T, Q> {

}
