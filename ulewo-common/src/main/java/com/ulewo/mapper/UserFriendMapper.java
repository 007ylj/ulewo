package com.ulewo.mapper;

import org.springframework.stereotype.Repository;

/**
 * 好友
 * @author luo.hl
 * @version 3.0
 */
@Repository
public interface UserFriendMapper<T, Q> extends BaseMapper<T, Q> {

}
