package com.ulewo.mapper;

import com.ulewo.po.query.UpdateQuyery4ArticleCount;
import org.apache.ibatis.annotations.Param;

/**
 * 章节 数据库操作接口
 */
public interface BookChapterMapper<T, P> extends BaseMapper<T, P> {

    /**
     * 根据ArticleId更新
     */
    public Integer updateByArticleId(@Param("bean") T t, @Param("articleId") Long articleId);


    /**
     * 根据ArticleId删除
     */
    public Integer deleteByArticleId(@Param("articleId") Long articleId);


    /**
     * 根据ArticleId获取对象
     */
    public T selectByArticleId(@Param("articleId") Long articleId);


    public void updateInfoCount(UpdateQuyery4ArticleCount query);
}
