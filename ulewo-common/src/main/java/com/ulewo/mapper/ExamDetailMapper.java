/**
 * Project Name:ulewo-common
 * File Name:ExamDetailMapper.java
 * Package Name:com.ulewo.mapper
 * Date:2015年10月10日下午9:57:42
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.ulewo.po.model.Exam;

/**
 * ClassName:ExamDetailMapper <br/>
 * Date:     2015年10月10日 下午9:57:42 <br/>
 * @author 多多洛
 *         Copyright (c) 2015, ulewo.com All Rights Reserved.
 */
@Repository
public interface ExamDetailMapper<T, Q> extends BaseMapper<T, Q> {

    /**
     * selectListWithRightAnswer:(查询考题答案和正确答案). <br/>
     * @param examId
     * @return
     * @since JDK 1.7
     */
    public List<Exam> selectListWithRightAnswer(@Param("examId") Integer examId);
}
