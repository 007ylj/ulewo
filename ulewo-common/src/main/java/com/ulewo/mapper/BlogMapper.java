package com.ulewo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ulewo.po.model.Blog;
import com.ulewo.po.query.UpdateQuyery4ArticleCount;

public interface BlogMapper<T, Q> extends BaseMapper<T, Q> {
    public void updateInfoCount(UpdateQuyery4ArticleCount query);

    public void deleteBatch(@Param("ids") Integer[] ids);

    public List<Blog> selectPreAndNext(@Param("userId") Integer userId, @Param("blogId") Integer blogId, @Param("categoryId") Integer categoryId);
}
