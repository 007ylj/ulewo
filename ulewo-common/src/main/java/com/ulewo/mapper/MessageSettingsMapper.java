/**
 * Project Name:ulewo-common
 * File Name:MessageMapper.java
 * Package Name:com.ulewo.mapper
 * Date:2015年11月30日下午9:25:07
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.mapper;


/**
 * ClassName:MessageMapper <br/>
 * Date:     2015年11月30日 下午9:25:07 <br/>
 * @author 多多洛
 * Copyright (c) 2015, ulewo.com All Rights Reserved. 
 */
public interface MessageSettingsMapper<T, Q> extends BaseMapper<T, Q> {
}
