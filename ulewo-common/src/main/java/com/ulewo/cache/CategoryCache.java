/**
 * Project Name:ulewo-web
 * File Name:CategoryCache.java
 * Package Name:com.ulewo.cache
 * Date:2015年11月5日下午9:13:22
 * Copyright (c) 2015, ulewo.com All Rights Reserved.
 */

package com.ulewo.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ulewo.po.model.Category;
import com.ulewo.service.CategoryService;
import com.ulewo.utils.Constants;
import com.ulewo.utils.SpringContextUtil;

/**
 * ClassName:CategoryCache <br/>
 * Date:     2015年11月5日 下午9:13:22 <br/>
 * @author 多多洛
 * Copyright (c) 2015, ulewo.com All Rights Reserved. 
 */
public class CategoryCache {

    private static Map<String, List<Category>> categoryCache = null;

    private static Map<String, Category> signleCategoryCache = null;

    private static CategoryService categoryService = (CategoryService) SpringContextUtil.getBean("categoryService");

    static {
        categoryCache = new HashMap<String, List<Category>>();
        signleCategoryCache = new HashMap<String, Category>();
        categoryCache.put(Constants.CACHE_KEY_BBS_CATEGORY, new ArrayList<Category>());
        categoryCache.put(Constants.CACHE_KEY_EXAM_CATEGORY, new ArrayList<Category>());
        categoryCache.put(Constants.CACHE_KEY_KNOWLEDGE_CATEGORY, new ArrayList<Category>());
        categoryCache.put(Constants.CACHE_KEY_ASK_CATEGORY, new ArrayList<Category>());
    }

    public static void refreshCategoryCache() {

        //清空原来的数据
        categoryCache.get(Constants.CACHE_KEY_BBS_CATEGORY).clear();
        categoryCache.get(Constants.CACHE_KEY_EXAM_CATEGORY).clear();
        categoryCache.get(Constants.CACHE_KEY_KNOWLEDGE_CATEGORY).clear();
        categoryCache.get(Constants.CACHE_KEY_ASK_CATEGORY).clear();
        List<Category> list = categoryService.findCategroyList(null);
        for (Category c : list) {
            if (Constants.Y.equals(c.getShowInBBS())) {//bbs
                Category c_temp = (Category) c.clone();
                categoryCache.get(Constants.CACHE_KEY_BBS_CATEGORY).add(c_temp);
                filterChild(c_temp, Constants.CACHE_KEY_BBS_CATEGORY);
            }
            if (Constants.Y.equals(c.getShowInExam())) {//exam
                Category c_temp = (Category) c.clone();
                categoryCache.get(Constants.CACHE_KEY_EXAM_CATEGORY).add(c_temp);
                filterChild(c_temp, Constants.CACHE_KEY_EXAM_CATEGORY);
            }
            if (Constants.Y.equals(c.getShowInKnowledge())) {//knowledge
                Category c_temp = (Category) c.clone();
                categoryCache.get(Constants.CACHE_KEY_KNOWLEDGE_CATEGORY).add(c_temp);
                filterChild(c_temp, Constants.CACHE_KEY_KNOWLEDGE_CATEGORY);
            }
            if (Constants.Y.equals(c.getShowInQuestion())) {//ask
                Category c_temp = (Category) c.clone();
                categoryCache.get(Constants.CACHE_KEY_ASK_CATEGORY).add(c_temp);
                filterChild(c_temp, Constants.CACHE_KEY_ASK_CATEGORY);
            }
            signleCategoryCache.put(Constants.CACHE_KEY_CATEOGRY + c.getCategoryId(), c);
            List<Category> children = c.getChildren();
            for (Category s : children) {
                signleCategoryCache.put(Constants.CACHE_KEY_CATEOGRY + s.getCategoryId(), s);
            }

        }
    }

    public static void filterChild(Category c, String show) {
        List<Category> filterChildren = new ArrayList<Category>();
        List<Category> children = c.getChildren();
        for (Category s : children) {
            if (show.equals(Constants.CACHE_KEY_BBS_CATEGORY) && Constants.Y.equals(s.getShowInBBS())) {
                filterChildren.add(s);
            }
            if (show.equals(Constants.CACHE_KEY_EXAM_CATEGORY) && Constants.Y.equals(s.getShowInExam())) {
                filterChildren.add(s);
            }
            if (show.equals(Constants.CACHE_KEY_KNOWLEDGE_CATEGORY) && Constants.Y.equals(s.getShowInKnowledge())) {
                filterChildren.add(s);
            }
            if (show.equals(Constants.CACHE_KEY_ASK_CATEGORY) && Constants.Y.equals(s.getShowInQuestion())) {
                filterChildren.add(s);
            }

        }
        c.setChildren(filterChildren);
    }

    /**
     *
     * getBbsCategory:(获取论坛分类). <br/>
     *
     * @author 多多洛
     * @return
     * @since JDK 1.7
     */
    public static List<Category> getBbsCategory() {
        return categoryCache.get(Constants.CACHE_KEY_BBS_CATEGORY);
    }

    public static List<Category> getKnowledgeCategory() {
        return categoryCache.get(Constants.CACHE_KEY_KNOWLEDGE_CATEGORY);
    }

    public static List<Category> getAskCategory() {
        return categoryCache.get(Constants.CACHE_KEY_ASK_CATEGORY);
    }

    public static List<Category> getExamCategory() {
        return categoryCache.get(Constants.CACHE_KEY_EXAM_CATEGORY);
    }

    public static Category getCategoryById(Integer categoryId) {
        return signleCategoryCache.get(Constants.CACHE_KEY_CATEOGRY + categoryId);
    }
}
