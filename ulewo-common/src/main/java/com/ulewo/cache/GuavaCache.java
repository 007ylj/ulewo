package com.ulewo.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.ulewo.utils.StringTools;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2017/3/9.
 */
public class GuavaCache {
    private static final int TOKEN_INVALID_MIN = 2;

    private static LoadingCache<String, String> apiTokenCache = CacheBuilder
            .newBuilder().expireAfterAccess(TOKEN_INVALID_MIN, TimeUnit.MINUTES)
            .build(new CacheLoader<String, String>() {
                @Override
                public String load(String key) throws Exception {
                    return "";
                }
            });

    public static void put(String key, String value) {
        apiTokenCache.put(key, value);
    }

    public static String get(String key) {
        try {
            if (StringTools.isEmpty(key)) {
                return null;
            }
            String value = apiTokenCache.get(key);
            return "".equals(value) ? null : value;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(3000 * 60);
        System.out.println(GuavaCache.get("xx"));
    }
}
